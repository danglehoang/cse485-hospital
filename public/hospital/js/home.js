var modal1 = document.getElementsByClassName('modal-item-1')[0];
var closeModal1 = document.querySelectorAll('.modal .close-modal')[0];
var modalItem1 = document.getElementById("modalItem1");
var btnItem1 = document.getElementById("btnItem1");

var modal2 = document.getElementsByClassName('modal-item-2')[0];
var closeModal2 = document.querySelectorAll('.modal .close-modal')[1];
var modalItem2 = document.getElementById("modalItem2");
var btnItem2 = document.getElementById("btnItem2");

var modal3 = document.getElementsByClassName('modal-item-3')[0];
var modalItem3 = document.getElementById("modalItem3");
var btnItem3 = document.getElementById("btnItem3");
var closeModal3 = document.querySelectorAll('.modal .close-modal')[2];

btnItem1.onclick = function () {
    modalItem1.style.display = "block";
    document.querySelector('body').style.overflow = "hidden";
};

closeModal1.onclick = function () {
    modalItem1.style.display = "none";
    document.querySelector('body').style.overflow = "visible";
};

btnItem2.onclick = function () {
    modalItem2.style.display = "block";
    document.querySelector('body').style.overflow = "hidden";
};
closeModal2.onclick = function () {
    modalItem2.style.display = "none";
    document.querySelector('body').style.overflow = "visible";
};

btnItem3.onclick = function () {
    modalItem3.style.display = "block";
    document.querySelector('body').style.overflow = "hidden";
};
closeModal3.onclick = function () {
    modalItem3.style.display = "none";
    document.querySelector('body').style.overflow = "visible";
};
window.onclick = function (event) {
    if (event.target == modal1) {
        modalItem1.style.display = "none";
        document.querySelector('body').style.overflow = "visible";
    }
    if (event.target == modal2) {
        modalItem2.style.display = "none";
        document.querySelector('body').style.overflow = "visible";
    }
    if (event.target == modal3) {
        modalItem3.style.display = "none";
        document.querySelector('body').style.overflow = "visible";
    }
}
$('.wrapper-home-banner .owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    responsive: {
        0: {
            items: 1
        }
    }
});

var owl2 = $('.section-2 .owl-carousel');
owl2.owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    onDragged: draggedSlide,
    onTranslated: draggedSlide,
    onInitialized: draggedSlide,
    autoHeight: true,
    navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    responsive: {
        0: {
            items: 3
        }
    }
});
$('.section-2 .owl-next').click(function () {
    owl2.trigger('next.owl.carousel', [1000]);
    draggedSlide();
});
$('.section-2 .owl-prev').click(function () {
    owl2.trigger('prev.owl.carousel', [1000]);
    draggedSlide();
});

function draggedSlide(event) {
    document.querySelectorAll('.section-2 .owl-stage .owl-item.active')[1].classList.add('pos-center');
    document.querySelectorAll('.section-2 .owl-stage .owl-item.active')[0].classList.remove('pos-center');
    document.querySelectorAll('.section-2 .owl-stage .owl-item.active')[2].classList.remove('pos-center');
}

function openTabs(evt, tabs) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabs).style.display = "block";
    evt.currentTarget.className += " active";
}

document.getElementById('defaultTabs').click();
$('.section-5 .owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoHeight: true,
    dots: false,
    navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    responsive: {
        0: {
            items: 6
        }
    }
});

function redirectMenu(id) {
    location.href = "/admin/articles/ajax_edit/" + id;
}

function redirectArticles(id_category, slug_category, type_category, menu_number = '', slug_menu = '') {
    if(menu_number != '') {
        localStorage.setItem('menu_type', 'menu_' + menu_number);
    }
    location.href = "/" + slug_menu + '/' + slug_category + '-' + id_category;
}

function redirectArticlesMenu(id, slug, menu_number) {
    localStorage.setItem('menu_type', 'menu_' + menu_number);
    location.href = '/menu/' + slug + '-' + id;
}

function redirectArticlesByBasisId(id) {
    location.href = '/footer/' +  id;
}

function redirectArticlesByDepartmentId(id) {
    location.href = '/department/' +  id;
}

function redirectDetailArticles(id, slug) {
    location.href = '/' + slug + '-' + id;
}

function redirectDetailDoctor(id, slug) {
    location.href = '/doctor/' + slug + '-' + id;
}

function submitSearchDoctor() {
    var department_id = $('#department_id').val(),
        basis_id = $('#basis_id').val();

    if(department_id == 'defaulta') {
        department_id = '';
    }
    if(basis_id == 'defaulta') {
        basis_id = '';
    }

    location.href = window.location.pathname + '?department=' + department_id + '&basis=' + basis_id;
}

function submitSearchArticles() {
    var articles = $('#search_articles').val();

    if (articles == '') {
        toastr.remove();
        toastr['error']('Vui lòng nhập thông tin tìm kiếm');
    } else {
        location.href = '/search?articles=' + articles;
    }
}
var count_click_search_mb = 0;
function addOpenSearchMB(count) {
    count = ++count_click_search_mb;
    if (count % 2) {
        document.getElementById('search-form-mb').classList.add('open');
    } else {
        document.getElementById('search-form-mb').classList.remove('open');
    }
}

