$('.wrapper_header_mb .button_menu').click(function(){
    $(this).hasClass('show_menu') ? $(this).removeClass('show_menu') : $(this).addClass('show_menu');
    $('.wrapper_navbar_menu').slideToggle(500);
});

$('.wrapper_navbar_menu .dropdown').click(function(){
    $(this).children('.dropdown-menu-mb').slideToggle(500);
});

var count_click_search = 0;

function addOpenSearch(count) {
    count = ++count_click_search;
    if (count % 2) {
        document.getElementById('search-form').classList.add('open');
    } else {
        document.getElementById('search-form').classList.remove('open');
    }
}

$(document).ready(function () {
    document.addEventListener("scroll", function () {
        var menu = document.querySelector('header');
        var heightMenu = menu.offsetHeight;
        var pageY = pageYOffset;
        pageY > heightMenu ? menu.classList.add('header-scroll') : menu.classList.remove('header-scroll');
    });

    var menu_current = localStorage.getItem('menu_type');

    if(menu_current != null) {
        document.getElementById(menu_current).classList.add('active');
    }

    if(window.location.pathname == '/') {
        localStorage.removeItem("menu_type");
    }
});
