//file js dinh nghia ham dung chung
var table = '',
    save_method = '',
    csrf_token = '',
    dataFilter = {};


function init_data_table() {
    //load table ajax
    var element = $('#data-table');
    if (jQuery.isEmptyObject(dataFilter) == false) {
        table = element.DataTable({
            'ajax': {
                type: "POST",
                url: url_ajax_search,
                headers: {
                    "X-CSRF-TOKEN": csrf_token
                },
                data: function (d) {
                    return $.extend({}, d, dataFilter);
                }
            },
            "language": {
                "sProcessing": "Đang xử lý...",
                "sLengthMenu": "Xem _MENU_ mục",
                "sZeroRecords": "Data not found",
                "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sSearch": "Tìm:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Đầu",
                    "sPrevious": "Trước",
                    "sNext": "Tiếp",
                    "sLast": "Cuối"
                }
            },
            fixedHeader: true,
            'bProcessing': true,
            'serverSide': true,
            'dom': 'Bfrtip',
            'buttons': [],
            "oSearch": {"bSmart": false, "bRegex": true},
            'columnDefs': [
                {
                    'targets': 'no-sort',
                    "orderable": false,
                    'className': 'text-center'
                },
                {
                    'targets': 0,
                    'visible': false,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data) {
                        return '<input type="checkbox" class="chk_id" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                    }
                },
                {
                    'targets': -1,
                    'searchable': false,
                    'orderable': false
                }
            ],
            'order': [[1, 'desc']],
            "ordering": false,
            "bFilter": false,
            "fnDrawCallback": function () {
                //$("a.fancybox").fancybox();
            }
        });
    } else {
        table = element.DataTable({
            'ajax': {
                type: "POST",
                url: url_ajax_load_datatable,
                headers: {
                    "X-CSRF-TOKEN": csrf_token
                },
                data: function (d) {
                    return $.extend({}, d, dataFilter);
                }
            },
            "language": {
                "sProcessing": "Processing...",
                "sLengthMenu": "Watching _MENU_ entries",
                "sZeroRecords": "Data not found",
                "sInfo": "Watching _START_ to _END_ in total of  _TOTAL_ entries",
                "sInfoEmpty": "Watching 0 to 0 in total of 0 entries",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sSearch": "Search:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Top",
                    "sPrevious": "Before",
                    "sNext": "Next",
                    "sLast": "End"
                }
            },
            fixedHeader: true,
            'bProcessing': true,
            'serverSide': true,
            'dom': 'Bfrtip',
            'buttons': [],
            "oSearch": {"bSmart": false, "bRegex": true},
            'columnDefs': [
                {
                    'targets': 'no-sort',
                    "orderable": false,
                    'className': 'text-center'
                },
                {
                    'targets': 0,
                    'visible': false,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta) {
                        return '<input type="checkbox" class="chk_id" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                    }
                },
                {
                    'targets': -1,
                    'searchable': false,
                    'orderable': false
                }
            ],
            'order': [[1, 'desc']],
            "ordering": false,
            "bFilter": false,
            "fnDrawCallback": function () {
                //$("a.fancybox").fancybox();
            }
        });
    }
}

function updateSortDatatables() {
    $.extend(
        $.fn.dataTable.RowReorder.defaults,
        {dataSrc: 2, selector: 'td:not(:first-child, :last-child, :nth-child(5), :nth-child(6))'}
    );
    $.fn.dataTable.defaults.rowReorder = true;
    table.on('row-reorder', function (e, diff, edit) {
        var result = 'Reorder started on row: ' + edit.triggerRow.data()[1] + '\n';

        var ien = diff.length;
        for (var i = 0; i < ien; i++) {
            var rowData = table.row(diff[i].node).data();

            result += rowData[1] + ' updated to be in position ' +
                diff[i].newData + ' (was ' + diff[i].oldData + ') \n';

            updateField(rowData[1], 'order', diff[i].newData);
        }
        console.log(result);
    });
}

function filterDatatables(data) {
    dataFilter = data;
    reload_table();
}

function filterDatatablesSearch(data) {
    dataFilter = data
    table.ajax.url(url_ajax_search).load();
}

function reload_table_after() {
    window.location.reload();
}
