$(function () {
    if (page_curent == 'index')
    {
        init_data_table();
    }
});

function add_form()
{
    location.href = url_add;
    save_method = 'add';
}

function edit_item(id)
{
    location.href = "/admin/articles/ajax_edit/" + id;
    save_method = 'update';
}

function save_create()
{
    $('#btnSave').text('Đang lưu');
    $('#btnSave').attr('disabled', true);

    var formData = new FormData();
    formData.append("category_id", $('#category_id').val());
    formData.append("slug", $('#slug').val());
    formData.append("status", $('#status').val());
    formData.append("basis_id", $('#basis_id').val());
    formData.append("department_id", $('#department_id').val());
    formData.append("is_hot", $('#is_hot').val());
    formData.append("type", $('#type').val());
    formData.append("imageFileThumb", $('#profile-img-thumb')[0].files[0]);
    formData.append("imageFileBanner", $('#profile-img-banner')[0].files[0]);

    formData.append("lang_code_en", $('#lang_code_en').val());
    formData.append("title_en", $('#title_en').val());
    formData.append("content_en", $('#content_en').val());
    formData.append("summary_en", $('#summary_en').val());

    formData.append("lang_code_vi", $('#lang_code_vi').val());
    formData.append("title_vi", $('#title_vi').val());
    formData.append("content_vi", $('#content_vi').val());
    formData.append("summary_vi", $('#summary_vi').val());

    $.ajax({
        url: url_ajax_add,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var data = JSON.parse(response);
            if (data.type === "warning") {
                toastr.remove();
                toastr[data.type](data.message);

                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');

                $.each(data.validation, function (i, val) {
                    $('[name="' + i + '"]').addClass('is-invalid');
                    $('[name="' + i + '"]').closest('.form-group').append('<span class="error invalid-feedback" style="">' + val + '</span>');
                })
            } else if(data.type === "error") {
                toastr.remove();
                toastr[data.type](data.message);
            } else {
                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                location.href = '/admin/articles';
            }
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }
    });
}

function save_edit()
{
    $('#btnSave').text('Processing save');
    $('#btnSave').attr('disabled', true);

    var formData = new FormData();
    formData.append("id", $('.id').val());
    formData.append("category_id", $('#category_id').val());
    formData.append("slug", $('#slug').val());
    formData.append("status", $('#status').val());
    formData.append("basis_id", $('#basis_id').val());
    formData.append("department_id", $('#department_id').val());
    formData.append("is_hot", $('#is_hot').val());
    formData.append("type", $('#type').val());
    formData.append("imageFileThumb", $('#profile-img-thumb')[0].files[0]);
    formData.append("imageFileBanner", $('#profile-img-banner')[0].files[0]);

    formData.append("lang_code_en", $('#lang_code_en').val());
    formData.append("title_en", $('#title_en').val());
    formData.append("content_en", $('#content_en').val());
    formData.append("summary_en", $('#summary_en').val());

    formData.append("lang_code_vi", $('#lang_code_vi').val());
    formData.append("title_vi", $('#title_vi').val());
    formData.append("content_vi", $('#content_vi').val());
    formData.append("summary_vi", $('#summary_vi').val());

    $.ajax({
        url: url_ajax_update,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var data = JSON.parse(response);
            if (data.type === "warning") {
                toastr.remove();
                toastr[data.type](data.message);

                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');

                $.each(data.validation, function (i, val) {
                    $('[name="' + i + '"]').addClass('is-invalid');
                    $('[name="' + i + '"]').closest('.form-group').append('<span class="error invalid-feedback" style="">' + val + '</span>');
                })
            } else if(data.type === "error") {
                toastr.remove();
                toastr[data.type](data.message);
            } else {
                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                location.href = '/admin/articles';
            }
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }
    });
}

function delete_item(id)
{
    var sure = confirm("Are you sure you delete articles ?");

    if (sure) {
        $.ajax({
            url: url_ajax_delete,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            data: {'id': id},
            dataType: "JSON",
            success: function (data) {
                if (data.status == 'success') {
                    reload_table_after();
                    toastr.remove();
                    toastr[data.type](data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
}

function readUrlThumb(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#profile-img-tag-thumb').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readUrlBanner(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#profile-img-tag-banner').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function search_item() {
    var status_search = $('#status_search').val(),
        department_search = $('#department_search').val(),
        basis_search = $('#basis_search').val(),
        is_hot_search = $('#is_hot_search').val(),
        type_search = $('#type_search').val();

    var data = {
        status_search: status_search,
        department_search: department_search,
        basis_search: basis_search,
        is_hot_search: is_hot_search,
        type_search: type_search
    }
    filterDatatablesSearch(data);
}

function submitForm()
{
    search_item();
}
