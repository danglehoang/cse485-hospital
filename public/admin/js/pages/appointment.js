$(function () {
    init_data_table();
});

function show_item(id)
{
    $('span.invalid-feedback').remove();
    $('.form-control').removeClass('is-invalid');
    slug_disable = true;
    save_method = 'update';

    $.ajax({
        url: url_ajax_show + "/" + id,
        type: "GET",
        dataType: "JSON",

        success: function (data) {
            if (data.status == 'success') {
                $.each(data.data, function (k, v) {
                    $('#form [name="' + k + '"]').text(v);
                });
                $('#modal_form').modal('show');
            } else {
                toastr.remove();
                toastr[data.type](data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}
