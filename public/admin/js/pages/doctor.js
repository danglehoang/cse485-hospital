$(function () {
    if (page_curent == 'index')
    {
        init_data_table();
    }
});

function add_form()
{
    location.href = url_add;
    save_method = 'add';
}

function edit_item(id)
{
    location.href = "/admin/doctor/ajax_edit/" + id;
    save_method = 'update';
}

function save_create()
{
    $('#btnSave').text('Đang lưu');
    $('#btnSave').attr('disabled', true);

    var formData = new FormData();
    formData.append("name", $('#name').val());
    formData.append("department_id", $('#department_id').val());
    formData.append("basis_id", $('#basis_id').val());
    formData.append("language", $('#language').val());
    formData.append("slug", $('#slug').val());
    formData.append("status", $('#status').val());
    formData.append("education", $('#education').val());
    formData.append("specialized_activities", $('#specialized_activities').val());
    formData.append("achievement", $('#achievement').val());
    formData.append("advisory", $('#advisory').val());
    formData.append("description", $('#description').val());
    formData.append("level", $('#level').val());
    formData.append("imageFile", $('#profile-img')[0].files[0]);

    $.ajax({
        url: url_ajax_add,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var data = JSON.parse(response);
            if (data.type === "warning") {
                toastr.remove();
                toastr[data.type](data.message);
                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');

                $.each(data.validation, function (i, val) {
                    $('[name="' + i + '"]').addClass('is-invalid');
                    $('[name="' + i + '"]').closest('.form-group').append('<span class="error invalid-feedback" style="">' + val + '</span>');
                })
            } else if(data.type === "error") {
                toastr.remove();
                toastr[data.type](data.message);
            } else {
                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                location.href = '/admin/doctor';
            }
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }
    });
}
function save_edit()
{
    $('#btnSave').text('Processing save');
    $('#btnSave').attr('disabled', true);

    var formData = new FormData();
    formData.append("id", $('.id').val());
    formData.append("name", $('#name').val());
    formData.append("department_id", $('#department_id').val());
    formData.append("basis_id", $('#basis_id').val());
    formData.append("language", $('#language').val());
    formData.append("slug", $('#slug').val());
    formData.append("status", $('#status').val());
    formData.append("education", $('#education').val());
    formData.append("specialized_activities", $('#specialized_activities').val());
    formData.append("achievement", $('#achievement').val());
    formData.append("advisory", $('#advisory').val());
    formData.append("description", $('#description').val());
    formData.append("level", $('#level').val());
    formData.append("imageFile", $('#profile-img')[0].files[0]);

    $.ajax({
        url: url_ajax_update,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var data = JSON.parse(response);
            if (data.type === "warning") {
                toastr.remove();
                toastr[data.type](data.message);

                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');

                $.each(data.validation, function (i, val) {
                    $('[name="' + i + '"]').addClass('is-invalid');
                    $('[name="' + i + '"]').closest('.form-group').append('<span class="error invalid-feedback" style="">' + val + '</span>');
                })
            } else if(data.type === "error") {
                toastr.remove();
                toastr[data.type](data.message);
            } else {
                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                location.href = '/admin/doctor';
            }
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }
    });
}

function delete_item(id)
{
    var sure = confirm("Are you sure you delete doctor ?");
    if (sure) {
        $.ajax({
            url: url_ajax_delete,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            data: {'id': id},
            dataType: "JSON",
            success: function (data) {
                if (data.status == 'success') {
                    reload_table_after();
                    toastr.remove();
                    toastr[data.type](data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
}

function readURL(input)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#profile-img-tag').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function search_item() {
    var name_search = $('#name_search').val(),
        status_search = $('#status_search').val(),
        department_search = $('#department_search').val(),
        basis_search = $('#basis_search').val(),
        language_search = $('#language_search').val();

    var data = {
        name_search: name_search,
        status_search: status_search,
        department_search: department_search,
        basis_search: basis_search,
        language_search: language_search
    }
    filterDatatablesSearch(data);
}

function submitForm()
{
    search_item();
}
