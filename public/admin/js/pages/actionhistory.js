$(function () {
    init_data_table();
});

function search_item() {
    var name_search = $('#name_search').val(),
        type_search = $('#type_search').val();

    var data = {
        name_search: name_search,
        type_search: type_search,
    }
    filterDatatablesSearch(data);
}

function submitForm()
{
    search_item();
}
