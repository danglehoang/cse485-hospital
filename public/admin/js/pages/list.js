    function search_list() {
        var name_search = $('#name_search').val(),
            email_search = $('#email_search').val(),
            office_search = $('#office_search').val();
        if (name_search == '' && email_search == '' && office_search == '') {
            message = 'Please fill out the form search';
            type = 'warning';
            toastr[type](message);
        } else {
            var data = {
                name_search: name_search,
                email_search: email_search,
                office_search: office_search
            }

            var formData = new FormData();
            formData.append("name_search", name_search);
            formData.append("email_search", email_search);
            formData.append("office_search", office_search);

            $.ajax({
                url: url_ajax,
                type: "POST",
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": csrf_token
                },
                contentType: false,
                processData: false,
                success: function (response) {
                    var data = JSON.parse(response);
                    if (data.type === "success") {
                        toastr.remove();
                        toastr[data.type](data.message);
                        $("#tbody-data tr").remove();
                        var html = '';
                        html += `
                            <tr>
                                <td class="name"></td>
                                <td class="department_id"></td>
                                <td class="department_id"></td>
                                <td class="email"></td>
                                <td class="phone"></td>
                                <td class="department_id"></td>
                            </tr>
                            `;
                        $('#tbody-data').append(html);

                        $.each(data.data, function (k, v) {
                            $.each(v, function (a, s) {
                                $('.' + a).val(s);
                            });
                        });
                    } else {
                        toastr.remove();
                        toastr[data.type](data.message);
                    }
                },
            });
        }
    }
