$(function () {
    init_data_table();
});

function add_form()
{
    $("#form")[0].reset();
    $('.modal-title').text(title_add);
    save_method = 'add';
    $('#form').trigger("reset");
    $('#modal_form').modal('show');
}

function edit_item(id)
{
    $('.modal-title').text(title_edit);
    $('span.invalid-feedback').remove();
    $('.form-control').removeClass('is-invalid');
    slug_disable = true;
    save_method = 'update';

    $.ajax({
        url: url_ajax_edit + "/" + id,
        type: "GET",
        dataType: "JSON",

        success: function (data) {
            if (data.status == 'success') {
                $.each(data.data, function (k, v) {
                    $('.' + k).val(v);
                });
                $('#modal_form').modal('show');
            } else {
                toastr.remove();
                toastr[data.type](data.message);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function save()
{
    $('#btnSave').text('Processing save');
    $('#btnSave').attr('disabled', true);
    var url;
    if (save_method == 'add') {
        url = url_ajax_add;
    } else {
        url = url_ajax_update;
    }

    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function (data) {
            if (data.type === "warning") {
                toastr.remove();
                toastr[data.type](data.message);

                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');

                $.each(data.validation, function (i, val) {
                    $('[name="' + i + '"]').addClass('is-invalid');
                    $('[name="' + i + '"]').closest('.form-group').append('<span class="error invalid-feedback" style="">' + val + '</span>');
                })
            } else if(data.type === "error") {
                toastr.remove();
                toastr[data.type](data.message);
            } else {
                $('span.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                $('#modal_form').modal('hide');
                reload_table_after();
            }
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text('Save');
            $('#btnSave').attr('disabled', false);
        }
    });
}

function delete_item(id)
{
    var sure = confirm("Are you sure you delete department ?");

    if (sure) {
        $.ajax({
            url: url_ajax_delete,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": csrf_token
            },
            data: {'id': id},
            dataType: "JSON",
            success: function (data) {

                if (data.status == 'success') {
                    reload_table_after();
                    toastr.remove();
                    toastr[data.type](data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
}
