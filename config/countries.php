<?php

return [
    'code'=>[
        'VN'=>'Việt Nam',
        'ID'=>'Indonesia',
        'TH'=>'Thái Lan',
        'PH'=>'Philippines',
        'MY'=>'Malaysia',
        'SG'=>'Singapore',
        'EN'=>'Anh',
        'OTHER'=>'Other',
    ],
];
