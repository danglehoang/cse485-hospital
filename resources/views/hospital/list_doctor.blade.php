@extends('hospital.layout')

@section('content')
    <style>
        .img-hober-banner img {
            width: 100%;
        }
    </style>
    <div class="wrapper-person">
        <div class="container tab-intro-child-1 tab-intro-child-cck-1 mt-4">
            <div class="row intro-content-child-1">
                <div class="col-md-12 col-center">
                    <p class="img-hober-banner">
                        <img src="/{{ $banner->image }}">
                    </p>
                </div>
            </div>
        </div>
        <div class="container introduction-child-tab84 introduction-child-cck-tab84">
            <div class="row introduction-child">
                <div class="col-md-8 person-left">
                    <h1 class="title-person">
                        Đội ngũ chuyên gia
                    </h1>
                    <div class="search-person">
                        <form action="">
                            <div class="row">
                                <div class="form-group col-md-3 person-search-name">
                                    <input type="text" class="form-control" placeholder="Tìm theo tên">
                                </div>
                                <div class="form-group col-md-4">
                                    <select class="form-control" id="department_id" onchange="submitSearchDoctor()">
                                        <option value="defaulta">Chọn theo Chuyên khoa</option>
                                        @foreach($department as $item)
                                            <option value="{{ $item->id }}" {{ ($item->id == old('item', $department_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <select class="form-control" id="basis_id" onchange="submitSearchDoctor()">
                                        <option value="defaulta">Tìm kiếm theo cơ sở</option>
                                        @foreach($basis as $item)
                                            <option value="{{ $item->id }}" {{ ($item->id == old('item', $basis_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-1 person-search-button">
                                    <button class="sb-search"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="list-doctor">
                        @foreach($doctor as $item)
                            <div class="item-doctor d-flex">
                                <figure class="thumbnail">
                                    <a href="javascript:void(0);"
                                       onclick="redirectDetailDoctor('{{ $item->id }}', '{{ $item->slug }}')">
                                        <img src="/{{ $item->thumb }}" alt="Cao Độc Lập">
                                    </a>
                                </figure>
                                <div class="info-doctor">
                                    <div class="title">
                                        <a href="javascript:void(0);"
                                           onclick="redirectDetailDoctor('{{ $item->id }}', '{{ $item->slug }}')">
                                            {{ $item->name }}
                                        </a>
                                    </div>
                                    <div class="level">
                                        <img src="{{ asset('hospital/images/icon-level.png') }}">
                                        {{ $item->level }}
                                    </div>
                                    <div class="position">
                                        <img src="{{ asset('hospital/images/icon-park.png') }}">
                                        {{ $item->postsDepartment->name }}
                                    </div>
                                    <div class="address">
                                        <img src="{{ asset('hospital/images/icon-map.png') }}">
                                        {{ $item->postsBasis->name }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4 introduction-child-right">
                    <div id="search-form" class="search-form-wrapper open">
                        <div class="hoppicenter">
                            <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                            <button onclick="submitSearchArticles()">
                                Search
                            </button>
                        </div>
                    </div>
                    <h2 class="widget-title">
                        Bài viết mới
                    </h2>
                    <ul>
                        @foreach($articlesMore as $item)
                            <li>
                                <a href="javascript:void(0);"
                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                    {{ $item->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            document.title = 'Đội ngũ chuyên gia - Hồng Ngọc Hospital';
        </script>
    @endpush
@endsection
