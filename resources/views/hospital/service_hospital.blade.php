@extends('hospital.layout')

@section('content')
    <style>
        .img-hober-banner img {
            width: 100%;
        }
        label.error {
            color: red;
        }
    </style>
    <div class="wrapper-service">
        <div class="container tab-intro-child-1 tab-intro-child-cck-1 mt-4">
            <div class="row intro-content-child-1">
                <div class="col-md-12 col-center">
                    <p class="img-hober-banner">
                        <img src="/{{ $banner->image }}">
                    </p>
                </div>
            </div>
        </div>
        <div class="container service-container">
            <h1 class="title-service">Các chuyên khoa, dịch vụ</h1>
            <div class="content-service">
                <ul class="list-items-service">
                    @foreach($department as $item)
                        <li class="item-service">
                            <a href="javascript:void(0);"
                               onclick="redirectArticlesByDepartmentId('{{ $item->id }}')">
                                <img src="/{{ $item->thumb }}">
                                {{ $item->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="container service-container">
            <h1 class="title-service">Các dịch vụ</h1>
            <div class="content-service">
                <ul class="list-items-service">
                    @foreach($categories as $item)
                        <li class="item-service">
                            <a href="javascript:void(0);"
                               onclick="redirectArticles('{{ $item->id }}', '{{ $item->slug }}', '{{ $item->type }}', '', 'dich-vu-y-te')">
                                {{ $item->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="container service-container" style="margin-bottom: 30px">
            <h1 class="title-service">Đặt lịch hẹn khám</h1>
            <div class="content-service" style="font-size: 15px">
                <form enctype="multipart/form-data" id="formAppointment">
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Địa điểm muốn khám*</b></div>
                        <div class="col-8">
                            <select name="basis" id="basis" class="form-control" style="height: 40px; font-size: 15px" required>
                                <option value="" selected>Chọn địa điểm</option>
                                @foreach($basis as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Họ và tên*</b></div>
                        <div class="col-8">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Họ và tên" style="height: 40px; font-size: 15px" required>
                        </div>
                    </div>
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Ngày sinh*</b></div>
                        <div class="col-8">
                            <input type="date" name="birthday" id="birthday" class="form-control" placeholder="Họ và tên" style="height: 40px; font-size: 15px" required>
                        </div>
                    </div>
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Số điện thoại*</b></div>
                        <div class="col-8">
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Số điện thoại" style="height: 40px; font-size: 15px" required>
                        </div>
                    </div>
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Email*</b></div>
                        <div class="col-8">
                            <input type="text" name="email" id="email" class="form-control" placeholder="Email" style="height: 40px; font-size: 15px" required>
                        </div>
                    </div>
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Nhu cầu khám bệnh*</b></div>
                        <div class="col-8">
                            <input type="text" name="content" id="content" class="form-control" placeholder="Nhu cầu khám bệnh" style="height: 40px; font-size: 15px" required>
                        </div>
                    </div>
                    <div class="col-12 form-group row" style="margin-top: 20px">
                        <div class="col-4"><b>Ngày hẹn*</b></div>
                        <div class="col-8">
                            <input type="date" name="time_appointment" id="time_appointment" class="form-control" placeholder="Nhu cầu khám bệnh" style="height: 40px; font-size: 15px" required>
                        </div>
                    </div>
                    <div class="col-12 form-group" style="text-align: center; margin-top: 20px">
                        <button type="submit" data-href="javascript:void(0);" class="btn btn-success w-75" style="height: 40px; font-size: 15px; color: white">Đặt lịch khám</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="appointmentSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="font-size: 13px;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Thông báo</b></h5>
                </div>
                <div class="modal-body">
                    Đăng ký thành công. Bệnh viện sẽ liên hệ lại trong thời gian sớm nhất có thể !!!
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $("#formAppointment").validate({
                rules: {
                    basis: {
                        required: true,
                    },
                    phone: {
                        required: true,
                    },
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    birthday: {
                        required: true,
                    },
                    content: {
                        required: true,
                    },
                    time_appointment: {
                        required: true,
                    },
                },
                messages:{
                    basis:{
                        required:"Please choose basis"
                    },
                    phone:{
                        required:"Please import phone"
                    },
                    name:{
                        required:"Please import name"
                    },
                    email:{
                        required:"Please import email"
                    },
                    birthday:{
                        required:"Please import birthday"
                    },
                    content:{
                        required:"Please import content"
                    },
                    time_appointment:{
                        required:"Please import appointment"
                    },
                },
                submitHandler: function(form) {
                    var formData = new FormData();
                    formData.append("basis", $('#basis').val());
                    formData.append("name", $('#name').val());
                    formData.append("phone", $('#phone').val());
                    formData.append("email", $('#email').val());
                    formData.append("birthday", $('#birthday').val());
                    formData.append("content", $('#content').val());
                    formData.append("time_appointment", $('#time_appointment').val());

                    $.ajax({
                        url: '/submitAppointment',
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            var data = JSON.parse(response);
                            if (data.type === "warning") {
                                toastr.remove();
                                toastr[data.type](data.message);
                            } else if(data.type === "error") {
                                toastr.remove();
                                toastr[data.type](data.message);
                            } else {
                                $('#appointmentSuccess').modal('show');
                                $('#appointmentSuccess').on('hidden.bs.modal', function (e) {
                                    location.reload();
                                })
                            }
                        }, error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });
                }
            });
        </script>
    @endpush
    @push('scripts')
        <script>
            document.title = 'Dịch vụ y tế - Hồng Ngọc Hospital';
        </script>
    @endpush
@endsection
