@extends('hospital.layout')

@section('content')
    <style>
        .img-hober-banner img {
            width: 100%;
        }
    </style>
    <div class="container tab-intro-child-1 tab-intro-child-cck-1">
        <div class="row intro-content-child-1">
            <div class="col-md-12 col-center">
                <p class="img-hober-banner">
                    <img src="/{{ $banner->image }}">
                </p>
            </div>
        </div>
    </div>
    <div class="container introduction-child-tab84 introduction-child-cck-tab84">
        <div class="row introduction-child">
            <div class="col-md-8 introduction-child-left">
                <div class="row panel-title">
                    <h1>
                        {{ $articlesDetail->title }}
                    </h1>
                    <div style="font-size: 16px !important; font-weight: bold">
                        {!! $articlesDetail->summary !!}
                    </div>
                    <div style="font-size: 16px !important;">
                        {!! $articlesDetail->content !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4 introduction-child-right">
                <div id="search-form" class="search-form-wrapper open">
                    <div class="hoppicenter">
                        <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                        <button onclick="submitSearchArticles()">
                            Search
                        </button>
                    </div>
                </div>
                <h2 class="widget-title">
                    Bài viết mới
                </h2>
                <ul>
                    @foreach($articlesMore as $item)
                        <li>
                            <a href="javascript:void(0);"
                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                {{ $item->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            document.title = '{{ $articlesDetail->title }}';
        </script>
    @endpush
@endsection
