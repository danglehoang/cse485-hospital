@extends('hospital.layout')

@section('content')
    <style>
        .img-hober-banner img {
            width: 100%;
        }
    </style>
    <div class="container tab-intro-child-1 tab-intro-child-bsck-1">
        <div class="row intro-content-child-hnyn-1">
            <div class="col-md-12 col-center">
                <p class="img-hober-banner">
                    <img src="/{{ $banner->image }}" alt="emergency">
                </p>
            </div>
        </div>
    </div>
    <div class="container introduction-child introduction-child-hnyn">
        <div class="row introduction-child">
            <div class="col-md-8 introduction-child-left">
                <div class="container introduction-child-bsck">
                    <div class="row introduction-child-bsck-section">
                        <div class="col-md-4 introduction-child-right">
                            <div class="panel-title">
                                <a href="#" class="img-title-1">
                                    <img src="/{{ $detailDoctor->thumb }}" alt="emergency">
                                </a>
                            </div>
                            <div class="text-center">
                                <button class="more" data-toggle="modal" data-target="#">
                                    Đặt lịch hẹn khám
                                </button>
                            </div>
                        </div>
                        <div class="col-md-8 introduction-child-left">
                            <div class="introduction-child-right-bsck panel-title">
                                <h1>
                                    {{ $detailDoctor->name }}
                                </h1>
                            </div>
                            <div class="list-info">
                                <table cellpadding="10">
                                    <tbody>
                                    <tr>
                                        <td> <strong>Chuyên Khoa</strong></td>
                                        <td>{{ $detailDoctor->postsDepartment->name }}</td>
                                    </tr>
                                    <tr>
                                        <td> <strong>Ngôn ngữ</strong></td>
                                        <td>{{ $detailDoctor->language }}</td>
                                    </tr>
                                    <tr>
                                        <td> <strong>Học vấn</strong></td>
                                        <td>{!! $detailDoctor->education !!}</td>
                                    </tr>
                                    <tr>
                                        <td> <strong>Hoạt động chuyên ngành</strong></td>
                                        <td>{!! $detailDoctor->specialized_activities !!}</td>
                                    </tr>
                                    <tr>
                                        <td> <strong>Nghiên cứu chuyên sâu</strong></td>
                                        <td>{!! $detailDoctor->description !!}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="text-intro text-1">
                            <h3>
                                Thành tựu chuyên môn
                            </h3>
                            <div>
                                <td>{!! $detailDoctor->achievement !!}</td>
                            </div>
                        </div>
                        <div class="text-intro text-2">
                            <h3>
                                Tư vấn của bác sĩ
                            </h3>
                            <div>
                                <td>{!! $detailDoctor->advisory !!}</td>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 introduction-child-right">
                <div id="search-form" class="search-form-wrapper open">
                    <div class="hoppicenter">
                        <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                        <button onclick="submitSearchArticles()">
                            Search
                        </button>
                    </div>
                </div>
                <h2 class="widget-title">
                    Bài viết mới
                </h2>
                <ul>
                    @foreach($articlesMore as $item)
                        <li>
                            <a href="javascript:void(0);"
                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                {{ $item->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            document.title = '{{ $detailDoctor->name }}' + ' * Hồng Ngọc Hospital';
        </script>
    @endpush
@endsection
