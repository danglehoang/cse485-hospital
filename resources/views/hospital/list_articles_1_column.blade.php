@extends('hospital.layout')

@section('content')
    <div class="container tab-intro-child-1 tab-intro-child-hnyn-1">
        <div class="row intro-content-child-hnyn-1">
            <div class="col-md-12 col-center">
                <p class="img-hober-banner">
                    <img src="/{{ $banner->image }}" alt="emergency">
                </p>
            </div>
        </div>
    </div>
    <div class="container introduction-child-tab84 introduction-child-td-tab84">
        <div class="row introduction-child-td">
            <div class="col-md-8 introduction-child-left">
                <div class="row panel-title">
                    <h1>
                        TUYỂN DỤNG
                    </h1>
                </div>
                <div class="container introduction-child-td-3-9">
                    @foreach($listArticles as $item)
                        <div class="introduction-child-3-9-td">
                            <div class="row introduction-child-39-td-center">
                                <div class="col-md-4 introduction-child-3-9-left">
                                    <a href="javascript:void(0);"
                                       onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')" class="img-title-1">
                                        <img src="/{{ $item->thumb }}" alt="emergency">
                                    </a>
                                </div>
                                <div class="col-md-8 introduction-child-3-9-right">
                                    <div class="introduction-child-tab39-td">
                                        <a href="javascript:void(0);"
                                           onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')"
                                           class="title-1 content">
                                            <span>
                                                <strong>
                                                    {{ $item->title }}
                                                </strong>
                                            </span>
                                        </a>
                                        <div style="font-size: 16px;">
                                            {{ date('d/m/Y', strtotime($item->updated)) }}
                                        </div>
                                        <div style="font-size: 16px;">
                                            {!! $item->summary !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-4 introduction-child-right">
                <div id="search-form" class="search-form-wrapper open">
                    <div class="hoppicenter">
                        <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                        <button onclick="submitSearchArticles()">
                            Search
                        </button>
                    </div>
                </div>
                <h2 class="widget-title">
                    Bài viết mới
                </h2>
                <ul>
                    @foreach($articlesMore as $item)
                        <li>
                            <a href="javascript:void(0);"
                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                {{ $item->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
