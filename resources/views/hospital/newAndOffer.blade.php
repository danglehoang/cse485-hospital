@extends('hospital.layout')

@section('content')
    <div class="wrapper-news-offers">
        <div class="container tab-intro-child-1 tab-intro-child-cck-1 mt-4">
            <div class="row intro-content-child-1">
                <div class="col-md-12 col-center">
                    <p class="img-hober-banner">
                        <img src="/{{ $banner->image }}">
                    </p>
                </div>
            </div>
        </div>
        <div class="container introduction-child-tab84 introduction-child-cck-tab84">
            <div class="row introduction-child">
                <div class="col-md-8 news-offers-left">
                    <div class="highlight-news">
                        <div class="highlight-news-title">Tin nổi bật</div>
                        <div class="highlight-news-content">
                            <?php
                            $count_articles_hot = 1;
                            ?>
                                @foreach($articlesHot as $item)
                                    @if($count_articles_hot == 1)
                                        <?php
                                        $count_articles_hot++;
                                        ?>
                                        <div class="item-news big-news">
                                            <figure>
                                                <a href="javascript:void(0);"
                                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                                    <img src="/{{ $item->thumb }}">
                                                </a>
                                            </figure>
                                            <div class="title">
                                                <a href="javascript:void(0);"
                                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">{{ $item->title }}</a>
                                            </div>
                                        </div>
                                    @elseif($count_articles_hot > 1)
                                        <?php
                                        $count_articles_hot++;
                                        ?>
                                        <div class="item-news small-news">
                                            <figure>
                                                <a href="javascript:void(0);"
                                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                                    <img src="/{{ $item->thumb }}">
                                                </a>
                                            </figure>
                                            <div class="title">
                                                <a href="javascript:void(0);"
                                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">{{ $item->title }}</a>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                        </div>
                    </div>
                    @foreach($category as $item_category)
                        <div class="news-by-category">
                            <div class="category-title">
                                <a href="#">{{ $item_category->name }}</a>
                            </div>
                            <div class="category-content">
                                <?php
                                $listArticles = \App\Models\Articles::getListArticlesByCategoryId($item_category->id, $lang);
                                $count_articles = 1;
                                ?>
                                @foreach($listArticles as $item_articles)
                                    @if($count_articles <= 2)
                                        <?php
                                        $count_articles++;
                                        ?>
                                        <div class="item-news big-news">
                                            <figure>
                                                <a href="javascript:void(0);"
                                                   onclick="redirectDetailArticles('{{ $item_articles->id }}', '{{ $item_articles->slug }}')">
                                                    <img src="/{{ $item_articles->thumb }}" alt="PTM">
                                                </a>
                                            </figure>
                                            <div class="news-content">
                                                <div class="title">
                                                    <a href="javascript:void(0);"
                                                       onclick="redirectDetailArticles('{{ $item_articles->id }}', '{{ $item_articles->slug }}')">{{ $item_articles->title }}</a>
                                                </div>
                                                <div class="brief">
                                                    {!! $item_articles->summary !!}
                                                </div>
                                            </div>
                                        </div>
                                    @elseif($count_articles > 2)
                                        <?php
                                        $count_articles++;
                                        ?>
                                        <div class="item-news small-news">
                                            <figure>
                                                <a href="javascript:void(0);"
                                                   onclick="redirectDetailArticles('{{ $item_articles->id }}', '{{ $item_articles->slug }}')">
                                                    <img src="/{{ $item_articles->thumb }}" alt="Anh2">
                                                </a>
                                            </figure>
                                            <div class="news-content">
                                                <div class="title">
                                                    <a href="javascript:void(0);"
                                                       onclick="redirectDetailArticles('{{ $item_articles->id }}', '{{ $item_articles->slug }}')">{{ $item_articles->title }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-4 introduction-child-right">
                    <div id="search-form" class="search-form-wrapper open">
                        <div class="hoppicenter">
                            <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                            <button onclick="submitSearchArticles()">
                                Search
                            </button>
                        </div>
                    </div>
                    <h2 class="widget-title">
                        Bài viết mới
                    </h2>
                    <ul>
                        @foreach($articlesMore as $item)
                            <li>
                                <a href="javascript:void(0);"
                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                    {{ $item->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            document.title = 'Tin tức và ưu đãi - Hồng Ngọc Hospital';
        </script>
    @endpush
@endsection
