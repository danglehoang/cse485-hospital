@extends('hospital.layout')

@section('content')
    <div class="container tab-intro-child-1 tab-intro-child-cck-1 mt-4">
        <div class="row intro-content-child-1">
            <div class="col-md-12 col-center">
                <p class="img-hober-banner">
                    <img src="/{{ $banner->image }}">
                </p>
            </div>
        </div>
    </div>
    <div class="wrapper-question">
        <div class="container introduction-child-tab84 introduction-child-cck-tab84">
            <div class="row introduction-child">
                <div class="col-md-8 question-left">
                    <h1 class="question-title">
                        Câu hỏi thường gặp
                    </h1>
                    <div class="question-list">
                        <div id="accordion">
                            @foreach($faq as $key => $value)
                            <div class="card">
                                <div class="card-header" id="{{ $key }}_heading">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $key }}"
                                                aria-expanded="false" aria-controls="{{ $key }}">
                                            <span>{{ $value->title }}</span>
                                            <i class="fas" aria-hidden="true"></i>
                                        </button>
                                    </h5>
                                </div>

                                <div id="{{ $key }}" class="collapse" aria-labelledby="{{ $key }}_heading"
                                     data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="question">
                                            <div class="title">
                                                Câu hỏi
                                            </div>
                                            <div class="content">
                                                {!! $value->summary !!}
                                            </div>
                                        </div>
                                        <div class="answers">
                                            <div class="title">
                                                Trả lời:
                                            </div>
                                            <div class="content">
                                                {!! $value->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-4 introduction-child-right">
                    <div id="search-form" class="search-form-wrapper open">
                        <div class="hoppicenter">
                            <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                            <button onclick="submitSearchArticles()">
                                Search
                            </button>
                        </div>
                    </div>
                    <h2 class="widget-title">
                        Bài viết mới
                    </h2>
                    <ul>
                        @foreach($articlesMore as $item)
                            <li>
                                <a href="javascript:void(0);"
                                   onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                    {{ $item->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
