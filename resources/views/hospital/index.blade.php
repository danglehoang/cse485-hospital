<?php
$numberTab = 1;
$numberTabShow = 1;
?>

@extends('hospital.layout')

@section('content')
    <style>
        .wrapper-home .section-4 .tab-news .tabs .active {
            color: #00BB6D;
            font-weight: bold;
        }
    </style>
    <div class="wrapper-home">
        <div class="wrapper-home-banner">
            <div class="owl-carousel owl-theme">
                @foreach($banner_slide as $item)
                    <div class="item">
                        <a href="{{ $item->link }}">
                            <img src="{{ $item->image }}" alt="banner1">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="section-1 container">
            <div class="item-1 item">
                <a href="javascript:void(0);" id="btnItem1">
                    <img src="{{ asset('hospital/images/icon-3.svg') }}" class="normal">
                    <img src="{{ asset('hospital/images/icon-3-active.svg') }}" class="active">
                    <span>Giờ làm việc</span>
                </a>
                <div id="modalItem1" class="modal-item-1 modal">
                    <div class="modal-content">
                        <button class="close-modal"></button>
                        <div class="modal-body">
                            <h2 class="modal-title">
                                Giờ làm việc
                            </h2>
                            <p>
                                Với mong muốn mang lại cho quý khách hàng các dịch vụ y tế chất lượng cao, phục vụ kịp
                                thời
                                nhất, Bệnh viện Hồng Ngọc áp dụng giờ làm việc như sau:
                            </p>
                            <div class="list-time">
                                <p>
                                    <span class="text-left">Giờ làm việc hành chính: từ 7h00 - 16h50, ngoài giờ hành chính giá dịch vụ sẽ tăng 20%</span>
                                    <span class="text-right"></span>
                                </p>
                                @foreach($basis as $item)
                                    <p>
                                        <span class="text-left">{{ $item->name }}</span>
                                        <span class="text-right">{{ $item->working_time }}</span>
                                    </p>
                                @endforeach
                                <p>
                                    <span class="text-left">Cấp cứu 24/24</span>
                                    <span class="text-right"></span>
                                </p>
                                <div class="contact">
                                    <a href="tel: (+84)2439275568"> <img src="{{ asset('hospital/images/phone.png') }}">
                                        (+84) 24 3927
                                        5568</a>
                                    <span> Mọi thông tin chi tiết xin liên lạc với Hotline của Bệnh viện Hồng Ngọc. </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-2 item">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSfAZLvvy1xnxgzOsqTVRerkDur7v9sFrAw5gvoS1EK4N8xZoA/viewform?fbzx=739061430253993804">
                    <img src="{{ asset('hospital/images/icon-4.svg') }}" class="normal">
                    <img src="{{ asset('hospital/images/icon-4-active.svg') }}" class="active">
                    <span>Tờ khai y tế</span>
                </a>
            </div>
            <div class="item-3 item">
                <a href="javascript:void(0);" id="btnItem2">
                    <img src="{{ asset('hospital/images/icon-5.svg') }}" class="normal">
                    <img src="{{ asset('hospital/images/icon-5-active.svg') }}" class="active">
                    <span>Tìm bác sĩ</span>
                </a>
                <div id="modalItem2" class="modal-item-2 modal">
                    <div class="modal-content">
                        <button class="close-modal"></button>
                        <div class="modal-body">
                            <h2 class="modal-title">
                                Tìm bác sĩ
                            </h2>
                            <form action="">
                                <div class="form-group row">
                                    <label for="nameDoctor" class="col-md-4">Tìm kiếm theo tên bác sĩ</label>
                                    <input type="text" class="form-control col-md-8" id="nameDoctor"
                                           placeholder="Tìm kiếm theo tên bác sĩ">
                                </div>
                                <div class="form-group row">
                                    <label for="select1" class="col-md-4">Chọn theo Chuyên Khoa</label>
                                    <select class="form-control col-md-8" id="select1">
                                        <option>Chọn theo Chuyên khoa</option>
                                        @foreach($department as $item)
                                            <option>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group row">
                                    <label for="select2" class="col-md-4">Tìm kiếm theo cơ sở</label>
                                    <select class="form-control col-md-8" id="select2">
                                        <option>Tìm kiếm theo cơ sở</option>
                                        @foreach($basis as $item)
                                            <option>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button class="btn-search text-center">
                                    Tìm kiếm
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-4 item">
                <a href="javascript:void(0);" id="btnItem3">
                    <img src="{{ asset('hospital/images/icon-6.svg') }}" class="normal">
                    <img src="{{ asset('hospital/images/icon-6-active.svg') }}" class="active">
                    <span>Chi nhánh Hồng Ngọc</span>
                </a>
                <div id="modalItem3" class="modal-item-3 modal">
                    <div class="modal-content">
                        <button class="close-modal"></button>
                        <div class="modal-body">
                            <h2 class="modal-title">
                                Chi nhánh Hồng Ngọc
                            </h2>
                            <div class="row">
                                @foreach($basis as $item)
                                    <div class="col-md-6 modal-address">
                                        <img src="{{ asset('hospital/images/map.png') }}" alt="map">
                                        <div class="content">
                                            <strong>{{ $item->name }}</strong>
                                            <div class="address">
                                                {{ $item->address }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-5 item">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSd5eLB_h2ouxDdkq3d7PxLUmHr4BXZxkBijqY9H6DaBYiGhlQ/viewform">
                    <img src="{{ asset('hospital/images/icon-4.svg') }}" class="normal">
                    <img src="{{ asset('hospital/images/icon-4-active.svg') }}" class="active">
                    <span>Hòm thư góp ý</span>
                </a>
            </div>
        </div>
        <div class="section-2 container">
            <div class="row">
                <div class="col-md-3 intro">
                    <div class="title">
                        Dịch vụ Hồng Ngọc
                    </div>
                    <div class="content">
                        Thăm khám và điều trị tại Bệnh viện Đa khoa Hồng Ngọc, khách hàng sẽ được trải nghiệm các dịch
                        vụ
                        chăm sóc sức khỏe chất lương cao đạt tiêu chuẩn quốc tế.
                    </div>
                </div>
                <div class="col-md-9 slider-service">
                    <div class="owl-carousel owl-theme">
                        @foreach($banner_service as $item)
                            <div class="service-1">
                                <figure class="image-service">
                                    <img src="{{ $item->image }}">
                                </figure>
                                <div class="link-service">
                                    <img src="{{ asset('hospital/images/icon-9.svg') }}">
                                    <a href="{{ $item->link }}">{{ $item->name }}</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="section-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 data-left">
                        <div class="data-title">Số liệu nổi bật</div>
                        <div class="data-blog">
                            <div class="blog-1 blog">
                                <img src="{{ asset('hospital/images/icon-10.svg') }}">
                                <div class="blog-content">
                                    <div class="data">18</div>
                                    <div class="title">Năm thành lập</div>
                                </div>
                            </div>
                            <div class="blog-2 blog">
                                <img src="{{ asset('hospital/images/icon-12.svg') }}">
                                <div class="blog-content">
                                    <div class="data">206</div>
                                    <div class="title">Chuyên gia</div>
                                </div>
                            </div>
                            <div class="blog-3 blog">
                                <img src="{{ asset('hospital/images/icon-13.svg') }}">
                                <div class="blog-content">
                                    <div class="data">1.821.000</div>
                                    <div class="title"> Bệnh nhân</div>
                                </div>
                            </div>
                            <div class="blog-4 blog">
                                <img src="{{ asset('hospital/images/icon-14.svg') }}">
                                <div class="blog-content">
                                    <div class="data">7</div>
                                    <div class="title"> Cơ sở</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 data-right">
                        <p> Từ những ngày đầu đi vào hoạt động, Hồng Ngọc đã từng bước tạo dựng uy tín và danh tiếng với
                            mô
                            hình bệnh viện – khách sạn tiên phong ở Hà Nội cũng như toàn khu vực miền Bắc.</p>
                        <div class="blog-intro">
                            <img src="{{ asset('hospital/images/1f1e57470c04f75aae15.jpg') }}" alt="person">
                            <div class="intro-content">
                                <div class="intro-header">
                                    <div class="title-name">
                                        TTƯT. BSCK II CAO ĐỘC LẬP
                                    </div>
                                    <div class="title-position">
                                        Giám đốc BV Đa khoa Hồng Ngọc
                                    </div>
                                </div>
                                <div class="intro-text">
                                    <img src="{{ asset('hospital/images/icon-15.svg') }}">
                                    <p>
                                        Với đội ngũ bác sĩ giỏi chuyên môn, liên tục cập nhật những phương pháp điều trị
                                        hiện đại, Bệnh viện Hồng Ngọc luôn không ngừng phấn đấu để khẳng định sứ mệnh
                                        lớn
                                        lao mà mình theo đuổi bằng việc trở thành hệ thống y tế tư nhân hàng đầu Việt
                                        Nam.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-4 container">
            <div class="header-news">
                <div class="title">
                    Tin tức hoạt động
                </div>
                <a href="#" class="btn">Xem thêm</a>
            </div>
            <div class="tab-news">
                <div class="tabs">
                    @foreach($category as $item)
                        <button class="tablinks"
                                @if($numberTab == 1)
                                id="defaultTabs"
                                @endif
                                onclick="openTabs(event, 'tabs{{ $numberTab++ }}' )">{{ $item->name }}</button>
                    @endforeach
                </div>
                @foreach($category as $item)
                    <?php
                    $articles = \App\Models\Articles::getListArticlesByCategoryId($item->id, $lang);
                    ?>
                        <div id="tabs{{ $numberTabShow++ }}" class="tabcontent">
                            <div class="row">
                                @foreach($articles as $item)
                                <div class="col-md-4 news-detail">
                                    <a href="javascript:void(0);"
                                       onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')"
                                       class="image">
                                        <img src="{{ $item->thumb }}">
                                    </a>
                                    <div class="content">
                                        <div class="title">
                                            <a href="javascript:void(0);"
                                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                                {{ $item->title }}
                                            </a>
                                        </div>
                                        <div class="brief">
                                            {!! $item->summary !!}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                @endforeach
            </div>
        </div>
        <div class="section-5">
            <div class="wrapper-partner container">
                <div class="owl-carousel owl-theme">
                    @foreach($banner_partner as $item)
                        <div class="item">
                            <img src="{{ $item->image }}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="section-6">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.755724778281!2d105.84194631533224!3d21.042457892685462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abba7ba53e11%3A0x587dbd43c0113ca9!2zQuG7h25oIFZp4buHbiDEkGEgS2hvYSBI4buTbmcgTmfhu41j!5e0!3m2!1svi!2s!4v1578450310705!5m2!1svi!2s"
                frameborder="0" width="100%" height="550"></iframe>
        </div>
        <div class="chat-messenger">
            <a href="https://m.me/BenhvienHongNgoc">
                <img src="{{ asset('hospital/images/icon-chat.png') }}">
            </a>
        </div>
    </div>
@endsection
