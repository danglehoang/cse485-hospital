@extends('hospital.layout')

@section('content')
    <style>
        .introduction-gallery-item {
            margin-bottom: 1.5vw;
        }

        .img-hover {
            margin-bottom: 10px;
        }

        .img-hober-banner img {
            width: 100%;
        }
    </style>
    <div class="container tab-intro-content-1">
        <div class="row intro-content1">
            <div class="col-md-12 col-center">
                <p class="img-hober-banner">
                    <img src="/{{ $banner->image }}" alt="emergency">
                </p>
            </div>
        </div>
    </div>
    <div class="wrapper-intro-content">
        <div>
            <div class="content-introduction">
                <div class="container introduction-content-1">
                    <div class="row panel-title">
                        <h1>
                            {{ $articlesByCategory->title }}
                        </h1>
                    </div>
                </div>
            </div>
            <div>
                {!! $articlesByCategory->content !!}
            </div>
            <div class="banner-2-introduction">
                <div class="container tab-content-banner-2">
                    <div class="row content-2-banner">
                        <div class="col-md-12 col-center">
                            <div class="title-why-setion">
                                Vì sao chọn chúng tôi ?
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container introduction-reason">
                    <div class="row reason-4-img">
                        @foreach($slideIntro as $item)
                            <div class="col-md-3 col-left reason-left">
                                <div class="img-hover">
                                    <img src="/{{ $item->image }}">
                                </div>
                                <div class="reason-title">
                                    <h4>{{ $item->name }}</h4>
                                    <p>
                                        {!! $item->description !!}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="container introduction-charity">
                <div class="row introduction-charity6-6">
                    <div class="col-md-6 introduction-6 col-left">
                        <div class="title-section" style="max-width: 100%;">
                            <h2 class="section-title">Từ thiện HỒNG NGỌC</h2>
                            <p class="text-rection">
                                Ngoài sự nghiệp cung cấp giải pháp chăm sóc sức khỏe chất lượng cao cho người dân, Bệnh
                                viện
                                Hồng Ngọc còn tạo được điểm sáng trong hành trình của mình bằng những hoạt động từ
                                thiện, hỗ
                                trợ cộng đồng. <br> <br> Hồng Ngọc luôn sẵn sàng tổ chức hoặc kết nối với các tổ chức,
                                đoàn
                                thể nhằm tạo điều kiện cho những người có số phận kém may mắn được hỗ trợ bằng nhiều
                                hình
                                thức khác nhau: Phẫu thuật dị tật từ thiện cho các em nhỏ có hoàn cảnh khó khăn; khám
                                chữa
                                bệnh miễn phí cho người dân vùng sâu vùng xa; tặng quà cho các cựu chiến binh, bậc lão
                                thành
                                cách mạng, các gia đình có hoàn cảnh khó khăn trên địa bàn thủ đô; tham gia bảo trợ y tế
                                cho
                                các sự kiện lớn trong cộng đồng…<br> <br> Tất cả các hoạt động đó đều nhằm chia sẻ và
                                lan
                                tỏa tinh thần tương thân tương ái, yêu thương con người, vì một ngày mai tươi sáng
                                hơn.<br>
                            </p>
                            <div class="button-center">
                                <a href="#" class="more-hear">
                                    Xem thêm
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 introduction-6 col-right">
                        <div class="img-hover">
                            <img src="{{ asset('hospital/images/hoat-dong-tu-thien-1.jpg') }}">
                        </div>
                        <div class="container introduction-chidle">
                            <div class="row introduction-chidle-2">
                                <div class="col-left col-md-6 chidle-1">
                                    <img src="{{ asset('hospital/images/hoat-dong-tu-thien-2.jpg') }}">
                                </div>
                                <div class="col-left col-md-6 chidle-1">
                                    <img src="{{ asset('hospital/images/hoat-dong-tu-thien-3.jpg') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container introduction-gallery">
                <div class="gallery-title">
                    <h2>
                        Thư viện ảnh
                    </h2>
                </div>
                <div class="row">
                    @foreach($libImage as $item)
                        <div class="introduction-gallery-item col-6">
                            <div class="img-hover">
                                <a href="#">
                                    <img src="/{{ $item->image }}"
                                         style="width: 100%; height: 100%;">
                                </a>
                            </div>
                            <h4>
                                {{ $image->name }}
                            </h4>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            document.title = 'Giới thiệu';
        </script>
    @endpush
@endsection
