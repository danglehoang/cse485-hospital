@extends('hospital.layout')

@section('content')
    <style>
        .img-hober-banner img {
            width: 100%;
        }
    </style>
    <div class="container tab-intro-child-1 tab-intro-child-tvsk-1">
        <div class="row intro-content-child-1">
            <div class="col-md-12 col-center">
                <p class="img-hober-banner">
                    <img src="/{{ $banner->image }}">
                </p>
            </div>
        </div>
    </div>
    <div class="container introduction-child-tab84 introduction-child-tvsk-tab84">
        <div class="row introduction-child">
            <div class="col-md-8 introduction-child-left">
                <div class="row panel-title">
                    <h1>
                        {{ $category->name }}
                    </h1>
                </div>
                <div class="container introduction-child-tvsk-6-6">
                    <div class="row introduction-child-tvsk">
                        <?php
                        $count_articles = 1;
                        ?>
                        @foreach($listArticles as $item)
                            @if($count_articles == 1)
                                <?php
                                $count_articles++;
                                ?>
                                <div>
                                    <div class="banner-img-title-2">
                                        <p class="img-title-1">
                                            <a href="javascript:void(0);"
                                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                                <img src="/{{ $item->thumb }}" style="width: 760px;">
                                            </a>
                                        </p>
                                    </div>
                                    <h2>
                                        <a href="javascript:void(0);" style="text-decoration: none;color: black"
                                           onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                            {{ $item->title }}
                                        </a>
                                    </h2>
                                    <div style="font-size: 16px !important;">{!! $item->summary !!}</div>
                                </div>
                            @elseif($count_articles > 1)
                                <?php
                                $count_articles++;
                                ?>
                                <div class="col-md-6">
                                    <div class="banner-img-title-2">
                                        <p class="img-title-1">
                                            <a href="javascript:void(0);"
                                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                                <img src="/{{ $item->thumb }}" style=" max-width: 100%; height: 210px;">
                                            </a>
                                        </p>
                                    </div>
                                    <h2>
                                        <a href="javascript:void(0);" style="text-decoration: none;color: black"
                                           onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                            {{ $item->title }}
                                        </a>
                                    </h2>
                                    <div style="font-size: 16px !important;">{!! $item->summary !!}</div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-4 introduction-child-right">
                <div id="search-form" class="search-form-wrapper open">
                    <div class="hoppicenter">
                        <input type="text" class="search-field" placeholder="" id="search_articles" value="">
                        <button onclick="submitSearchArticles()">
                            Search
                        </button>
                    </div>
                </div>
                <h2 class="widget-title">
                    Bài viết mới
                </h2>
                <ul>
                    @foreach($articlesMore as $item)
                        <li>
                            <a href="javascript:void(0);"
                               onclick="redirectDetailArticles('{{ $item->id }}', '{{ $item->slug }}')">
                                {{ $item->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            document.title = '{{ $category->name }}';
        </script>
    @endpush
@endsection
