<?php
$basis = \App\Models\Basis::getBasis();
?>
<footer>
    <div class="introduction-child-footer">
        <div class="container">
            <div class="row wrapper_footer">
                <div class="col-md-4 wrapper_footer_left">
                    <p class="text-center">
                        <a href="#">
                            <img src="{{ asset('hospital/images/logo.png') }}">
                        </a>
                    </p>
                    <div class="wrapper_footer_left_contact">
                        <div class="wrapper_footer_left_contact_title">
                            <strong> Bệnh viên đa khoa hồng ngọc</strong>
                        </div>
                        <div class="wrapper_footer_left_contact_address">
                            <img src="{{ asset('hospital/images/icon-16.svg') }}">
                            <span>
                                Số 55 Yên Ninh, phường Trúc Bạch, quận Ba Đình, Hà Nội
                            </span>
                        </div>
                        <div class="wrapper_footer_left_contact_phone">
                            <img src="{{ asset('hospital/images/icon-17.svg') }}">
                            <span>024 3927 5568 ext 0</span>
                        </div>
                        <div class="wrapper_footer_left_contact_mail">
                            <img src="{{ asset('hospital/images/icon-18.svg') }}">
                            <span>info@hongngochospital.vn</span>
                        </div>
                    </div>
                    <ul class="wrapper_footer_left_menu list-inline">
                        <li class="wrapper_footer_left_menu_item">
                            <a href="#">
                                Tuyển dụng
                            </a>
                        </li>
                        <li class="wrapper_footer_left_menu_item">
                            <a href="#">
                                Tư vấn sức khỏe
                            </a>
                        </li>
                        <li class="wrapper_footer_left_menu_item">
                            <img src="{{ asset('hospital/images/logoSaleNoti.png') }}" width="200">
                        </li>
                    </ul>
                </div>
                <div class="col-md-8 wrapper_footer_right">
                    <div class="row">
                        @foreach($basis as $item)
                            <div class="col-md-6 pb-4">
                                <p class="wrapper_footer_right_title">
                                    <a href="javascript:void(0);" onclick="redirectArticlesByBasisId('{{ $item->id }}')">{{ $item->name }}</a>
                                </p>
                                <div class="wrapper_footer_left_contact_address">
                                    <img src="{{ asset('hospital/images/icon-16.svg') }}">
                                    <span>
                                    {{ $item->address }}
                                </span>
                                </div>
                                <div class="wrapper_footer_left_contact_phone">
                                    <img src="{{ asset('hospital/images/icon-17.svg') }}">
                                    <span>
                                    {{ $item->phone }}
                                </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="wrapper_footer_social text-center w-100">
                    <a href="#">
                        <img src="{{ asset('hospital/images/fb.svg') }}">
                    </a>
                    <a href="#">
                        <img src="{{ asset('hospital/images/twiter.svg') }}">
                    </a>
                    <a href="#">
                        <img src="{{ asset('hospital/images/LinkedIN.svg') }}">
                    </a>
                    <a href="#">
                        <img src="{{ asset('hospital/images/instagram.svg') }}">
                    </a>
                </div>
            </div>
        </div>
        <div class="wrapper_footer_copyright">
            COPYRIGHT @2015 BỆNH VIỆN HỒNG NGỌC. ALL RIGHTS RESERVED
        </div>
        <div class="nav-right">
            <img src="{{ asset('hospital/images/icon-19.svg') }}">
            <div class="contact">
                <p>
                    <img src="{{ asset('hospital/images/icon-2.svg') }}">
                    <span>024 3927 5568</span>
                </p>
            </div>
            <div class="mail">
                <p>
                    <img src="{{ asset('hospital/images/icon-20.svg') }}">
                    <span>
                        <a href="mailto:info@hongngochospital.vn">info@hongngochospital.vn</a>
                    </span>
                </p>
            </div>
        </div>
        <div class="phone-gif">
            <a href="tel:02439275568">
                <img src="{{ asset('hospital/images/phone.gif') }}">
            </a>
        </div>
    </div>
</footer>
