<?php
$menu = \App\Models\Menu::getMenu();
$lang = empty(session('website_language')) ? config('app.locale') : session('website_language');
?>
<script>
</script>
<header class="">
    <div class="container">
        <div class="wrapper_header row">
            <div class="col-md-1 wrapper_header_left">
                <a href="/">
                    <img src="{{ asset('hospital/images/logo.png') }}">
                </a>
            </div>
            <div class="col-md-11 wrapper_header_right">
                <div class="wrapper_header_service">
                    <p class="info1">
                        <img src="{{ asset('hospital/images/icon-1.svg') }}">
                        <span class="text-white pl-2">{{ trans('hospital.emergency') }} 1900 636 555</span>
                    </p>
                    <p class="info2 ml-3">
                        <img src="{{ asset('hospital/images/icon-2.svg') }}">
                        <span class="text-white pl-2">024 3927 5568</span>
                    </p>
                    <div class="ml-3 mb-0 search-form" style="line-height: 32px;">
                        &nbsp; <span>|</span> &nbsp;
                        <span class="search-form-tigger">
                                <a id="open-search" href="#" onclick="addOpenSearch()">
                                    <img src="{{ asset('hospital/images/icon-search.png') }}">
                                </a>
                            </span>
                        <div id="search-form" class="search-form-wrapper">
                            <form action="#">
                                <input type="text" class="search-field" placeholder="Tìm kiếm" value="">
                                <button>
                                    <i class="fas fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="switch-lang">
                        <a href="
                        @if($lang == "en")
                        {!! route('hospital.change_language', ['vi']) !!}
                        @else
                        {!! route('hospital.change_language', ['en']) !!}
                        @endif
                            ">
                            <img src="
                            @if($lang == "en")
                            {{ asset('hospital/images/pngegg.png') }}
                            @else
                            {{ asset('hospital/images/vietnam.png') }}
                            @endif
                                ">
                        </a>
                    </div>
                </div>
                <nav class="wrapper_header_navbar">
                    <ul class="list-inline d-flex mb-0">
                        @foreach($menu as $itemMenu)
                            <li class="wrapper_header_navbar_item {{ $itemMenu->menu_number == 3 ? '' : 'dropdown' }}"
                                id="menu_{{ $itemMenu->menu_number }}">
                                <a href="javascript:void(0);"
                                   onclick="redirectArticlesMenu('{{ $itemMenu->id }}', '{{ $itemMenu->slug }}', '{{ $itemMenu->menu_number }}')">{{ $itemMenu->name }}</a>
                                <ul class="dropdown-menu p-0">
                                    <?php
                                    $category = \App\Models\Categories::getCategoryByMenuHeader($itemMenu->id);
                                    ?>
                                    @foreach($category as $itemCategory)
                                        <li class="wrapper_header_navbar_item">
                                            <a href="javascript:void(0);"
                                               onclick="redirectArticles('{{ $itemCategory->id }}', '{{ $itemCategory->slug }}', '{{ $itemCategory->type }}', '{{ $itemMenu->menu_number }}', '{{ $itemMenu->slug }}')">{{ $itemCategory->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
        <div class="wrapper_header_mb">
            <div class="button_menu">
                <span class="top"></span>
                <span class="mid"></span>
                <span class="bottom"></span>
            </div>
            <div class="logo_mb">
                <a href="/">
                    <img src="{{ asset('hospital/images/logo.png') }}">
                </a>
            </div>
            <div class="wrapper_header_service_mb">
                <div class="ml-3 mb-0 search-form" style="line-height: 32px;">
                    <span class="search-form-tigger">
                        <a id="open-search-mb" href="#" onclick="addOpenSearchMB()">
                            <img src="{{ asset('hospital/images/icon-search.png') }}">
                        </a>
                    </span>
                    <div id="search-form-mb" class="search-form-wrapper">
                        <form action="#">
                            <input type="text" class="search-field" placeholder="Tìm kiếm" value="">
                            <button>
                                <i class="fas fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>

                <div class="switch-lang">
                    <a href="
                        @if($lang == "en")
                    {!! route('hospital.change_language', ['vi']) !!}
                    @else
                    {!! route('hospital.change_language', ['en']) !!}
                    @endif
                        ">
                        <img src="
                            @if($lang == "en")
                        {{ asset('hospital/images/pngegg.png') }}
                        @else
                        {{ asset('hospital/images/vietnam.png') }}
                        @endif
                            ">
                    </a>
                </div>
            </div>
        </div>
        <div class="wrapper_navbar_menu">
            <ul class="list-inline mb-0">
                @foreach($menu as $itemMenu)
                    <li class="wrapper_header_navbar_item {{ $itemMenu->menu_number == 3 ? '' : 'dropdown' }}"
                        id="menu_mb_{{ $itemMenu->menu_number }}">
                        <a href="javascript:void(0);"
                           onclick="redirectArticlesMenu('{{ $itemMenu->id }}', '{{ $itemMenu->slug }}', '{{ $itemMenu->menu_number }}')">{{ $itemMenu->name }}</a>
                        <ul class="dropdown-menu-mb p-0">
                            <?php
                            $category = \App\Models\Categories::getCategoryByMenuHeader($itemMenu->id);
                            ?>
                            @foreach($category as $itemCategory)
                                <li class="wrapper_header_navbar_item">
                                    <a href="javascript:void(0);"
                                       onclick="redirectArticles('{{ $itemCategory->id }}', '{{ $itemCategory->slug }}', '{{ $itemCategory->type }}', '{{ $itemMenu->menu_number }}', '{{ $itemMenu->slug }}')">{{ $itemCategory->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    </div>
</header>
