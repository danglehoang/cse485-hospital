<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="shortcut icon" href="{{ asset('hospital/images/logo.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    <link rel="stylesheet"
          href="{{ asset('hospital/libraries/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('hospital/libraries/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/toastr/toastr.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('hospital/css/style.css') }}">
    <title>Trang chủ</title>
</head>
<body>
@include('hospital.header')

@yield('content')

@include('hospital.footer')
<script src="{{ asset('hospital/js/jquery-3.2.1.slim.min.js') }}"></script>
<script src="{{ asset('hospital/js/popper.min.js') }}"></script>
<script src="{{ asset('hospital/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('hospital/libraries/OwlCarousel2-2.3.4/docs_src/assets/vendors/jquery.min.js') }}"></script>
<script src="{{ asset('hospital/libraries/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('admin//plugins/bootstrap/js/bootstrap.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script src="{{ asset('admin/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('hospital/js/home.js') }}"></script>
<script src="{{ asset('hospital/js/general.js') }}"></script>
@stack('scripts')

<script>
    csrf_token = '{{csrf_token()}}';
</script>
</body>
</html>
