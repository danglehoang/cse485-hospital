    <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Admin Hospital</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/toastr/toastr.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/overlayScrollbars/css/OverlayScrollbars.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css"/>
    <link rel="stylesheet" href="{{ asset('admin/css/css.css') }}">
</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper">
    <div class="col-sm-12 col-xs-12 float-left">
        <div id="form_search">
            <div class="card">
                <div class="card-body">
                    <h3>Filter</h3>
                    <div class="d-flex">
                        <div class="w-25">
                            <span style="font-weight: bold;">{{ trans('label.admin_name') }}</span><br>
                            <input type="text"
                                   name="name"
                                   id="name_search"
                                   placeholder="{{ trans('label.admin_modal_add_name_place_holder') }}"
                                   class="form-control w-100">
                        </div>
                        <div class="w-25" style="margin-left:100px;">
                            <span style="font-weight: bold;">{{ trans('label.admin_email') }}</span><br>
                            <input type="text"
                                   name="name"
                                   id="email_search"
                                   placeholder="{{ trans('label.admin_modal_add_email_place_holder') }}"
                                   class="form-control w-100">
                        </div>
                        <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_status') }}
                                        </span>
                            <br>
                            <select id="office_search" class="form-control w-100">
                                <option value="">Chọn phòng ban</option>
                                @foreach($office as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="w-100">
                        <button class="btn btn-info float-right" onclick="search_list()">
                            <i class="fa fa-search" aria-hidden="true"></i>SEARCH
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Họ và tên</th>
            <th scope="col">Chức vụ</th>
            <th scope="col">Phòng ban</th>
            <th scope="col">Email</th>
            <th scope="col">Số điện thoại</th>
            <th scope="col">Số điện thoại phòng ban</th>
        </tr>
        </thead>
        @include('list-data')
    </table>
</div>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script src="{{ asset('admin/plugins/jquery/jquery.js') }}"></script>
<script src="{{ asset('admin/plugins/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('admin//plugins/bootstrap/js/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('admin//plugins/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('admin/plugins/chart.js/Chart.js') }}"></script>
<script src="{{ asset('admin/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-buttons/js/dataTables.buttons.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-buttons/js/buttons.bootstrap4.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-buttons/js/buttons.colVis.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-buttons/js/buttons.flash.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-buttons/js/buttons.html5.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-buttons/js/buttons.print.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-autofill/js/dataTables.autoFill.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.js') }}"></script>
<script src="{{ asset('admin/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.js') }}"></script>
<script src="{{ asset('admin/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('admin/js/adminlte.js') }}"></script>
<script src="{{ asset('admin/js/main.js') }}"></script>
{{--    <script src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>--}}
<script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('admin/js/pages/list.js') }}"></script>

<script>
    csrf_token = '{{csrf_token()}}';
</script>

<script>
    var url_ajax = '<?php echo url('/search-list') ?>';
</script>

</body>
</html>
