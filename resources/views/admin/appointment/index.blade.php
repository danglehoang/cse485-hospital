@extends('admin.layouts.layout')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="text-transform: uppercase;">
                            {{ trans('label.admin_appointment') }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    {{ trans('label.admin_home') }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('label.admin_appointment') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group"></div>
                    </div>
                    <div class="col-sm-8 col-xs-12 float-right">
                        <button class="btn btn-default float-right" onclick="reload_table_after()">
                            <i class="fas fa-refresh"></i> {{ trans('label.admin_reload') }}
                        </button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="data-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('label.admin_name') }}</th>
                                    <th>{{ trans('label.admin_phone') }}</th>
                                    <th>{{ trans('label.admin_email') }}</th>
                                    <th>{{ trans('label.admin_updated') }}</th>
                                    <th style="text-align: center">{{ trans('label.admin_action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="title-form">DETAIL</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-right: 15px;padding-left: 15px;">
                    <?php echo Form::open(array('id' => 'form', 'class' => '')) ?>
                    <input type="hidden" class="id" name="id" value="">
                    <div class="row col-12">
                        <div class="col-4">
                            <th></th>
                            <th>{{ trans('label.admin_name') }}</th>
                            <th>{{ trans('label.admin_phone') }}</th>
                            <th>{{ trans('label.admin_email') }}</th>
                            <th>{{ trans('label.admin_updated') }}</th>
                            <th>{{ trans('label.admin_content') }}</th>
                        </div>
                        <div class="col-8">
                            <p name="name"></p>
                            <p name="phone"></p>
                            <p name="email"></p>
                            <p name="content"></p>
                        </div>
                    </div>
                    <?php echo Form::close() ?>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_appointment') }}';
        var url_ajax_load_datatable = '<?php echo url('/admin/appointment/ajax_list_data') ?>',
            url_ajax_show = '<?php echo url('admin/appointment/ajax_show/') ?>';
    </script>
@endsection
