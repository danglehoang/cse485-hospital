@extends('admin.layouts.layout')

@section('content')
    <style>
        .content-wrapper .content table tbody tr td:nth-child(1) {
            width: 5%;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="text-transform: uppercase;">
                            {{ trans('label.admin_banner') }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    {{ trans('label.admin_home') }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('label.admin_basis') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group"></div>
                    </div>
                    <div class="col-sm-8 col-xs-12 float-right">
                        <button class="btn btn-default float-right" onclick="reload_table_after()">
                            <i class="fas fa-refresh"></i> {{ trans('label.admin_reload') }}
                        </button>

                        @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
                            <button class="btn btn-success float-right" onclick="add_form()"
                                    style="margin-right: 10px;">
                                <i class="fas fa-plus"></i> {{ trans('label.admin_create') }}
                            </button>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 float-left">
                    <div id="form_search">
                        <div class="card">
                            <div class="card-body">
                                <h3>Filter</h3>
                                <div class="d-flex">
                                    <div class="w-25">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_type') }}
                                        </span>
                                        <br>
                                        <select id="type_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_type_place_holder') }}</option>
                                            @foreach($type as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_status') }}
                                        </span>
                                        <br>
                                        <select id="status_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_status_place_holder') }}</option>
                                            <option value="{{ \App\Models\Users::STATUS_ACTIVE_USER }}">
                                                {{ trans('label.admin_active') }}
                                            </option>
                                            <option value="{{ \App\Models\Users::STATUS_DEACTIVATE_USER }}">
                                                {{ trans('label.admin_deactivate') }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="data-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('label.admin_thumb') }}</th>
                                    <th>{{ trans('label.admin_type') }}</th>
                                    <th>{{ trans('label.admin_menu') }}</th>
                                    <th>{{ trans('label.admin_status') }}</th>
                                    @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
                                        <th>{{ trans('label.admin_action') }}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="title-form"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-right: 15px;padding-left: 15px;">
                    <?php echo Form::open(array('id' => 'form', 'class' => '')) ?>
                    <input type="hidden" class="id" name="id" value="0">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_name') }} :</label>
                                <input name="name"
                                       placeholder="{{ trans('label.admin_modal_add_name_place_holder') }}"
                                       class="form-control name"
                                       type="text"/>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_link') }} : </label>
                                <input name="link"
                                       placeholder="{{ trans('label.admin_modal_add_link_place_holder') }}"
                                       class="form-control link"
                                       type="text"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_type') }}</label>
                                <select class="form-control type" name="type">
                                    @foreach($type as $key => $value)
                                        <option value="{{ $key }}">
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_status') }} :</label>
                                <select class="form-control status"
                                        name="status">
                                    <option
                                        value="{{ \App\Models\Banner::STATUS_ACTIVE_BANNER }}">{{ trans('label.admin_active') }}</option>
                                    <option
                                        value="{{ \App\Models\Banner::STATUS_DEACTIVATE_BANNER }}">{{ trans('label.admin_deactivate') }}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_menu') }} :</label>
                                <select name="menu_id"
                                        class="form-control menu_id">
                                    <option value="empty"
                                            disabled
                                            selected>
                                        {{ trans('label.admin_modal_add_menu_place_holder') }}
                                    </option>
                                    @foreach ($menu as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <label>{{ trans('label.admin_thumb') }}</label>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6">
                                    <input type="file"
                                           name="image"
                                           onchange="readURL(this)"
                                           id="profile-img"
                                           multiple
                                           action="">
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <img src=""
                                         id="profile-img-tag"
                                         width="45px"
                                         alt=""
                                         class="img-tag"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12" style="margin-top: 20px">
                            <div class="form-group">
                                <label>{{ trans('label.admin_description') }}</label>
                                <div>
                                    <textarea name="description"
                                              class="textarea form-control description"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button"
                            id="btnSave"
                            onclick="save()"
                            class="btn btn-primary pull-left">
                        {{ trans('label.admin_modal_add_save') }}
                    </button>
                    <button type="button"
                            class="btn btn-danger"
                            data-dismiss="modal">
                        {{ trans('label.admin_modal_add_close') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_banner') }}';
        const title_add = '{{ trans('label.admin_modal_add') }}';
        const title_edit = '{{ trans('label.admin_modal_edit') }}';
        var url_ajax_load_datatable = '<?php echo url('/admin/banner/ajax_list_data') ?>',
            url_ajax_edit = '<?php echo url('admin/banner/ajax_edit/') ?>',
            url_ajax_add = '<?php echo url('admin/banner/ajax_add') ?>',
            url_ajax_update = '<?php echo url('admin/banner/ajax_update') ?>',
            url_ajax_delete = '<?php echo url('admin/banner/ajax_delete') ?>',
            url_ajax_search = '<?php echo url('admin/banner/ajax_list_data') ?>';
    </script>
@endsection
