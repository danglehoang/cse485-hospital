<?php
$user_login = (new App\Helper\GetUserLogin)->UserLogin();
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$slug = explode('/', $_SERVER['REQUEST_URI']);
$id = end($slug);
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img
                    src="{{ !empty($user_login['avatar']) ? '/' . $user_login['avatar'] : '/admin/img/user2-160x160.jpg' }}"
                    class="img-circle elevation-2" alt="">
            </div>
            <div class="info d-flex">
                <a href="#" class="d-block">
                    {{ !empty($user_login['full_name']) ? $user_login['full_name'] : 1 }}
                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="{{ route('admin.auth.logout') }}" style="color:red">{{ trans('label.admin_logout') }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <span style="color: white; font-weight: bold">{{ trans('label.final_exam') }}</span>
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.dashboard') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_dashboard') }}</p>
                    </a>
                </li>

                @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
                    <li class="nav-item">
                        <a href="{{ route('admin.users') }}" class="nav-link">
                            <i class="fa{{ $actual_link == route('admin.users') ? 's' : 'r' }} fa-circle nav-icon"></i>
                            <p>{{ trans('label.admin_user') }}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.role') }}" class="nav-link">
                            <i class="fa{{ $actual_link == route('admin.role') ? 's' : 'r' }} fa-circle nav-icon"></i>
                            <p>{{ trans('label.admin_role') }}</p>
                        </a>
                    </li>
                @endif

                <li class="nav-item">
                    <a href="{{ route('admin.menu') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.menu') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_menu') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.banner') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.banner') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_banner') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.articles') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.articles') || $actual_link == route('admin.articles.ajax_add') || $actual_link == url('admin/articles/ajax_edit/') . '/' . $id ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_articles') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.categories') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.categories') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_category') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.appointment') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.appointment') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_appointment') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.basis') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.basis') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_basis') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.department') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.department') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_department') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.doctor') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.doctor') || $actual_link == route('admin.doctor.ajax_add') || $actual_link == url('admin/doctor/ajax_edit/') . '/' . $id ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_doctor') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.actionHistory') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.actionHistory') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_action_history') }}</p>
                    </a>
                </li>

                <hr>
                <span style="color: white; font-weight: bold">{{ trans('label.mid_term_test') }}</span>
                <li class="nav-item">
                    <a href="{{ route('admin.cadres') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.cadres') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_cadres') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.office') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.office') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_office') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.department_after') }}" class="nav-link">
                        <i class="fa{{ $actual_link == route('admin.department_after') ? 's' : 'r' }} fa-circle nav-icon"></i>
                        <p>{{ trans('label.admin_department') }}</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
