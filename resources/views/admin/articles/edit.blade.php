@extends('admin.layouts.layout')

@section('content')
    @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 style="text-transform: uppercase;">
                                {{ trans('label.admin_articles') }}
                            </h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">
                                    <a href="#">
                                        {{ trans('label.admin_home') }}
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">
                                    {{ trans('label.admin_articles') }}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <div class="text-center">
                <h1 style="text-transform: uppercase;">
                    <span class="font-weight-bold"></span>
                    {{ trans('label.admin_edit_site') }}
                </h1>
                <hr/>
            </div>
            <div class="row container-fluid ">
                <div class="col-sm-12 col-md-12 col-12">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <form enctype="multipart/form-data" id="form_articles" class="row justify-content-center">
                                @csrf
                                <?php echo Form::open(array('id' => 'form', 'class' => '')) ?>
                                <input type="hidden" name="id" class="id" value="{{ $articles->id }}">
                                <div class="col-sm-5 col-md-5 col-5">
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_department_id') }} :</label>
                                        <select name="department_id"
                                                class="form-control"
                                                id="department_id">
                                            <option value="empty"
                                                    disabled
                                                    selected>
                                                {{ trans('label.admin_modal_add_department_id_place_holder') }}
                                            </option>
                                            @foreach ($department as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ ($item->id == old('item', $articles->department_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_category') }} :</label>
                                        <select name="category_id"
                                                class="form-control"
                                                id="category_id">
                                            <option value="empty"
                                                    disabled
                                                    selected>
                                                {{ trans('label.admin_modal_add_category_place_holder') }}
                                            </option>
                                            @foreach ($categories as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ ($item->id == old('item', $articles->category_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_basis_id') }} :</label>
                                        <select name="basis_id"
                                                class="form-control"
                                                id="basis_id">
                                            <option value="empty"
                                                    disabled
                                                    selected>
                                                {{ trans('label.admin_modal_add_basis_id_place_holder') }}
                                            </option>
                                            @foreach ($basis as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ ($item->id == old('item', $articles->basis_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group row">
                                        <div class="form-group col-6">
                                            <label>{{ trans('label.admin_thumb') }}</label>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <input type="file"
                                                           name="thumb"
                                                           onchange="readUrlThumb(this)"
                                                           id="profile-img-thumb"
                                                           multiple
                                                           value="{{ $articles->thumb }}"
                                                           action="">
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <img src="/{{ $articles->thumb }}"
                                                         style="margin-left: 125px;margin-top: -10px;"
                                                         id="profile-img-tag-thumb"
                                                         width="45px"
                                                         alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>{{ trans('label.admin_banner') }}</label>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-6">
                                                    <input type="file"
                                                           name="banner"
                                                           onchange="readUrlBanner(this)"
                                                           id="profile-img-banner"
                                                           multiple
                                                           value="{{ $articles->banner }}"
                                                           action="">
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <img src="/{{ $articles->banner }}"
                                                         style="margin-left: 125px;margin-top: -10px;"
                                                         id="profile-img-tag-banner"
                                                         width="45px"
                                                         alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-5 col-5">
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_slug') }} :</label>
                                        <div>
                                            <input id="slug"
                                                   placeholder="{{ trans('label.admin_modal_add_slug_place_holder') }}"
                                                   type="text"
                                                   class="form-control"
                                                   name="slug"
                                                   value="{{ $articles->slug }}"
                                                   required
                                                   autocomplete="slug"
                                                   autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_status') }} :</label>
                                        <select class="form-control"
                                                name="status"
                                                id="status">
                                            <option
                                                <?php if ($articles->status == \App\Models\Articles::STATUS_ACTIVE_ARTICLES) echo 'selected'; ?>
                                                value="{{ \App\Models\Articles::STATUS_ACTIVE_ARTICLES }}">{{ trans('label.admin_active') }}</option>
                                            <option
                                                <?php if ($articles->status == \App\Models\Articles::STATUS_DEACTIVATE_ARTICLES) echo 'selected'; ?>
                                                value="{{ \App\Models\Articles::STATUS_DEACTIVATE_ARTICLES }}">{{ trans('label.admin_deactivate') }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_is_hot') }} :</label>
                                        <select class="form-control"
                                                name="is_hot"
                                                id="is_hot">
                                            <option
                                                <?php if ($articles->is_hot == \App\Models\Articles::IS_HOT_ARTICLES) echo 'selected'; ?>
                                                value="{{ \App\Models\Articles::IS_HOT_ARTICLES }}">{{ trans('label.admin_yes') }}</option>
                                            <option
                                                <?php if ($articles->is_hot == \App\Models\Articles::IS_NOT_HOT_ARTICLES) echo 'selected'; ?>
                                                value="{{ \App\Models\Articles::IS_NOT_HOT_ARTICLES }}">{{ trans('label.admin_no') }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_type') }} :</label>
                                        <select class="form-control"
                                                name="type"
                                                id="type">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_type_place_holder') }}</option>
                                            @foreach($type as $key => $value)
                                                <option value="{{ $key }}" {{ ($key == old('item', $articles->type)) ? 'selected': '' }}>{{ $value }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-md-10 col-10">
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab">
                                            <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab"
                                               href="#vietnam">Tiếng Việt</a>
                                            <a class="nav-item nav-link" id="nav-kr-tab" data-toggle="tab"
                                               href="#england">Tiếng Anh</a>
                                        </div>
                                    </nav>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div id="vietnam" role="tabpanel"
                                             class="active show tab-pane fade col-sm-12 col-md-12 col-12">
                                            <input type="text" id="lang_code_vi" value="vi" hidden>
                                            <div class="form-group" style="margin-top: 10px">
                                                <label>{{ trans('label.admin_modal_add_title') }} :</label>
                                                <div>
                                                    <input id="title_vi"
                                                           placeholder="{{ trans('label.admin_modal_add_title_place_holder') }}"
                                                           type="text"
                                                           class="form-control"
                                                           name="title"
                                                           value="{{ $articles_vi->title }}"
                                                           required
                                                           autocomplete="title"
                                                           autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 15px;">
                                                <label>{{ trans('label.admin_modal_add_summary') }} :</label>
                                                <div>
                                                    <textarea name="summary"
                                                              class="textarea form-control "
                                                              id="summary_vi"
                                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                        {!! $articles_vi->summary !!}
                                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 15px;">
                                                <label>{{ trans('label.admin_modal_add_content') }} :</label>
                                                <div>
                                                    <textarea name="content"
                                                              class="textarea form-control"
                                                              id="content_vi"
                                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                        {!! $articles_vi->content !!}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="england" role="tabpanel"
                                             class="tab-pane fade col-sm-12 col-md-12 col-12">
                                            <input type="text" id="lang_code_en" value="en" hidden>
                                            <div class="form-group" style="margin-top: 10px">
                                                <label>{{ trans('label.admin_modal_add_title') }} :</label>
                                                <div>
                                                    <input id="title_en"
                                                           placeholder="{{ trans('label.admin_modal_add_title_place_holder') }}"
                                                           type="text"
                                                           class="form-control"
                                                           name="title"
                                                           value="{{ $articles_en->title }}"
                                                           required
                                                           autocomplete="title"
                                                           autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 15px;">
                                                <label>{{ trans('label.admin_modal_add_summary') }} :</label>
                                                <div>
                                                    <textarea name="summary"
                                                              class="textarea form-control "
                                                              id="summary_en"
                                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                        {!! $articles_en->summary !!}
                                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 15px;">
                                                <label>{{ trans('label.admin_modal_add_content') }} :</label>
                                                <div>
                                                    <textarea name="content"
                                                              class="textarea form-control"
                                                              id="content_en"
                                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                        {!! $articles_en->content !!}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex col-sm-10 col-md-10 col-10"
                                     style="justify-content:space-evenly; margin-bottom: 20px;">
                                    <button type="button"
                                            id="btnSave"
                                            onclick="save_edit()"
                                            class="btn btn-primary pull-left">
                                        {{ trans('label.admin_modal_add_save') }}
                                    </button>
                                    <a href="/admin/articles" class="btn btn-danger">
                                        {{ trans('label.admin_modal_add_close') }}
                                    </a>
                                </div>
                                <?php echo Form::close() ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @push('scripts')
            <script>
                document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_edit_site') }}';
                var url_ajax_update = '<?php echo url('admin/articles/ajax_update') ?>',
                    page_curent = 'edit';
            </script>
        @endpush
    @endif
@endsection
