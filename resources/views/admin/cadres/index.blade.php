@extends('admin.layouts.layout')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="text-transform: uppercase;">
                            {{ trans('label.admin_cadres') }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    {{ trans('label.admin_home') }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('label.admin_cadres') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group"></div>
                    </div>
                    <div class="col-sm-8 col-xs-12 float-right">
                        <button class="btn btn-default float-right" onclick="reload_table_after()">
                            <i class="fas fa-refresh"></i> {{ trans('label.admin_reload') }}
                        </button>
                        <button class="btn btn-success float-right" onclick="add_form()"
                                style="margin-right: 10px;">
                            <i class="fas fa-plus"></i> {{ trans('label.admin_create') }}
                        </button>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 float-left">
                    <div id="form_search">
                        <div class="card">
                            <div class="card-body">
                                <h3>Filter</h3>
                                <div class="d-flex">
                                    <div class="w-25">
                                        <span style="font-weight: bold;">{{ trans('label.admin_name') }}</span><br>
                                        <input type="text"
                                               name="name"
                                               id="name_search"
                                               placeholder="{{ trans('label.admin_modal_add_name_place_holder') }}"
                                               class="form-control w-100">
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">{{ trans('label.admin_email') }}</span><br>
                                        <input type="text"
                                               name="name"
                                               id="email_search"
                                               placeholder="{{ trans('label.admin_modal_add_email_place_holder') }}"
                                               class="form-control w-100">
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_status') }}
                                        </span>
                                        <br>
                                        <select id="status_search" class="form-control w-100">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_status_place_holder') }}</option>
                                            <option value="{{ \App\Models\Office::STATUS_ACTIVE_OFFICE }}">
                                                {{ trans('label.admin_active') }}
                                            </option>
                                            <option value="{{ \App\Models\Office::STATUS_DEACTIVATE_OFFICE }}">
                                                {{ trans('label.admin_deactivate') }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="w-100">
                                    <button class="btn btn-info float-right" onclick="search_item()"
                                            style="outline: none;outline-offset:0;">
                                        <i class="fa fa-search" aria-hidden="true"></i>SEARCH
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="data-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('label.admin_name') }}</th>
                                    <th>{{ trans('label.admin_email') }}</th>
                                    <th>{{ trans('label.admin_phone') }}</th>
                                    <th>Chức vụ</th>
                                    <th>Phòng ban</th>
                                    <th>{{ trans('label.admin_status') }}</th>
                                    <th>{{ trans('label.admin_updated') }}</th>
                                    <th style="text-align: center">{{ trans('label.admin_action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="title-form"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-right: 15px;padding-left: 15px;">
                    <?php echo Form::open(array('id' => 'form', 'class' => '')) ?>
                    <input type="hidden" class="id" name="id" value="0">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_name') }} :</label>
                                <input name="name"
                                       placeholder="{{ trans('label.admin_modal_add_name_place_holder') }}"
                                       class="form-control name"
                                       type="text"/>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_email') }} : </label>
                                <input name="email"
                                       placeholder="{{ trans('label.admin_modal_add_email_place_holder') }}"
                                       class="form-control email"
                                       type="email"/>
                            </div>
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_phone') }} : </label>
                                <input name="phone"
                                       placeholder="{{ trans('label.admin_modal_add_phone_place_holder') }}"
                                       class="form-control phone"
                                       type="number"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>{{ trans('label.admin_modal_add_status') }} :</label>
                                <select class="form-control status"
                                        name="status">
                                    <option
                                        value="{{ \App\Models\Cadres::STATUS_ACTIVE_CADRES }}">{{ trans('label.admin_active') }}</option>
                                    <option
                                        value="{{ \App\Models\Cadres::STATUS_DEACTIVATE_CADRES }}">{{ trans('label.admin_deactivate') }}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Chức vụ :</label>
                                <select class="form-control department_id"
                                        name="department_id">
                                    <option value="">Chọn chức vụ</option>
                                    @foreach($department as $item)
                                        <?php
                                        $office = \App\Models\Office::where('id', $item->office_id)-> first();
                                        ?>
                                        <option value="{{ $item->id }}">{{ $item->name }} ({{ $office->name }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button"
                            id="btnSave"
                            onclick="save()"
                            class="btn btn-primary pull-left">
                        {{ trans('label.admin_modal_add_save') }}
                    </button>
                    <button type="button"
                            class="btn btn-danger"
                            data-dismiss="modal">
                        {{ trans('label.admin_modal_add_close') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_cadres') }}';
        const title_add = '{{ trans('label.admin_modal_add') }}';
        const title_edit = '{{ trans('label.admin_modal_edit') }}';
        var url_ajax_load_datatable = '<?php echo url('/admin/cadres/ajax_list_data') ?>',
            url_ajax_edit = '<?php echo url('admin/cadres/ajax_edit/') ?>',
            url_ajax_add = '<?php echo url('admin/cadres/ajax_add') ?>',
            url_ajax_update = '<?php echo url('admin/cadres/ajax_update') ?>',
            url_ajax_delete = '<?php echo url('admin/cadres/ajax_delete') ?>',
            url_ajax_search = '<?php echo url('admin/cadres/ajax_list_data') ?>';
    </script>
@endsection
