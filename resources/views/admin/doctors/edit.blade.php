@extends('admin.layouts.layout')

@section('content')
    @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 style="text-transform: uppercase;">
                                {{ trans('label.admin_doctor') }}
                            </h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">
                                    <a href="#">
                                        {{ trans('label.admin_home') }}
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">
                                    {{ trans('label.admin_doctor') }}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>
            <div class="text-center">
                <h1 style="text-transform: uppercase;">
                    <span class="font-weight-bold"></span>
                    {{ trans('label.admin_edit_site') }}
                </h1>
                <hr/>
            </div>
            <div class="row container-fluid ">
                <div class="col-sm-12 col-md-12 col-12">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <form enctype="multipart/form-data" id="form_articles" class="row justify-content-center">
                                @csrf
                                <?php echo Form::open(array('id' => 'form', 'class' => '')) ?>
                                <input type="hidden" name="id" class="id" value="{{ $doctor->id }}">
                                <div class="col-sm-5 col-md-5 col-5">
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_name') }} :</label>
                                        <div>
                                            <input placeholder="{{ trans('label.admin_modal_add_name_place_holder') }}"
                                                   type="text"
                                                   class="form-control"
                                                   name="name"
                                                   id="name"
                                                   value="{{ $doctor->name }}"
                                                   required
                                                   autocomplete="name"
                                                   autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_slug') }} :</label>
                                        <div>
                                            <input placeholder="{{ trans('label.admin_modal_add_slug_place_holder') }}"
                                                   type="text"
                                                   class="form-control"
                                                   name="slug"
                                                   id="slug"
                                                   value="{{ $doctor->slug }}"
                                                   required
                                                   autocomplete="slug"
                                                   autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_level') }} :</label>
                                        <div>
                                            <input placeholder="{{ trans('label.admin_modal_add_level_place_holder') }}"
                                                   type="text"
                                                   class="form-control"
                                                   name="level"
                                                   id="level"
                                                   value="{{ $doctor->level }}"
                                                   required
                                                   autocomplete="level"
                                                   autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_basis_id') }} :</label>
                                        <select name="basis_id"
                                                id="basis_id"
                                                class="form-control">
                                            @foreach ($basis as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ ($item->id == old('item', $doctor->basis_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_thumb') }}</label>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <input type="file"
                                                       name="thumb"
                                                       onchange="readURL(this)"
                                                       id="profile-img"
                                                       multiple
                                                       value="{{ $doctor->thumb }}"
                                                       action="">
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <img src="/{{ $doctor->thumb }}"
                                                     style="margin-left: 125px;margin-top: -10px;"
                                                     id="profile-img-tag"
                                                     width="45px"
                                                     alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-5 col-5">
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_language') }} :</label>
                                        <select name="language"
                                                id="language"
                                                class="form-control">
                                            <option value="empty"
                                                    disabled
                                                    selected>
                                                {{ trans('label.admin_modal_add_language_place_holder') }}
                                            </option>
                                            @foreach ($language as $key => $item)
                                                <option
                                                    value="{{ $key }}" {{ ($key == old('key', $doctor->language)) ? 'selected': '' }}>{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_department_id') }} :</label>
                                        <select name="department_id"
                                                id="department_id"
                                                class="form-control">
                                            <option value="empty"
                                                    disabled
                                                    selected>
                                                {{ trans('label.admin_modal_add_department_id_place_holder') }}
                                            </option>
                                            @foreach ($department as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ ($item->id == old('item', $doctor->department_id)) ? 'selected': '' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ trans('label.admin_modal_add_status') }} :</label>
                                        <select class="form-control"
                                                id="status"
                                                name="status">
                                            <option
                                                <?php if ($doctor->status == \App\Models\Doctors::STATUS_ACTIVE_DOCTOR) echo 'selected'; ?>
                                                value="{{ \App\Models\Doctors::STATUS_ACTIVE_DOCTOR }}">
                                                {{ trans('label.admin_active') }}
                                            </option>
                                            <option
                                                <?php if ($doctor->status == \App\Models\Doctors::STATUS_DEACTIVATE_DOCTOR) echo 'selected'; ?>
                                                value="{{ \App\Models\Doctors::STATUS_DEACTIVATE_DOCTOR }}">
                                                {{ trans('label.admin_deactivate') }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-10 col-md-10 col-10" style="margin-top: 15px;">
                                    <label>{{ trans('label.admin_modal_add_education') }} :</label>
                                    <div>
                                    <textarea name="education"
                                              id="education"
                                              class="textarea form-control"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                        <?= $doctor->education ?>
                                    </textarea>
                                    </div>
                                </div>
                                <div class="form-group col-sm-10 col-md-10 col-10" style="margin-top: 15px;">
                                    <label>{{ trans('label.admin_modal_add_specialized_activities') }} :</label>
                                    <div>
                                    <textarea name="specialized_activities"
                                              id="specialized_activities"
                                              class="textarea form-control"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                        <?= $doctor->specialized_activities ?>
                                    </textarea>
                                    </div>
                                </div>
                                <div class="form-group col-sm-10 col-md-10 col-10" style="margin-top: 15px;">
                                    <label>{{ trans('label.admin_modal_add_achievement') }} :</label>
                                    <div>
                                    <textarea name="achievement"
                                              id="achievement"
                                              class="textarea form-control"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                        <?= $doctor->achievement ?>
                                    </textarea>
                                    </div>
                                </div>
                                <div class="form-group col-sm-10 col-md-10 col-10" style="margin-top: 15px;">
                                    <label>{{ trans('label.admin_modal_add_advisory') }} :</label>
                                    <div>
                                    <textarea name="advisory"
                                              id="advisory"
                                              class="textarea form-control"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                        <?= $doctor->advisory ?>
                                    </textarea>
                                    </div>
                                </div>
                                <div class="form-group col-sm-10 col-md-10 col-10" style="margin-top: 15px;">
                                    <label>{{ trans('label.admin_modal_add_description') }} :</label>
                                    <div>
                                    <textarea name="description"
                                              id="description"
                                              class="textarea form-control"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                        <?= $doctor->description ?>
                                    </textarea>
                                    </div>
                                </div>
                                <div class="d-flex col-sm-10 col-md-10 col-10"
                                     style="justify-content:space-evenly; margin-bottom: 20px;">
                                    <button type="button"
                                            id="btnSave"
                                            onclick="save_edit()"
                                            class="btn btn-primary pull-left">
                                        {{ trans('label.admin_modal_add_save') }}
                                    </button>
                                    <a href="/admin/doctor" class="btn btn-danger">
                                        {{ trans('label.admin_modal_add_close') }}
                                    </a>
                                </div>
                                <?php echo Form::close() ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @push('scripts')
            <script>
                document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_edit_site') }}';
                var url_ajax_update = '<?php echo url('admin/doctor/ajax_update') ?>',
                    page_curent = 'edit';
            </script>
        @endpush
    @endif
@endsection
