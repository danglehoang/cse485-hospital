@extends('admin.layouts.layout')

@section('content')
    <style>
        .content-wrapper .content table tbody tr td:nth-child(1) {
            width: 5%;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="text-transform: uppercase;">
                            {{ trans('label.admin_doctor') }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    {{ trans('label.admin_home') }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('label.admin_doctor') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group"></div>
                    </div>
                    <div class="col-sm-8 col-xs-12 float-right">
                        <button class="btn btn-default float-right" onclick="reload_table_after()">
                            <i class="fas fa-refresh"></i> {{ trans('label.admin_reload') }}
                        </button>

                        @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
                            <button class="btn btn-success float-right" onclick="add_form()"
                                    style="margin-right: 10px;">
                                <i class="fas fa-plus"></i> {{ trans('label.admin_create') }}
                            </button>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 float-left">
                    <div id="form_search">
                        <div class="card">
                            <div class="card-body">
                                <h3>Filter</h3>
                                <div class="d-flex">
                                    <div class="w-25">
                                        <span style="font-weight: bold;">{{ trans('label.admin_name') }}</span><br>
                                        <input type="text"
                                               name="name"
                                               id="name_search"
                                               onchange="submitForm()"
                                               placeholder="{{ trans('label.admin_modal_add_name_place_holder') }}"
                                               class="form-control w-100">
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_department') }}
                                        </span>
                                        <br>
                                        <select id="department_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_department_id_place_holder') }}</option>
                                            @foreach($department as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_basis') }}
                                        </span>
                                        <br>
                                        <select id="basis_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_basis_id_place_holder') }}</option>
                                            @foreach($basis as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_language') }}
                                        </span>
                                        <br>
                                        <select id="language_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_language_place_holder') }}</option>
                                            @foreach($language as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_status') }}
                                        </span>
                                        <br>
                                        <select id="status_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_status_place_holder') }}</option>
                                            <option value="{{ \App\Models\Articles::STATUS_ACTIVE_ARTICLES }}">
                                                {{ trans('label.admin_active') }}
                                            </option>
                                            <option value="{{ \App\Models\Articles::STATUS_DEACTIVATE_ARTICLES }}">
                                                {{ trans('label.admin_deactivate') }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="data-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('label.admin_thumb') }}</th>
                                    <th>{{ trans('label.admin_name') }}</th>
                                    <th>{{ trans('label.admin_department_id') }}</th>
                                    <th>{{ trans('label.admin_basis_id') }}</th>
                                    <th>{{ trans('label.admin_language') }}</th>
                                    <th>{{ trans('label.admin_status') }}</th>
                                    @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
                                        <th class="text-center">{{ trans('label.admin_action') }}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_doctor') }}';
        const url_ajax_load_datatable = '<?php echo url('/admin/doctor/ajax_list_data') ?>',
            url_ajax_edit = '<?php echo url('admin/doctor/ajax_edit/') ?>',
            url_ajax_add = '<?php echo url('admin/doctor/ajax_add') ?>',
            url_add = '<?php echo url('admin/doctor/add') ?>',
            url_ajax_update = '<?php echo url('admin/doctor/ajax_update') ?>',
            url_ajax_delete = '<?php echo url('admin/doctor/ajax_delete') ?>',
            page_curent = 'index',
            url_ajax_search = '<?php echo url('admin/doctor/ajax_list_data') ?>';
    </script>

@endsection
