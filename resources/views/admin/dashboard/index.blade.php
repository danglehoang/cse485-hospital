@extends('admin.layouts.layout')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="text-transform: uppercase;">
                            {{ trans('label.admin_dashboard') }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    {{ trans('label.admin_home') }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('label.admin_dashboard') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    @if($user_login['detailRole']['type'] == \App\Models\Role::TYPE_ADMIN )
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3" style="background-color: #343a40; color: white">
                                <span class="info-box-icon bg-warning elevation-1">
                                    <i class="fas fa-users"></i>
                                </span>
                                <div class="info-box-content">
                                    <span class="info-box-text">{{ trans('label.admin_user') }}</span>
                                    <span class="info-box-number">{{ $users }}</span>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box"  style="background-color: #343a40; color: white">
                                <span class="info-box-icon bg-info elevation-1">
                                    <i class="fas fa-cog"></i>
                                </span>
                            <div class="info-box-content">
                                <span class="info-box-text">{{ trans('label.admin_articles') }}</span>
                                <span class="info-box-number">{{ $articles }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Latest Members</h3>
                                <div class="card-tools">
                                    <span class="badge badge-danger">Members</span>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <ul class="users-list clearfix">
                                    @foreach($listUserNew as $item)
                                        <li>
                                            <img class="w-25" src="/{{ $item->avatar }}">
                                            <a class="users-list-name" href="#">{{ $item->full_name }}</a>
                                            <span class="users-list-date">{{ date('d/m/Y', strtotime($item->created)) }}</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-footer text-center">
                                <a href="{{ route('admin.users') }}">View All Users</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card bg-gradient-secondary">
                            <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">
                                <h3 class="card-title">
                                    <i class="far fa-calendar-alt"></i>
                                    Calendar
                                </h3>
                                <div class="card-tools">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-light btn-sm dropdown-toggle"
                                                data-toggle="dropdown" data-offset="-52" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" role="menu" style="">
                                            <a href="#" class="dropdown-item">Add new event</a>
                                            <a href="#" class="dropdown-item">Clear events</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="#" class="dropdown-item">View calendar</a>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-light btn-sm" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-light btn-sm" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <div id="calendar" style="width: 100%">
                                    <div class="bootstrap-datetimepicker-widget usetwentyfour">
                                        <ul class="list-unstyled">
                                            <li class="show">
                                                <div class="datepicker">
                                                    <div class="datepicker-days" style="">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                        class="fa fa-chevron-left"
                                                                        title="Previous Month"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5"
                                                                    title="Select Month">August 2021
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                        class="fa fa-chevron-right"
                                                                        title="Next Month"></span>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="dow">Su</th>
                                                                <th class="dow">Mo</th>
                                                                <th class="dow">Tu</th>
                                                                <th class="dow">We</th>
                                                                <th class="dow">Th</th>
                                                                <th class="dow">Fr</th>
                                                                <th class="dow">Sa</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="08/01/2021"
                                                                    class="day weekend">1
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/02/2021"
                                                                    class="day">
                                                                    2
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/03/2021"
                                                                    class="day">
                                                                    3
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/04/2021"
                                                                    class="day">
                                                                    4
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/05/2021"
                                                                    class="day">
                                                                    5
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/06/2021"
                                                                    class="day">
                                                                    6
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/07/2021"
                                                                    class="day weekend">7
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="08/08/2021"
                                                                    class="day weekend">8
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/09/2021"
                                                                    class="day">
                                                                    9
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/10/2021"
                                                                    class="day">
                                                                    10
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/11/2021"
                                                                    class="day">
                                                                    11
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/12/2021"
                                                                    class="day">
                                                                    12
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/13/2021"
                                                                    class="day">
                                                                    13
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/14/2021"
                                                                    class="day weekend">14
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="08/15/2021"
                                                                    class="day weekend">15
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/16/2021"
                                                                    class="day">
                                                                    16
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/17/2021"
                                                                    class="day">
                                                                    17
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/18/2021"
                                                                    class="day">
                                                                    18
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/19/2021"
                                                                    class="day">
                                                                    19
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/20/2021"
                                                                    class="day">
                                                                    20
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/21/2021"
                                                                    class="day weekend">21
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="08/22/2021"
                                                                    class="day weekend">22
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/23/2021"
                                                                    class="day">
                                                                    23
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/24/2021"
                                                                    class="day">
                                                                    24
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/25/2021"
                                                                    class="day">
                                                                    25
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/26/2021"
                                                                    class="day">
                                                                    26
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/27/2021"
                                                                    class="day">
                                                                    27
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/28/2021"
                                                                    class="day active today weekend">28
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="08/29/2021"
                                                                    class="day weekend">29
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/30/2021"
                                                                    class="day">
                                                                    30
                                                                </td>
                                                                <td data-action="selectDay" data-day="08/31/2021"
                                                                    class="day">
                                                                    31
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/01/2021"
                                                                    class="day new">1
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/02/2021"
                                                                    class="day new">2
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/03/2021"
                                                                    class="day new">3
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/04/2021"
                                                                    class="day new weekend">4
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="09/05/2021"
                                                                    class="day new weekend">5
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/06/2021"
                                                                    class="day new">6
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/07/2021"
                                                                    class="day new">7
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/08/2021"
                                                                    class="day new">8
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/09/2021"
                                                                    class="day new">9
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/10/2021"
                                                                    class="day new">10
                                                                </td>
                                                                <td data-action="selectDay" data-day="09/11/2021"
                                                                    class="day new weekend">11
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="datepicker-months" style="display: none;">
                                                        <table class="table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                        class="fa fa-chevron-left"
                                                                        title="Previous Year"></span>
                                                                </th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5"
                                                                    title="Select Year">2021
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                        class="fa fa-chevron-right"
                                                                        title="Next Year"></span>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="7"><span data-action="selectMonth"
                                                                                      class="month">Jan</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Feb</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Mar</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Apr</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">May</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Jun</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Jul</span><span
                                                                        data-action="selectMonth"
                                                                        class="month active">Aug</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Sep</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Oct</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Nov</span><span
                                                                        data-action="selectMonth"
                                                                        class="month">Dec</span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="datepicker-years" style="display: none;">
                                                        <table class="table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                        class="fa fa-chevron-left"
                                                                        title="Previous Decade"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5"
                                                                    title="Select Decade">2020-2029
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                        class="fa fa-chevron-right"
                                                                        title="Next Decade"></span>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="7"><span data-action="selectYear"
                                                                                      class="year old">2019</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2020</span><span
                                                                        data-action="selectYear"
                                                                        class="year active">2021</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2022</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2023</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2024</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2025</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2026</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2027</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2028</span><span
                                                                        data-action="selectYear"
                                                                        class="year">2029</span><span
                                                                        data-action="selectYear"
                                                                        class="year old">2030</span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="datepicker-decades" style="display: none;">
                                                        <table class="table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                        class="fa fa-chevron-left"
                                                                        title="Previous Century"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5">2000-2090
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                        class="fa fa-chevron-right"
                                                                        title="Next Century"></span>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="7">
                                                                <span data-action="selectDecade"
                                                                      class="decade old"
                                                                      data-selection="2006">
                                                                    1990
                                                                </span>
                                                                    <span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2006">2000</span><span
                                                                        data-action="selectDecade" class="decade active"
                                                                        data-selection="2016">2010</span><span
                                                                        data-action="selectDecade" class="decade active"
                                                                        data-selection="2026">2020</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2036">2030</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2046">2040</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2056">2050</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2066">2060</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2076">2070</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2086">2080</span><span
                                                                        data-action="selectDecade" class="decade"
                                                                        data-selection="2096">2090</span><span
                                                                        data-action="selectDecade" class="decade old"
                                                                        data-selection="2106">2100</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="picker-switch accordion-toggle"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
