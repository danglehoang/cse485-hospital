<!DOCTYPE html>
<html>
<head>
    <title>Login Google Error</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12 row-block">
            <strong>Bạn không có quyền sử dụng hệ thống này. <a href="{{ route('admin.auth.logout') }}" class="btn btn-lg btn-primary btn-block">Logout</a> </strong>
        </div>
    </div>
</div>
</body>
</html>
