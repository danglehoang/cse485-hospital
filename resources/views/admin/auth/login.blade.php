<!DOCTYPE html>
<html>
<head>
    <title>Login Google</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <br><br><br><br>
    <div class="col-6 offset-3" style="border: 1px solid #ccc;border-radius: 12px;background-color: #ffffff">
        <div style="padding: 10px; font-size: 25px; text-align: center;">
            <strong>ĐĂNG NHẬP</strong>
        </div>
        <hr>
        <div style="padding: 10px; text-align: center;">
            <a href="{{ url('admin/auth/redirectGoogle') }}" class="" style="text-decoration: none">
                <strong>Login With Google</strong>
            </a>
        </div>
    </div>
</div>
</body>
</html>
