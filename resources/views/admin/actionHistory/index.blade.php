@extends('admin.layouts.layout')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 style="text-transform: uppercase;">
                            {{ trans('label.admin_action_history') }}
                        </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="#">
                                    {{ trans('label.admin_home') }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('label.admin_action_history') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group"></div>
                    </div>
                    <div class="col-sm-8 col-xs-12 float-right">
                        <button class="btn btn-default float-right" onclick="reload_table_after()">
                            <i class="fas fa-refresh"></i> {{ trans('label.admin_reload') }}
                        </button>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 float-left">
                    <div id="form_search">
                        <div class="card">
                            <div class="card-body">
                                <h3>Filter</h3>
                                <div class="d-flex">
                                    <div class="w-25">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_user') }}
                                        </span>
                                        <br>
                                        <select id="name_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_user_id_place_holder') }}</option>
                                            @foreach($user as $item)
                                                <option value="{{ $item->id }}">{{ $item->full_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="w-25" style="margin-left:100px;">
                                        <span style="font-weight: bold;">
                                            {{ trans('label.admin_type') }}
                                        </span>
                                        <br>
                                        <select id="type_search" class="form-control w-100"
                                                onchange="submitForm()">
                                            <option
                                                value="">{{ trans('label.admin_modal_add_type_place_holder') }}</option>
                                            @foreach($type as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="data-table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('label.admin_type') }}</th>
                                    <th>{{ trans('label.admin_name') }}</th>
                                    <th>{{ trans('label.admin_email') }}</th>
                                    <th>{{ trans('label.admin_description') }}</th>
                                    <th>{{ trans('label.admin_updated') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        document.title = '{{ trans('label.admin_title_web') }} - {{ trans('label.admin_action_history') }}';
        const url_ajax_load_datatable = '<?php echo url('/admin/actionHistory/ajax_list_data') ?>',
            url_ajax_search = '<?php echo url('admin/actionHistory/ajax_list_data') ?>';
    </script>

@endsection
