<tbody id="tbody-data">
@foreach($cadres as $item)
    <tr>
        <td class="name">{{ $item->name }}</td>
        <td class="department_id">{{ $item->postsDepartment->name }}</td>
        <td class="department_id">{{ $item->postsDepartment->postsOffice->name }}</td>
        <td class="email">{{ $item->email }}</td>
        <td class="phone">{{ $item->phone }}</td>
        <td class="department_id">{{ $item->postsDepartment->postsOffice->phone }}</td>
    </tr>
@endforeach
</tbody>
