<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    const STATUS_ACTIVE_ROLE = 1;
    const STATUS_DEACTIVATE_ROLE = 2;
    const TYPE_ADMIN = 1;

    protected $table = 'role';

    protected $fillable = [
        'id',
        'name',
        'type',
        'status',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Role::newQuery()->whereIn('status', [self::STATUS_ACTIVE_ROLE, self::STATUS_DEACTIVATE_ROLE])
            ->count();
    }

    public function countAll(): int
    {
        return Role::newQuery()->count();
    }

    public function getListRole($condition = [])
    {
        return Role::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_ROLE, self::STATUS_DEACTIVATE_ROLE])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsUser(): HasMany
    {
        return $this->hasMany('app\Models\Users', 'role_id', 'id');
    }

    public function getRole($id)
    {
        return Role::newQuery()->select('name', 'type')
            ->where('id', $id)
            ->where('status', self::STATUS_ACTIVE_ROLE)
            ->first()
            ->toArray();
    }

    public function getListRoleSearch($condition = [])
    {
        return Role::newQuery()->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
