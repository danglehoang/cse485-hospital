<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Categories extends Model
{
    const STATUS_ACTIVE_CATEGORY = 1;
    const STATUS_DEACTIVATE_CATEGORY = 2;
    const STATUS_SHOW_MENU_CATEGORY = 1;
    const STATUS_NOT_SHOW_MENU_CATEGORY = 2;
    const TYPE_NEW = 1;
    const TYPE_LANDING_PAGE = 2;
    const TYPE_LIST_2_COLUMN = 3;
    const TYPE_LIST_1_COLUMN = 4;
    const TYPE_LIST_2_ROW = 5;
    const TYPE_FAQ = 6;
    const TYPE_OTHER = 7;
    const TYPE_RECRUITMENT = 8;

    protected $table = 'categories';

    protected $fillable = [
        'id',
        'name',
        'slug',
        'menu_id',
        'type',
        'is_show_menu',
        'status'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Categories::newQuery()->whereIn('status', [self::STATUS_ACTIVE_CATEGORY, self::STATUS_DEACTIVATE_CATEGORY])
            ->count();
    }

    public function countAll(): int
    {
        return Categories::newQuery()->count();
    }

    public function getListCategories($condition = [])
    {
        return Categories::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_CATEGORY, self::STATUS_DEACTIVATE_CATEGORY])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsArticles(): HasMany
    {
        return $this->hasMany('app\Models\Articles', 'category_id', 'id');
    }

    public function postsMenu(): BelongsTo
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id', 'id');
    }

    public static function getCategoryByMenuHeader($id)
    {
        return Categories::where('menu_id', $id)
            ->where('status', self::STATUS_ACTIVE_CATEGORY)
            ->where('is_show_menu', self::STATUS_SHOW_MENU_CATEGORY)
            ->orderBy('id', 'asc')
            ->get();
    }

    public static function getCategoryByMenu($id)
    {
        return Categories::where('menu_id', $id)
            ->where('status', self::STATUS_ACTIVE_CATEGORY)
            ->whereIn('is_show_menu', [self::STATUS_SHOW_MENU_CATEGORY, self::STATUS_NOT_SHOW_MENU_CATEGORY])
            ->orderBy('id', 'asc')
            ->get();
    }

    public function detailCategoryById($id)
    {
        return Categories::where('id', $id)
            ->where('status', self::STATUS_ACTIVE_CATEGORY)
            ->where('is_show_menu', self::STATUS_SHOW_MENU_CATEGORY)
            ->first();
    }

    public function getCategoriesNonShowByMenuId($id)
    {
        return Categories::where('menu_id', $id)
            ->where('status', self::STATUS_ACTIVE_CATEGORY)
            ->where('is_show_menu', self::STATUS_NOT_SHOW_MENU_CATEGORY)
            ->first();
    }

    public function getListCategoriesSearch($condition = [])
    {
        return Categories::newQuery()->where('name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('type', 'LIKE', '%' . $condition['type_search'] . '%')
            ->where('menu_id', 'LIKE', '%' . $condition['menu_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
