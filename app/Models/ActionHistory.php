<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ActionHistory extends Model
{
    const TYPE_CREATE = 1;
    const TYPE_EDIT = 2;
    const TYPE_DELETE = 3;
    const DESCRIPTION_CREATE = 'create item in';
    const DESCRIPTION_EDIT = 'edit item in';
    const DESCRIPTION_DELETE = 'delete item in';

    protected $table = 'action_history';

    protected $fillable = [
        'id',
        'type',
        'description',
        'data',
        'user_id'
    ];

    public $timestamps = false;

    public function countDataByDatatable(): int
    {
        return ActionHistory::newQuery()->count();
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countAll(): int
    {
        return ActionHistory::newQuery()->count();
    }

    public function getListActionHistory($condition = [])
    {
        return ActionHistory::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsUser(): BelongsTo
    {
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }

    public function getListActionHistorySearch($condition = [])
    {
        return ActionHistory::newQuery()->where('user_id', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('type', 'LIKE', '%' . $condition['type_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
