<?php

namespace App\Models;

use App\Http\Controllers\Admin\ArticlesController;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Articles extends Model
{
    const STATUS_ACTIVE_ARTICLES = 1;
    const STATUS_DEACTIVATE_ARTICLES = 2;
    const IS_HOT_ARTICLES = 1;
    const IS_NOT_HOT_ARTICLES = 2;

    protected $table = 'articles';

    protected $fillable = [
        'id',
        'slug',
        'basis_id',
        'category_id',
        'department_id',
        'thumb',
        'banner',
        'is_hot',
        'status',
        'type'
    ];

    public $timestamps = false;

    public function countDataByDatatable(): int
    {
        return Articles::newQuery()->whereIn('status', [self::STATUS_ACTIVE_ARTICLES, self::STATUS_DEACTIVATE_ARTICLES])
            ->count();
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countAll(): int
    {
        return Articles::newQuery()->count();
    }

    public function getListArticles($condition = [])
    {
        return Articles::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_ARTICLES, self::STATUS_DEACTIVATE_ARTICLES])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsCategory(): BelongsTo
    {
        return $this->belongsTo('App\Models\Categories', 'category_id', 'id');
    }

    public function postsBasis(): BelongsTo
    {
        return $this->belongsTo('App\Models\Basis', 'basis_id', 'id');
    }

    public function getDetailArticlesByCategoryId($id, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->where('articles.category_id', $id)
            ->first();
    }

    public static function getListArticlesByCategoryId($id, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->where('articles.category_id', $id)
            ->get();
    }

    public static function getListArticlesByCategoryIdArray($id, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->whereIn('articles.category_id', $id)
            ->get();
    }

    public function getArticlesMore($limit, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->orderBy('articles.id', 'desc')
            ->limit($limit)
            ->get();
    }

    public function getArticlesReCruitmentMore($limit, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->where('articles.type', 3)
            ->orderBy('articles.id', 'desc')
            ->limit($limit)
            ->get();
    }

    public function searchArticles($search, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->where('articles_lang.title', 'LIKE', '%' . $search . '%')
            ->orderBy('articles.id', 'desc')
            ->get();
    }

    public function getDetailArticlesByBasisId($basis_id, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles.basis_id', $basis_id)
            ->where('articles_lang.lang_code', $lang)
            ->first();
    }

    public function getDetailArticlesById($id, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles.id', $id)
            ->where('articles_lang.lang_code', $lang)
            ->first();
    }

    public function getListArticlesHot($limit = 10, $lang)
    {
        return DB::table('articles')->join('articles_lang', 'articles.id', '=', 'articles_lang.articles_id')
            ->select('articles_lang.*', 'articles_lang.id AS articles_lang_id', 'articles.*')
            ->where('articles.status', self::STATUS_ACTIVE_ARTICLES)
            ->where('articles_lang.lang_code', $lang)
            ->where('is_hot', self::IS_HOT_ARTICLES)
            ->orderBy('articles.id', 'desc')
            ->limit($limit)
            ->get();
    }

    public function getDetailArticlesByDepartmentId($department_id)
    {
        return Articles::newQuery()->where('status', self::STATUS_ACTIVE_ARTICLES)
            ->where('department_id', $department_id)
            ->first();
    }

    public function getTotalItemActive()
    {
        return Articles::newQuery()->where('status', self::STATUS_ACTIVE_ARTICLES)
            ->count();
    }

    public function getListArticlesSearch($condition = [])
    {
        return Articles::newQuery()->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('basis_id', 'LIKE', '%' . $condition['basis_search'] . '%')
            ->where('department_id', 'LIKE', '%' . $condition['department_search'] . '%')
            ->where('is_hot', 'LIKE', '%' . $condition['is_hot_search'] . '%')
            ->where('type', 'LIKE', '%' . $condition['type_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsArticlesLang(): HasMany
    {
        return $this->hasMany('App\Models\ArticlesLang', 'articles_id', 'id');
    }
}
