<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArticlesLang extends Model
{
    protected $table = 'articles_lang';

    protected $fillable = [
        'articles_id',
        'lang_code',
        'title',
        'content',
        'summary',
    ];

    public $timestamps = false;

    public function postsArticles(): BelongsTo
    {
        return $this->belongsTo('App\Models\Articles', 'articles_id', 'id');
    }
}
