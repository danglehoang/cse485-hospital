<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Office extends Model
{
    const STATUS_ACTIVE_OFFICE = 1;
    const STATUS_DEACTIVATE_OFFICE = 2;

    protected $table = 'office';

    protected $fillable = [
        'id',
        'name',
        'status',
        'phone',
        'email',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Office::newQuery()->whereIn('status', [self::STATUS_ACTIVE_OFFICE, self::STATUS_DEACTIVATE_OFFICE])
            ->count();
    }

    public function countAll(): int
    {
        return Office::newQuery()->count();
    }

    public function getListOffice($condition = [])
    {
        return Office::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_OFFICE, self::STATUS_DEACTIVATE_OFFICE])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getListOfficeSearch($condition = [])
    {
        return Office::newQuery()->where('name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsDepartment(): HasMany
    {
        return $this->hasMany('app\Models\Department', 'office_id', 'id');
    }
}
