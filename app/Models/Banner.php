<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Banner extends Model
{
    const STATUS_ACTIVE_BANNER = 1;
    const STATUS_DEACTIVATE_BANNER = 2;
    const TYPE_BANNER_SLIDE = 1;
    const TYPE_BANNER_SLIDE_SERVICE = 2;
    const TYPE_BANNER_SLIDE_PARTNER = 3;
    const TYPE_BANNER_SLIDE_OTHER = 4;
    const TYPE_BANNER_ALL = 5;

    protected $table = 'banner';

    protected $fillable = [
        'id',
        'name',
        'image',
        'link',
        'type',
        'menu_id',
        'description',
        'status'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Banner::newQuery()->whereIn('status', [self::STATUS_ACTIVE_BANNER, self::STATUS_DEACTIVATE_BANNER])
            ->count();
    }

    public function countAll(): int
    {
        return Banner::newQuery()->count();
    }

    public function getListBanner($condition = [])
    {
        return Banner::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_BANNER, self::STATUS_DEACTIVATE_BANNER])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsMenu(): BelongsTo
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id', 'id');
    }

    public function getSlide($menu_id)
    {
        return Banner::newQuery()->where('status', self::STATUS_ACTIVE_BANNER)
            ->where('type', self::TYPE_BANNER_SLIDE)
            ->where('menu_id', $menu_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getSlideService($menu_id)
    {
        return Banner::newQuery()->where('status', self::STATUS_ACTIVE_BANNER)
            ->where('type', self::TYPE_BANNER_SLIDE_SERVICE)
            ->where('menu_id', $menu_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getSlidePartner($menu_id)
    {
        return Banner::newQuery()->where('status', self::STATUS_ACTIVE_BANNER)
            ->where('type', self::TYPE_BANNER_SLIDE_PARTNER)
            ->where('menu_id', $menu_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getSlideOther($menu_id)
    {
        return Banner::newQuery()->where('status', self::STATUS_ACTIVE_BANNER)
            ->where('type', self::TYPE_BANNER_SLIDE_PARTNER)
            ->where('menu_id', $menu_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getBannerByAll()
    {
        return Banner::newQuery()->where('status', self::STATUS_ACTIVE_BANNER)
            ->where('type', self::TYPE_BANNER_ALL)
            ->first();
    }

    public function getListBannerSearch($condition = [])
    {
        return Banner::newQuery()->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('type', 'LIKE', '%' . $condition['type_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
