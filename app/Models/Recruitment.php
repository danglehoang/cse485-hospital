<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Recruitment extends Model
{
    const STATUS_ACTIVE_RECRUITMENT = 1;
    const STATUS_DEACTIVATE_RECRUITMENT = 2;

    protected $table = 'recruitment';

    protected $fillable = [
        'id',
        'title',
        'summary',
        'number',
        'time_end',
        'salary',
        'address',
        'employment_information',
        'description',
        'interest',
        'requirements',
        'info',
        'category_id',
        'thumb',
        'slug',
        'status',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Recruitment::newQuery()->whereIn('status', [self::STATUS_ACTIVE_RECRUITMENT, self::STATUS_DEACTIVATE_RECRUITMENT])
            ->count();
    }

    public function countAll(): int
    {
        return Recruitment::newQuery()->count();
    }

    public function getListRecruitment($condition = [])
    {
        return Recruitment::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_RECRUITMENT, self::STATUS_DEACTIVATE_RECRUITMENT])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsCategory(): BelongsTo
    {
        return $this->belongsTo('App\Models\Categories', 'category_id', 'id');
    }

    public function listRecruitment()
    {
        return Recruitment::newQuery()->where('status', self::STATUS_ACTIVE_RECRUITMENT)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getDetailRecruitmentById($id)
    {
        return Recruitment::newQuery()->where('id', $id)
            ->where('status', self::STATUS_ACTIVE_RECRUITMENT)
            ->first();
    }

    public function listRecruitmentMore($limit)
    {
        return Recruitment::newQuery()->where('status', self::STATUS_ACTIVE_RECRUITMENT)
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
    }
}
