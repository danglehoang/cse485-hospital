<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Cadres extends Model
{
    const STATUS_ACTIVE_CADRES = 1;
    const STATUS_DEACTIVATE_CADRES = 2;

    protected $table = 'cadres';

    protected $fillable = [
        'id',
        'name',
        'status',
        'email',
        'phone',
        'department_id',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Cadres::newQuery()->whereIn('status', [self::STATUS_ACTIVE_CADRES, self::STATUS_DEACTIVATE_CADRES])
            ->count();
    }

    public function countAll(): int
    {
        return Cadres::newQuery()->count();
    }

    public function getListCadres($condition = [])
    {
        return Cadres::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_CADRES, self::STATUS_DEACTIVATE_CADRES])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getListCadresSearch($condition = [])
    {
        return Cadres::newQuery()->where('full_name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('email', 'LIKE', '%' . $condition['email_search'] . '%')
            ->where('office_id', 'LIKE', '%' . $condition['office_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsDepartment(): BelongsTo
    {
        return $this->belongsTo('App\Models\DepartmentAfter', 'department_id', 'id');
    }
}
