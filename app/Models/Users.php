<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Users extends Model
{
    const STATUS_ACTIVE_USER = 1;
    const STATUS_DEACTIVATE_USER = 2;

    protected $table = 'users';

    protected $fillable = [
        'id',
        'email',
        'full_name',
        'avatar',
        'role_id',
        'status'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getUserByEmail($email)
    {
        return Users::newQuery()->where('email', $email)->first();
    }

    public function checkExitsEmailAndId($email, $id = 0)
    {
        $query = Users::newQuery()->where('email', $email);

        if ($id) $query->where('id', '!=', $id);

        $result = $query->first();

        return $result ? $result : false;
    }

    public function getUserByFullName($full_name)
    {
        return Users::newQuery()->where('full_name', $full_name)->first();
    }

    public function getListUser($condition = [])
    {
        return Users::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_USER, self::STATUS_DEACTIVATE_USER])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function countAll(): int
    {
        return Users::newQuery()->count();
    }

    public function countDataByDatatable(): int
    {
        return Users::newQuery()->whereIn('status', [self::STATUS_ACTIVE_USER, self::STATUS_DEACTIVATE_USER])
            ->count();
    }

    public function postsRole(): BelongsTo
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }

    public function postsActionHistory(): HasMany
    {
        return $this->hasMany('app\Models\ActionHistory', 'user_id', 'id');
    }

    public function getTotalItemActive()
    {
        return Users::newQuery()->where('status', self::STATUS_ACTIVE_USER)
            ->count();
    }

    public function getListUserNew($limit)
    {
        return Users::newQuery()->where('status', self::STATUS_ACTIVE_USER)
            ->limit($limit)
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function getListUserSearch($condition = [])
    {
        return Users::newQuery()->where('full_name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('email', 'LIKE', '%' . $condition['email_search'] . '%')
            ->where('role_id', 'LIKE', '%' . $condition['role_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
