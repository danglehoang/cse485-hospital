<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Menu extends Model
{
    const STATUS_ACTIVE_MENU = 1;
    const STATUS_DEACTIVATE_MENU = 2;
    const SHOW_MENU = 1;
    const NOT_SHOW_MENU = 2;

    protected $table = 'menu';

    protected $fillable = [
        'id',
        'name',
        'menu_number',
        'slug',
        'show_menu',
        'status'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Menu::newQuery()->whereIn('status', [self::STATUS_ACTIVE_MENU, self::STATUS_DEACTIVATE_MENU])
            ->count();
    }

    public function countAll(): int
    {
        return Menu::newQuery()->count();
    }

    public function getListMenu($condition = [])
    {
        return Menu::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_MENU, self::STATUS_DEACTIVATE_MENU])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsCategory(): HasMany
    {
        return $this->hasMany('app\Models\Categories', 'menu_id', 'id');
    }

    public function postsBanner(): HasMany
    {
        return $this->hasMany('app\Models\Banner', 'menu_id', 'id');
    }

    public static function getMenu()
    {
        return Menu::where('status', self::STATUS_ACTIVE_MENU)
            ->where('show_menu', self::SHOW_MENU)
            ->orderBy('menu_number', 'asc')
            ->get();
    }

    public function getDetailMenuById($id)
    {
        return Menu::where('status', self::STATUS_ACTIVE_MENU)
            ->where('show_menu', self::SHOW_MENU)
            ->where('id', $id)
            ->first();
    }

    public function getDetailMenuIndex()
    {
        return Menu::where('status', self::STATUS_ACTIVE_MENU)
            ->where('show_menu', self::NOT_SHOW_MENU)
            ->where('slug', 'index')
            ->first();
    }

    public function getListMenuSearch($condition = [])
    {
        return Menu::newQuery()->where('name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('show_menu', 'LIKE', '%' . $condition['show_menu_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
