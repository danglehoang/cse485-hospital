<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Department extends Model
{
    const STATUS_ACTIVE_DEPARTMENT = 1;
    const STATUS_DEACTIVATE_DEPARTMENT = 2;
    const SHOW_DEPARTMENT = 1;
    const HIDDEN_DEPARTMENT = 2;

    protected $table = 'department';

    protected $fillable = [
        'id',
        'name',
        'phone',
        'email',
        'is_show',
        'thumb',
        'status',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Department::newQuery()->whereIn('status', [self::STATUS_ACTIVE_DEPARTMENT, self::STATUS_DEACTIVATE_DEPARTMENT])
            ->count();
    }

    public function countAll(): int
    {
        return Department::newQuery()->count();
    }

    public function getListDepartment($condition = [])
    {
        return Department::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_DEPARTMENT, self::STATUS_DEACTIVATE_DEPARTMENT])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsDoctor(): HasMany
    {
        return $this->hasMany('App\Models\Doctors', 'department_id', 'id');
    }

    public static function getDepartment()
    {
        return Department::where('status', self::STATUS_ACTIVE_DEPARTMENT)
            ->where('is_show', self::SHOW_DEPARTMENT)
            ->get();
    }

    public function getListDepartmentSearch($condition = [])
    {
        return Department::newQuery()->where('name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('email', 'LIKE', '%' . $condition['email_search'] . '%')
            ->where('phone', 'LIKE', '%' . $condition['phone_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
