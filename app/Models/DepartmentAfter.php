<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DepartmentAfter extends Model
{
    const STATUS_ACTIVE_DEPARTMENT_AFTER = 1;
    const STATUS_DEACTIVATE_DEPARTMENT_AFTER = 2;

    protected $table = 'department_after';

    protected $fillable = [
        'id',
        'name',
        'status',
        'office_id',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return DepartmentAfter::newQuery()->whereIn('status', [self::STATUS_ACTIVE_DEPARTMENT_AFTER, self::STATUS_DEACTIVATE_DEPARTMENT_AFTER])
            ->count();
    }

    public function countAll(): int
    {
        return DepartmentAfter::newQuery()->count();
    }

    public function getListDepartmentAfter($condition = [])
    {
        return DepartmentAfter::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_DEPARTMENT_AFTER, self::STATUS_DEACTIVATE_DEPARTMENT_AFTER])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsOffice(): BelongsTo
    {
        return $this->belongsTo('App\Models\Office', 'office_id', 'id');
    }

    public function postsCadres(): HasMany
    {
        return $this->hasMany('app\Models\Cadres', 'department_id', 'id');
    }
}
