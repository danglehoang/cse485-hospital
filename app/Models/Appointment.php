<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $table = 'appointment';

    protected $fillable = [
        'id',
        'name',
        'phone',
        'email',
        'content',
        'birthday',
        'time_appointment',
        'basis',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getListAppointment($condition = [])
    {
        return Appointment::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function countAll(): int
    {
        return Appointment::newQuery()->count();
    }

    public function countDataByDatatable(): int
    {
        return Appointment::newQuery()->count();
    }
}
