<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Doctors extends Model
{
    const STATUS_ACTIVE_DOCTOR = 1;
    const STATUS_DEACTIVATE_DOCTOR = 2;

    protected $table = 'doctors';

    protected $fillable = [
        'id',
        'name',
        'department_id',
        'basis_id',
        'language',
        'education',
        'specialized_activities',
        'description',
        'achievement',
        'advisory',
        'slug',
        'thumb',
        'level',
        'status',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Doctors::newQuery()->whereIn('status', [self::STATUS_ACTIVE_DOCTOR, self::STATUS_DEACTIVATE_DOCTOR])
            ->count();
    }

    public function countAll(): int
    {
        return Doctors::newQuery()->count();
    }

    public function getListDoctor($condition = []): Collection
    {
        return Doctors::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_DOCTOR, self::STATUS_DEACTIVATE_DOCTOR])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsDepartment(): BelongsTo
    {
        return $this->belongsTo('App\Models\Department', 'department_id', 'id');
    }

    public function postsBasis(): BelongsTo
    {
        return $this->belongsTo('App\Models\Basis', 'basis_id', 'id');
    }

    public static function getAllDoctor()
    {
        return Doctors::where('status', self::STATUS_ACTIVE_DOCTOR)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function getDoctorByDepartmentAndBasis($basis_id, $department_id)
    {
        return Doctors::where('status', self::STATUS_ACTIVE_DOCTOR)
            ->where('basis_id', $basis_id)
            ->where('department_id', $department_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function getDoctorByBasis($basis_id)
    {
        return Doctors::where('status', self::STATUS_ACTIVE_DOCTOR)
            ->where('basis_id', $basis_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public static function getDoctorByDepartment($department_id)
    {
        return Doctors::where('status', self::STATUS_ACTIVE_DOCTOR)
            ->where('department_id', $department_id)
            ->orderBy('id', 'desc')
            ->get();
    }

    public function getDetailDoctor($id)
    {
        return Doctors::where('status', self::STATUS_ACTIVE_DOCTOR)
            ->where('id', $id)
            ->first();
    }

    public function getListDoctorSearch($condition = [])
    {
        return Doctors::newQuery()->where('name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('basis_id', 'LIKE', '%' . $condition['basis_search'] . '%')
            ->where('department_id', 'LIKE', '%' . $condition['department_search'] . '%')
            ->where('language', 'LIKE', '%' . $condition['language_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
