<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Basis extends Model
{
    const STATUS_ACTIVE_BASIS = 1;
    const STATUS_DEACTIVATE_BASIS = 2;
    const SHOW_BASIS = 1;
    const NOT_SHOW_BASIS = 2;

    protected $table = 'basis';

    protected $fillable = [
        'id',
        'name',
        'address',
        'phone',
        'email',
        'is_show',
        'working_time',
        'status'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function countDataByDatatable(): int
    {
        return Basis::newQuery()->whereIn('status', [self::STATUS_ACTIVE_BASIS, self::STATUS_DEACTIVATE_BASIS])
            ->count();
    }

    public function countAll(): int
    {
        return Basis::newQuery()->count();
    }

    public function getListBasis($condition = [])
    {
        return Basis::newQuery()->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->whereIn('status', [self::STATUS_ACTIVE_BASIS, self::STATUS_DEACTIVATE_BASIS])
            ->orderBy('id', 'desc')
            ->get();
    }

    public function postsDoctor(): HasMany
    {
        return $this->hasMany('app\Models\Doctors', 'basis_id', 'id');
    }

    public static function getBasis()
    {
        return Basis::where('status', self::STATUS_ACTIVE_BASIS)
            ->where('is_show', self::SHOW_BASIS)
            ->get();
    }

    public function getListBasisSearch($condition = [])
    {
        return Basis::newQuery()->where('name', 'LIKE', '%' . $condition['name_search'] . '%')
            ->where('status', 'LIKE', '%' . $condition['status_search'] . '%')
            ->where('email', 'LIKE', '%' . $condition['email_search'] . '%')
            ->where('phone', 'LIKE', '%' . $condition['phone_search'] . '%')
            ->skip(($condition['page'] - 1) * $condition['limit'])
            ->take($condition['limit'])
            ->orderBy('id', 'desc')
            ->get();
    }
}
