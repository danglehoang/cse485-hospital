<?php

namespace App\Helper;

use App\Helper\ResultHelper;
use App\Models\Articles;
use App\Models\Banner;
use App\Models\Basis;
use App\Models\Categories;
use App\Models\Department;
use App\Models\Doctors;
use App\Models\Faq;
use App\Models\Menu;
use App\Models\Recruitment;
use App\Models\Role;
use App\Models\Users;
use App\Models\ActionHistory;
use App\Helper\ConvertDataHelper;

class AjaxHelper
{
    protected $resultHelper;
    protected $convertDataHelper;

    public function __construct()
    {
        $this->resultHelper = new ResultHelper();
        $this->convertDataHelper = new ConvertDataHelper();
    }

    public function ajaxAdd($data_request, $model)
    {
        $user_login = session('user_auth');
        $data_insert = $this->convertDataHelper->convertData($data_request);

        if ($model == ConvertDataHelper::ROLE) {
            $query = Role::create($data_insert);
        } else if ($model == ConvertDataHelper::CATEGORY) {
            $query = Categories::create($data_insert);
        } else if ($model == ConvertDataHelper::BASIS) {
            $query = Basis::create($data_insert);
        } else if ($model == ConvertDataHelper::MENU) {
            $query = Menu::create($data_insert);
        }

        $data_log = [
            'type' => ActionHistory::TYPE_CREATE,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_CREATE . ' ' . $model,
            'data' => json_encode($data_insert),
            'user_id' => $user_login['id'],
        ];

        if ($query) {
            $query = ActionHistory::create($data_log);

            if ($query) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_create_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
            }

        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
        }

        return $result;
    }

    public function ajaxEdit($id, $model)
    {
        if ($id == null) {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_please_fill_out_the_form'));

            die(json_encode($result));
        }

        if ($model == ConvertDataHelper::USER) {
            $data = Users::findOrFail($id);
        } else if ($model == ConvertDataHelper::ROLE) {
            $data = Role::findOrFail($id);
        } else if ($model == ConvertDataHelper::DOCTOR) {
            $data = Doctors::findOrFail($id);
        } else if ($model == ConvertDataHelper::DEPARTMENT) {
            $data = Department::findOrFail($id);
        } else if ($model == ConvertDataHelper::CATEGORY) {
            $data = Categories::findOrFail($id);
        } else if ($model == ConvertDataHelper::BASIS) {
            $data = Basis::findOrFail($id);
        } else if ($model == ConvertDataHelper::BANNER) {
            $data = Banner::findOrFail($id);
        } else if ($model == ConvertDataHelper::ARTICLES) {
            $data = Articles::findOrFail($id);
        } else if ($model == ConvertDataHelper::MENU) {
            $data = Menu::findOrFail($id);
        } else if ($model == ConvertDataHelper::RECRUITMENT) {
            $data = Recruitment::findOrFail($id);
        }

        if ($data) {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_find_item_success'), '', $data);
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_find_item_error'));
        }

        return $result;
    }

    public function ajaxUpdate($data_request, $request, $model)
    {
        $user_login = session('user_auth');
        $data_update = $this->convertDataHelper->convertData($data_request);
        unset($data_update['id']);
        $data_update['updated'] = date('Y-m-d H:i:s');

        if ($model == ConvertDataHelper::ROLE) {
            $data_before_update = Role::where('id', $request->id)->first();
            $query = Role::where('id', $request->id)->update($data_update);
        } else if ($model == ConvertDataHelper::CATEGORY) {
            $data_before_update = Categories::where('id', $request->id)->first();
            $query = Categories::where('id', $request->id)->update($data_update);
        } else if ($model == ConvertDataHelper::BASIS) {
            $data_before_update = Basis::where('id', $request->id)->first();
            $query = Basis::where('id', $request->id)->update($data_update);
        } else if ($model == ConvertDataHelper::MENU) {
            $data_before_update = Menu::where('id', $request->id)->first();
            $query = Menu::where('id', $request->id)->update($data_update);
        }

        $data_log = [
            'type' => ActionHistory::TYPE_EDIT,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_EDIT . ' ' . $model,
            'data' => json_encode($data_before_update),
            'user_id' => $user_login['id'],
        ];

        if ($query) {
            $query = ActionHistory::create($data_log);

            if ($query) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_update_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_update_error'));
            }
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_update_error'));
        }

        return $result;
    }

    public function ajaxDelete($data_request, $model)
    {
        $user_login = session('user_auth');

        if ($data_request->id == null) {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_please_fill_out_the_form'));

            die(json_encode($result));
        }

        if ($model == ConvertDataHelper::USER) {
            $data_delete = Users::where('id', $data_request->id)->first();
            $query = Users::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::ROLE) {
            $data_delete = Role::where('id', $data_request->id)->first();
            $query = Role::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::DOCTOR) {
            $data_delete = Doctors::where('id', $data_request->id)->first();
            $query = Doctors::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::DEPARTMENT) {
            $data_delete = Department::where('id', $data_request->id)->first();
            $query = Department::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::CATEGORY) {
            $data_delete = Categories::where('id', $data_request->id)->first();
            $query = Categories::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::BASIS) {
            $data_delete = Basis::where('id', $data_request->id)->first();
            $query = Basis::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::BANNER) {
            $data_delete = Banner::where('id', $data_request->id)->first();
            $query = Banner::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::ARTICLES) {
            $data_delete = Articles::where('id', $data_request->id)->first();
            $query = Articles::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::MENU) {
            $data_delete = Menu::where('id', $data_request->id)->first();
            $query = Menu::where('id', $data_request->id)->delete();
        } else if ($model == ConvertDataHelper::RECRUITMENT) {
            $data_delete = Recruitment::where('id', $data_request->id)->first();
            $query = Recruitment::where('id', $data_request->id)->delete();
        }

        $data_log = [
            'type' => ActionHistory::TYPE_DELETE,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_DELETE . ' ' . $model,
            'data' => json_encode($data_delete),
            'user_id' => $user_login['id'],
        ];

        if ($query) {
            $query = ActionHistory::create($data_log);

            if ($query) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_delete_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_delete_error'));
            }
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_delete_error'));
        }

        return $result;
    }

    public static function create_folder($name, $id) {
        $path = public_path().'/images/admin/'. $name .'/'.$id.'/';

        if(!is_dir($path)){
            mkdir($path, 0755, TRUE);
        }
    }
}
