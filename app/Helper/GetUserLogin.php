<?php

namespace App\Helper;

use App\Models\Role;

class GetUserLogin
{
    protected $roleModel;

    public function __construct()
    {
        $this->roleModel = new Role();
    }

    public function UserLogin()
    {
        $user_login = session('user_auth');

        if(empty($user_login)) {
            Redirect::to('admin/auth/login')->send();
        } else {
            $getRoleUser = $this->roleModel->getRole($user_login['role_id']);
            $user_login['detailRole'] = $getRoleUser;

            return $user_login;
        }
    }
}
