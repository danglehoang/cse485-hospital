<?php

namespace App\Helper;

class ResultHelper
{
    public function resultAjax($status, $type, $message = '', $validation = '', $data = [])
    {
        $result =  [
            'status' => $status,
            'type' => $type,
            'message' => $message,
            'validation' => $validation,
            'data' => $data,
        ];

        return $result;
    }
}
