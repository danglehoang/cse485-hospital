<?php

namespace App\Helper;

use Illuminate\Support\Facades\Validator;

class ConvertDataHelper
{
    const CATEGORY = 'category';
    const DOCTOR = 'doctor';
    const DEPARTMENT = 'department';
    const DEPARTMENT_AFTER = 'department_after';
    const BASIS = 'basis';
    const ARTICLES = 'articles';
    const BANNER = 'banner';
    const USER = 'user';
    const ROLE = 'role';
    const CADRES = 'cadres';
    const OFFICE = 'office';
    const MENU = 'menu';

    public function convertData($request)
    {
        $this->validateData($request);

        unset($request['_token']);
        unset($request['key']);
        return $request;
    }

    public function validateData($request)
    {
        if ($request['key'] == self::CATEGORY) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'slug' => 'required',
                'menu_id' => 'required',
                'type' => 'required',
                'is_show_menu' => 'required',
                'status' => 'required'
            ]);
        } else if ($request['key'] == self::DOCTOR) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'department_id' => 'required',
                'basis_id' => 'required',
                'language' => 'required',
                'slug' => 'required',
                'level' => 'required',
                'status' => 'required',
            ]);
        } else if ($request['key'] == self::ARTICLES) {
            $validate = Validator::make($request, [
                'slug' => 'required',
                'category_id' => 'required',
                'department_id' => 'required',
                'is_hot' => 'required',
                'status' => 'required',
                'type' => 'required',
            ]);
        } else if ($request['key'] == self::DEPARTMENT) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'is_show' => 'required',
                'status' => 'required',
            ]);
        } else if($request['key'] == self::BASIS) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'is_show' => 'required',
                'working_time' => 'required',
                'status' => 'required',
            ]);
        } else if($request['key'] == self::ROLE) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'type' => 'required',
                'status' => 'required',
            ]);
        }else if($request['key'] == self::DEPARTMENT_AFTER) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'status' => 'required',
                'office_id' => 'required',
            ]);
        } else if ($request['key'] == self::BANNER) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'link' => 'required',
                'type' => 'required',
                'menu_id' => 'required',
                'status' => 'required',
            ]);
        } else if ($request['key'] == self::USER) {
            $validate = Validator::make($request, [
                'email' => 'required',
                'full_name' => 'required',
                'role_id' => 'required',
                'status' => 'required',
            ]);
        }else if ($request['key'] == self::CADRES) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'status' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'department_id' => 'required',
            ]);
        } else if ($request['key'] == self::OFFICE) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'status' => 'required',
                'email' => 'required',
                'phone' => 'required',
            ]);
        } else if ($request['key'] == self::MENU) {
            $validate = Validator::make($request, [
                'name' => 'required',
                'slug' => 'required',
                'show_menu' => 'required',
                'status' => 'required',
            ]);
        }

        if ($validate->fails()) {
            $data_error = [
                'status' => 'fail',
                'type' => 'warning',
                'message' => 'Please fill out the form',
                'validation' => $validate->messages()
            ];

            die(json_encode($data_error));
        }
    }
}
