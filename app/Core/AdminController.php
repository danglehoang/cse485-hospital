<?php

namespace App\Core;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use Redirect;
use Session;

class AdminController extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $page_data = [];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(empty(session('user_auth'))) {
                Redirect::to('admin/auth/login')->send();
            } else {
                return $next($request);
            }
        });
    }
}



