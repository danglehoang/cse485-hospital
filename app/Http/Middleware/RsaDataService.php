<?php

namespace App\Http\Middleware;

class RsaDataService
{
    
    public function decodeRsaData($string_rsa) {

        $list_params = explode('_', $string_rsa);
        if(!is_array($list_params)) {
            $response = [
                'status' => 1,
                'message' => 'Thông tin game lỗi, vui lòng kiểm tra lại.'
            ];
            die(json_encode($response));
        }

        $str_data = '';
        foreach($list_params as $val) {
            $str_data .= decryptRsa($val);
        }

        if(!$str_data) {
            $response = [
                'status' => 1,
                'message' => 'Thông tin game lỗi, vui lòng kiểm tra lại.'
            ];
            die(json_encode($response));
        }

        $data = json_decode($str_data);

        return $data;
    }

}