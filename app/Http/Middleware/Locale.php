<?php

namespace App\Http\Middleware;

use Closure;
use App;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = empty(session('website_language')) ? config('app.locale') : session('website_language');

        config(['app.locale' => $language]);

        return $next($request);
    }
}
