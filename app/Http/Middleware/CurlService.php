<?php

namespace App\Http\Middleware;

use Exception;

class CurlService
{

    public function __construct()
    {
        
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function curlPost($url, $data_request, $header = [], $custom_header = 0)
    {

        $post_field = '';
        foreach ($data_request as $key => $val) {
            $post_field .= $key . '=' . $val . '&';
        }

        if ($custom_header == 0) {
            $header_default = ['content-type: application/x-www-form-urlencoded'];
            $headers = array_merge($header_default, $header);
        } else {
            $headers = $header;
        }


        try {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post_field,
                CURLOPT_HTTPHEADER => $headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            // var_dump($err);
            // var_dump($response);
            // var_dump($url);
            // die;

            if ($err) {
                return json_encode(array(
                    'status' => '1',
                    'message' => 'Hệ thống bị lỗi, vui lòng quay lại sau ít phút.'
                ));
            } else {
                return $response;
            }

        } catch (Exception $e) {
            return json_encode(array(
                'status' => '1',
                'message' => 'Hệ thống bị lỗi, vui lòng quay lại sau ít phút.'
            ));
        }


    }

    public function curlGet($url, $data_request = [], $header = [])
    {
        $headers = $header;

        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => $headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return json_encode(array(
                    'status' => '1',
                    'message' => 'Hệ thống bị lỗi, vui lòng quay lại sau ít phút.'
                ));
            } else {
                return $response;
            }
        } catch (Exception $e) {
            return json_encode(array(
                'status' => '1',
                'message' => 'Hệ thống bị lỗi, vui lòng quay lại sau ít phút.'
            ));
        }

    }

    public function pushNotify($text)
    {
        $token = '612122610:AAGf477qu8IX0erRw6Ci3D2qFenRGfoNTV8';
        $data = [
            'chat_id' => '-542365035',
            'text' => $text
        ];

        $url = "https://api.telegram.org/bot" . $token . "/sendMessage?" . http_build_query($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); // 1s

        curl_exec($ch);

        if (curl_error($ch)) {
            return false;
        }

        curl_close($ch);
        return true;
    }

    public function curlNganLuong($url, $data_request, $header = [])
    {

        $post_field = '';
        foreach ($data_request as $key => $val) {
            $post_field .= $key . '=' . $val . '&';
        }
        $headers = $header;


        try {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post_field,
                CURLOPT_HTTPHEADER => $headers,
            ));

            $response = curl_exec($curl);
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
            $err = curl_error($curl);

            curl_close($curl);

            if ($response != '' && $status == 200) {
                return $response;
            } else {
                return false;
            }

        } catch (Exception $e) {
            return false;
        }


    }


    public function curlPostVMG($url, $data_request)
    {

        $post_field = '';
        foreach ($data_request as $key => $val) {
            $post_field .= $key . '=' . $val . '&';
        }

        $headers = ['content-type: application/x-www-form-urlencoded', 'charset: utf-8'];

        try {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post_field,
                CURLOPT_HTTPHEADER => $headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return false;
            } else {
                return $response;
            }

        } catch (Exception $e) {
            false;
        }

    }

}
