<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cadres;
use App\Models\DepartmentAfter;
use App\Models\Office;
use Illuminate\Http\Request;

class ListController extends Controller
{

    public function __construct()
    {
    }

    public function index()
    {
        $data = [
            'cadres' => Cadres::where('status', 1)->get(),
            'office' => Office::where('status', 1)->get()
        ];
        return view('list', $data);
    }

    public function search(Request $request)
    {
        $data_request = $request->all();

        if ($data_request['office_search'] != '') {
            $department = DepartmentAfter::where('office_id', $data_request['office_search'])->where('status', 1)->get()->pluck('id')->toArray();
            $data = Cadres::where('name', 'LIKE', '%' . $data_request['name_search'] . '%')
                ->where('email', 'LIKE', '%' . $data_request['email_search'] . '%')
                ->whereIn('department_id', $department);
        }
        $data = Cadres::where('name', 'LIKE', '%' . $data_request['name_search'] . '%')
            ->where('email', 'LIKE', '%' . $data_request['email_search'] . '%')
            ->orderBy('id', 'desc')
            ->get();


        if ($data) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => 'Thông tin tìm kiếm',
                'data' => $data
            ];
        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_find_item_error'),
            ];
        }

        die(json_encode($result));
    }
}
