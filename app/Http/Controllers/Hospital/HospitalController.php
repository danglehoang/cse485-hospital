<?php

namespace App\Http\Controllers\Hospital;

use App\Helper\ResultHelper;
use App\Http\Controllers\Admin\ArticlesController;
use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Articles;
use App\Models\Banner;
use App\Models\Basis;
use App\Models\Categories;
use App\Models\Department;
use App\Models\Doctors;
use App\Models\Menu;
use App\Models\Recruitment;
use Illuminate\Http\Request;
use Session;
use App;

class HospitalController extends Controller
{
    protected $resultHelper;
    protected $articlesController;
    protected $bannerModel;
    protected $basisModel;
    protected $departmentModel;
    protected $categoriesModel;
    protected $recruitmentModel;
    protected $articlesModel;
    protected $menuModel;
    protected $doctorModel;
    protected $appointmentModel;

    public function __construct()
    {
        $this->resultHelper = new ResultHelper();
        $this->articlesController = new ArticlesController();
        $this->bannerModel = new Banner();
        $this->basisModel = new Basis();
        $this->departmentModel = new Department();
        $this->categoriesModel = new Categories();
        $this->recruitmentModel = new Recruitment();
        $this->articlesModel = new Articles();
        $this->menuModel = new Menu();
        $this->doctorModel = new Doctors();
        $this->appointmentModel = new Appointment();
    }

    public function index(Request $request)
    {
        $detailMenu = $this->menuModel->getDetailMenuIndex();
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');

        $data = [
            'banner_slide' => $this->bannerModel->getSlide($detailMenu['id']),
            'banner_service' => $this->bannerModel->getSlideService($detailMenu['id']),
            'banner_partner' => $this->bannerModel->getSlidePartner($detailMenu['id']),
            'basis' => $this->basisModel->getBasis(),
            'department' => $this->departmentModel->getDepartment(),
            'category' => $this->categoriesModel->getCategoryByMenu(5),
            'lang' => $lang
        ];

        return view('hospital/index', $data);
    }

    public function detailCategory(Request $request, $slug_menu, $slug_category)
    {
        $slug = explode('-', $slug_category);
        $id = end($slug);
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');

        $detailCategoryById = $this->categoriesModel->detailCategoryById($id);
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);
        $banner = $this->bannerModel->getBannerByAll();
        $detailArticlesByCategory = $this->articlesModel->getDetailArticlesByCategoryId($detailCategoryById['id'], $lang);
        $listArticlesByCategory = $this->articlesModel->getListArticlesByCategoryId($detailCategoryById['id'], $lang);

        if ($detailCategoryById['type'] == Categories::TYPE_NEW) {
            $data = [
                'articlesDetail' => $detailArticlesByCategory,
                'articlesMore' => $articlesMore,
                'banner' => $banner,
            ];

            if ($detailArticlesByCategory) {
                return view('hospital/detail_new', $data);
            }

            return view('hospital/error');
        } else if ($detailCategoryById['type'] == Categories::TYPE_LANDING_PAGE) {
            return view('hospital/error');
        } else if ($detailCategoryById['type'] == Categories::TYPE_LIST_2_COLUMN) {
            $data = [
                'banner' => $banner,
                'listArticles' => $listArticlesByCategory,
                'articlesMore' => $articlesMore,
                'category' => $detailCategoryById,
            ];
            if ($listArticlesByCategory) {
                return view('hospital/list_articles_2_column', $data);
            }

            return view('hospital/error');
        } else if ($detailCategoryById['type'] == Categories::TYPE_LIST_1_COLUMN) {
            $articlesMore = $this->articlesModel->getArticlesReCruitmentMore(10, $lang);

            $data = [
                'banner' => $banner,
                'listArticles' => $listArticlesByCategory,
                'articlesMore' => $articlesMore,
                'category' => $detailCategoryById,
            ];

            if ($listArticlesByCategory) {
                return view('hospital/list_articles_1_column', $data);
            }

            return view('hospital/error');
        } else if ($detailCategoryById['type'] == Categories::TYPE_LIST_2_ROW) {

        } else if ($detailCategoryById['type'] == Categories::TYPE_FAQ) {
            $data = [
                'articlesMore' => $articlesMore,
                'banner' => $banner,
                'faq' => $listArticlesByCategory
            ];

            return view('hospital/detail_faq', $data);
        } else if ($detailCategoryById['type'] == Categories::TYPE_OTHER) {
            return view('hospital/error');
        }
    }

    public function ajaxExitsArticlesMenu($id)
    {
        $checkExistArticlesMenu = Articles::getDetailArticlesMenu($id);

        if (!$checkExistArticlesMenu) {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'));

            die(json_encode($result));
        }
        $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'));

        die(json_encode($result));
    }

    public function detailArticlesByBasisId($basis_id)
    {
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');
        $detailArticlesByBasisId = $this->articlesModel->getDetailArticlesByBasisId($basis_id, $lang);
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);
        $banner = $this->bannerModel->getBannerByAll();

        $data = [
            'articlesDetail' => $detailArticlesByBasisId,
            'articlesMore' => $articlesMore,
            'banner' => $banner,
        ];

        if ($detailArticlesByBasisId) {
            return view('hospital/detail_new', $data);
        }

        return view('hospital/error');
    }

    public function detailArticles($slug)
    {
        $slug = explode('-', $slug);
        $id = end($slug);
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');

        $detailArticlesById = $this->articlesModel->getDetailArticlesById($id, $lang);
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);
        $banner = $this->bannerModel->getBannerByAll();

        $data = [
            'articlesDetail' => $detailArticlesById,
            'articlesMore' => $articlesMore,
            'banner' => $banner,
        ];

        if ($detailArticlesById) {
            return view('hospital/detail_new', $data);
        }

        return view('hospital/error');
    }

    public function redirectByMenuId( Request $request, $slug)
    {
        $slug = explode('-', $slug);
        $id = end($slug);
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');

        $detailMenuById = $this->menuModel->getDetailMenuById($id);
        $banner = $this->bannerModel->getBannerByAll();
        $basis = $this->basisModel->getBasis();
        $department = $this->departmentModel->getDepartment();
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);
        $categoryNoShowByMenu = $this->categoriesModel->getCategoriesNonShowByMenuId($detailMenuById['id']);

        if ($detailMenuById['menu_number'] == 1) {
            $articlesByCategory = $this->articlesModel->getDetailArticlesByCategoryId($categoryNoShowByMenu['id'], $lang);
            $bannerOtherByMenu = $this->bannerModel->getSlideService($detailMenuById['id']);
            $libImage = $this->bannerModel->getSlideOther($detailMenuById['id']);

            $data = [
                'banner' => $banner,
                'articlesByCategory' => $articlesByCategory,
                'slideIntro' => $bannerOtherByMenu,
                'libImage' => $libImage
            ];

            if ($articlesByCategory) {
                return view('hospital/introduce', $data);
            }

            return view('hospital/error');
        } else if ($detailMenuById['menu_number'] == 2) {
            $categoryByMenu = $this->categoriesModel->getCategoryByMenu($detailMenuById['id']);

            $data = [
                'banner' => $banner,
                'department' => $department,
                'categories' => $categoryByMenu,
                'basis' => $basis,
            ];

            return view('hospital/service_hospital', $data);
        } else if ($detailMenuById['menu_number'] == 3) {

            $basis_id = $request['basis'];
            $department_id = $request['department'];

            if($basis_id == null) {
                if($department_id == null) {
                    $doctor = $this->doctorModel->getAllDoctor();
                } else {
                    $doctor = $this->doctorModel->getDoctorByDepartment($department_id);
                }
            } else {
                if($department_id != null) {
                    $doctor = $this->doctorModel->getDoctorByDepartmentAndBasis($basis_id, $department_id);
                } else {
                    $doctor = $this->doctorModel->getDoctorByBasis($basis_id);
                }
            }

            $data = [
                'banner' => $banner,
                'basis' => $basis,
                'department' => $department,
                'articlesMore' => $articlesMore,
                'doctor' => $doctor,
                'basis_id' => $basis_id,
                'department_id' => $department_id,
            ];

            if ($doctor) {
                return view('hospital/list_doctor', $data);
            }

            return view('hospital/error');
        } else if ($detailMenuById['menu_number'] == 4) {
            $categoryShowByMenu = $this->categoriesModel->getCategoryByMenuHeader($detailMenuById['id']);
            $arrayId = [];
            foreach($categoryShowByMenu as $item) {
                array_push($arrayId, $item['id']);
            }

            $listArticlesByCategory = $this->articlesModel->getListArticlesByCategoryIdArray($arrayId, $lang);
            $data = [
                'banner' => $banner,
                'listArticles' => $listArticlesByCategory,
                'articlesMore' => $articlesMore,
                'category' => $categoryNoShowByMenu,
            ];

            if ($listArticlesByCategory) {
                return view('hospital/list_articles_2_column', $data);
            }

            return view('hospital/error');
        } else if ($detailMenuById['menu_number'] == 5) {
            $category = $this->categoriesModel->getCategoryByMenu($detailMenuById['id']);
            $articlesHot = $this->articlesModel->getListArticlesHot(3, $lang);

            $data = [
                'banner' => $banner,
                'articlesMore' => $articlesMore,
                'articlesHot' => $articlesHot,
                'category' => $category,
                'lang' => $lang
            ];

            return view('hospital/newAndOffer', $data);
        } else if ($detailMenuById['menu_number'] == 6) {
            $articlesDetail = $this->articlesModel->getDetailArticlesByCategoryId($categoryNoShowByMenu['id'], $lang);
            $data = [
                'banner' => $banner,
                'articlesDetail' => $articlesDetail,
                'articlesMore' => $articlesMore,
            ];

            return view('hospital/detail_new', $data);
        } else if ($detailMenuById['menu_number'] == 7) {
            $articlesDetail = $this->articlesModel->getDetailArticlesByCategoryId($categoryNoShowByMenu['id'], $lang);
            $data = [
                'banner' => $banner,
                'articlesDetail' => $articlesDetail,
                'articlesMore' => $articlesMore,
            ];

            if ($articlesDetail) {
                return view('hospital/detail_new', $data);
            }

            return view('hospital/error');
        }
    }

    public function detailDoctor($slug)
    {
        $slug = explode('-', $slug);
        $id = end($slug);
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');

        $banner = $this->bannerModel->getBannerByAll();
        $detailDoctor = $this->doctorModel->getDetailDoctor($id);
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);

        $data = [
            'banner' => $banner,
            'detailDoctor' => $detailDoctor,
            'articlesMore' => $articlesMore,
        ];

        if ($detailDoctor) {
            return view('hospital/detail_doctor', $data);
        }

        return view('hospital/error');
    }

    public function detailArticlesByDepartmentId($department_id)
    {
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');
        $detailArticlesByDepartmentId = $this->articlesModel->getDetailArticlesByDepartmentId($department_id);
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);
        $banner = $this->bannerModel->getBannerByAll();

        $data = [
            'articlesDetail' => $detailArticlesByDepartmentId,
            'articlesMore' => $articlesMore,
            'banner' => $banner,
        ];

        if ($detailArticlesByDepartmentId) {
            return view('hospital/detail_new', $data);
        }

        return view('hospital/error');
    }

    public function submitAppointment(Request $request)
    {
        $data_request = $request->all();

        if ($this->appointmentModel::create($data_request)) {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_create_success'));
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
        }

        die(json_encode($result));
    }

    public function changeLanguage($language)
    {
        App::setLocale($language);
        session(['website_language' => $language]);
        return redirect()->back();
    }

    public function searchArticles(Request $request)
    {
        $data_request = $request->all();
        $lang = empty(session('website_language')) ? config('app.locale') : session('website_language');
        $banner = $this->bannerModel->getBannerByAll();
        $articles = $this->articlesModel->searchArticles($data_request['articles'], $lang);
        $articlesMore = $this->articlesModel->getArticlesMore(10, $lang);
        $category = new \stdClass();
        $category->name = 'Tìm kiếm';

        $data = [
            'banner' => $banner,
            'listArticles' => $articles,
            'articlesMore' => $articlesMore,
            'category' => $category,
        ];

        return view('hospital/list_articles_2_column', $data);
    }
}

