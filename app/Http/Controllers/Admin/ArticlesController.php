<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\AjaxHelper;
use App\Helper\GetUserLogin;
use App\Helper\ResultHelper;
use App\Models\ActionHistory;
use App\Models\ArticlesLang;
use App\Models\Basis;
use App\Models\Department;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Articles;
use App\Models\Categories;
use App\Helper\ConvertDataHelper;

class ArticlesController extends AdminController
{
    protected $articlesModel;
    protected $convertDataHelper;
    protected $resultHelper;
    protected $ajaxHelper;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->articlesModel = new Articles();
        $this->convertDataHelper = new ConvertDataHelper();
        $this->resultHelper = new ResultHelper();
        $this->ajaxHelper = new AjaxHelper();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index()
    {
        $user_login = $this->getUserLogin->UserLogin();
        $type = self::typeArticles();

        $data = [
            'user_login' => $user_login,
            'categories' => Categories::whereIn('status', [Categories::STATUS_ACTIVE_CATEGORY, Categories::STATUS_DEACTIVATE_CATEGORY])->get(),
            'basis' => Basis::whereIn('status', [Basis::STATUS_ACTIVE_BASIS, Basis::STATUS_DEACTIVATE_BASIS])->get(),
            'department' => Department::whereIn('status', [Department::STATUS_ACTIVE_DEPARTMENT, Department::STATUS_DEACTIVATE_DEPARTMENT])->get(),
            'type' => $type,
        ];

        return view('admin/articles/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;
        $params['basis_search'] = $request->basis_search;
        $params['department_search'] = $request->department_search;
        $params['is_hot_search'] = $request->is_hot_search;
        $params['type_search'] = $request->type_search;

        if (empty($request->status_search) && empty($request->basis_search) && empty($request->department_search) && empty($request->is_hot_search)&& empty($request->type_search)) {
            $list_data = $this->articlesModel->getListArticles($params);
        } else {
            $list_data = $this->articlesModel->getListArticlesSearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = isset($item->thumb) ? '<img class="w-25" src="/' . $item->thumb . '">' : '';
            $row[] = $item->postsCategory->name;
            $row[] = $item->postsBasis->name;
            $row[] = $item->is_hot == Articles::IS_HOT_ARTICLES ? '<span class="btn btn-block btn-success btn-sm">Yes</span>' : '<span class="btn btn-block btn-danger btn-sm">No</span>';
            $row[] = $item->slug;
            $row[] = $item->updated;
            $row[] = $item->status == Articles::STATUS_ACTIVE_ARTICLES ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactive</span>';

            if ($user_login['detailRole']['type'] == Role::TYPE_ADMIN) {
                $action = '<div class="text-center d-flex justify-content-center">';
                $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
                $action .= '</div>';
                $row[] = $action;
            }

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->articlesModel->countAll(),
            "recordsFiltered" => $this->articlesModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function create()
    {
        $user_login = $this->getUserLogin->UserLogin();
        $lang_code = self::langArticles();
        $type = self::typeArticles();

        $data = [
            'categories' => Categories::whereIn('status', [Categories::STATUS_ACTIVE_CATEGORY, Categories::STATUS_DEACTIVATE_CATEGORY])->get(),
            'basis' => Basis::whereIn('status', [Basis::STATUS_ACTIVE_BASIS, Basis::STATUS_DEACTIVATE_BASIS])->get(),
            'department' => Department::whereIn('status', [Department::STATUS_ACTIVE_DEPARTMENT, Department::STATUS_DEACTIVATE_DEPARTMENT])->get(),
            'user_login' => $user_login,
            'lang_code' => $lang_code,
            'type' => $type,
        ];
        return view('admin/articles/create', $data);
    }

    public function ajax_edit($id)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $lang_code = self::langArticles();
        $type = self::typeArticles();

        $data = [
            'articles' => Articles::find($id),
            'categories' => Categories::whereIn('status', [Categories::STATUS_ACTIVE_CATEGORY, Categories::STATUS_DEACTIVATE_CATEGORY])->get(),
            'basis' => Basis::whereIn('status', [Basis::STATUS_ACTIVE_BASIS, Basis::STATUS_DEACTIVATE_BASIS])->get(),
            'department' => Department::whereIn('status', [Department::STATUS_ACTIVE_DEPARTMENT, Department::STATUS_DEACTIVATE_DEPARTMENT])->get(),
            'user_login' => $user_login,
            'lang_code' => $lang_code,
            'articles_vi' => ArticlesLang::where('lang_code', $lang_code['vi'])->where('articles_id', $id)->first(),
            'articles_en' => ArticlesLang::where('lang_code', $lang_code['en'])->where('articles_id', $id)->first(),
            'type' => $type,
        ];

        return view('admin/articles/edit', $data);
    }

    public function ajax_add(Request $request)
    {
        $user_login = session('user_auth');

//        if (empty($_FILES['imageFileThumb']) || empty($_FILES['imageFileBanner'])) {
//            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));
//
//            die(json_encode($result));
//        }

        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::ARTICLES;
        $data_insert = $this->convertDataHelper->convertData($data_request);
        $data_insert['thumb'] = null;
        $data_insert['banner'] = null;
        //upload file image
        if (!empty($_FILES['imageFileThumb']['name'])) {
            if ($_FILES['imageFileThumb']['name'] |= '') {
                $tmpName = $_FILES['imageFileThumb']['tmp_name'];
                $extension = explode(".", $_FILES['imageFileThumb']['name']);
                $file_extension = end($extension);
                $allowed_type = array("jpg", "jpeg", "png", "gif");
                $size = getimagesize($tmpName);
                list($width, $height) = $size;
//            if ($width < 0 || $height < 0) {
//                $result = [
//                    'status' => 'fail',
//                    'type' => 'warning',
//                    'message' => 'Vui lòng chọn file ảnh có độ phân giải 1280px x 720px'
//                ];
//                die(json_encode($result));
//            } else {
                if (in_array($file_extension, $allowed_type)) {
                    $new_name = rand() . "." . $file_extension;
                    $path = public_path() . "/images/admin/articles/" . $new_name;

                    if (move_uploaded_file($_FILES['imageFileThumb']['tmp_name'], $path)) {
                        $data_insert['thumb'] = "images/admin/articles/" . $new_name;
                        unset($data_insert['imageFileThumb']);
                    }
                } else {
                    $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

                    die(json_encode($result));
                }
//            }
            }
        }

        if (!empty($_FILES['imageFileBanner']['name'])) {
            if ($_FILES['imageFileBanner']['name'] |= '') {
                $tmpName = $_FILES['imageFileBanner']['tmp_name'];
                $extension = explode(".", $_FILES['imageFileBanner']['name']);
                $file_extension = end($extension);
                $allowed_type = array("jpg", "jpeg", "png", "gif");
                $size = getimagesize($tmpName);
                list($width, $height) = $size;
//            if ($width < 0 || $height < 0) {
//                $result = [
//                    'status' => 'fail',
//                    'type' => 'warning',
//                    'message' => 'Vui lòng chọn file ảnh có độ phân giải 1280px x 720px'
//                ];
//                die(json_encode($result));
//            } else {
                if (in_array($file_extension, $allowed_type)) {
                    $new_name = rand() . "." . $file_extension;
                    $path = public_path() . "/images/admin/articles/" . $new_name;

                    if (move_uploaded_file($_FILES['imageFileBanner']['tmp_name'], $path)) {
                        $data_insert['banner'] = "images/admin/articles/" . $new_name;
                        unset($data_insert['imageFileBanner']);
                    }
                } else {
                    $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

                    die(json_encode($result));
                }
//            }
            }
        }

        $data_log = [
            'type' => ActionHistory::TYPE_CREATE,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_CREATE . ' ' . ConvertDataHelper::ARTICLES,
            'data' => json_encode($data_insert),
            'user_id' => $user_login['id'],
        ];

        $data_insert_articles = [
            'slug' => $data_insert['slug'],
            'basis_id' => $data_insert['basis_id'],
            'category_id' => $data_insert['category_id'],
            'department_id' => $data_insert['department_id'],
            'thumb' => $data_insert['thumb'],
            'banner' => $data_insert['banner'],
            'is_hot' => $data_insert['is_hot'],
            'status' => $data_insert['status'],
            'type' => $data_insert['type'],
        ];

        if ($id = $this->articlesModel::create($data_insert_articles)) {

            $data_insert_articles_vi = [
                'articles_id' => $id->id,
                'lang_code' => $data_insert['lang_code_vi'],
                'title' => $data_insert['title_vi'],
                'content' => $data_insert['content_vi'],
                'summary' => $data_insert['summary_vi'],
            ];

            $data_insert_articles_en = [
                'articles_id' => $id->id,
                'lang_code' => $data_insert['lang_code_en'],
                'title' => $data_insert['title_en'],
                'content' => $data_insert['content_en'],
                'summary' => $data_insert['summary_en'],
            ];

            ArticlesLang::create($data_insert_articles_vi);
            ArticlesLang::create($data_insert_articles_en);

            if (ActionHistory::create($data_log)) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_create_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
            }
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
        }

        die(json_encode($result));
    }

    public function ajax_update(Request $request)
    {
        $user_login = session('user_auth');
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::ARTICLES;
        $data_update = $this->convertDataHelper->convertData($data_request);
        $lang_code = self::langArticles();

        //upload file image
        if (!empty($_FILES['imageFileThumb']['name'])) {
            $tmpName = $_FILES['imageFileThumb']['tmp_name'];
            $extension = explode(".", $_FILES['imageFileThumb']['name']);
            $file_extension = end($extension);
            $allowed_type = array("jpg", "jpeg", "png", "gif");
            $size = getimagesize($tmpName);
            list($width, $height) = $size;
//            if ($width < 0 || $height < 0) {
//                $result = [
//                    'status' => 'fail',
//                    'type' => 'warning',
//                    'message' => 'Vui lòng chọn file ảnh có độ phân giải 1280px x 720px'
//                ];
//                die(json_encode($result));
//            } else {
            if (in_array($file_extension, $allowed_type)) {
                $new_name = rand() . "." . $file_extension;
                AjaxHelper::create_folder(ConvertDataHelper::ARTICLES, $data_update['id']);
                $path = public_path() . "/images/admin/articles/" . $data_update['id'] . '/' . $new_name;

                if (move_uploaded_file($_FILES['imageFileThumb']['tmp_name'], $path)) {
                    $data_update['thumb'] = "images/admin/articles/" . $data_update['id'] . '/' . $new_name;
                }
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

                die(json_encode($result));
            }
//            }
        }

        if (!empty($_FILES['imageFileBanner']['name'])) {
            $tmpName = $_FILES['imageFileBanner']['tmp_name'];
            $extension = explode(".", $_FILES['imageFileBanner']['name']);
            $file_extension = end($extension);
            $allowed_type = array("jpg", "jpeg", "png", "gif");
            $size = getimagesize($tmpName);
            list($width, $height) = $size;
//            if ($width < 0 || $height < 0) {
//                $result = [
//                    'status' => 'fail',
//                    'type' => 'warning',
//                    'message' => 'Vui lòng chọn file ảnh có độ phân giải 1280px x 720px'
//                ];
//                die(json_encode($result));
//            } else {
            if (in_array($file_extension, $allowed_type)) {
                $new_name = rand() . "." . $file_extension;
                AjaxHelper::create_folder(ConvertDataHelper::ARTICLES, $data_update['id']);
                $path = public_path() . "/images/admin/articles/" . $data_update['id'] . '/' . $new_name;

                if (move_uploaded_file($_FILES['imageFileBanner']['tmp_name'], $path)) {
                    $data_update['banner'] = "images/admin/articles/" . $data_update['id'] . '/' . $new_name;
                }
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

                die(json_encode($result));
            }
//            }
        }

        unset($data_update['id']);
        unset($data_update['imageFileBanner']);
        unset($data_update['imageFileThumb']);
        $data_update['updated'] = date('Y-m-d H:i:s');
        $data_before_update = Articles::where('id', $request->id)->first();

        $data_log = [
            'type' => ActionHistory::TYPE_EDIT,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_EDIT . ' ' . ConvertDataHelper::ARTICLES,
            'data' => json_encode($data_before_update),
            'user_id' => $user_login['id'],
        ];
        $data_update_articles = [];

        if (!empty($data_update['thumb']) && !empty($data_update['banner'])) {
            $data_update_articles = [
                'slug' => $data_update['slug'],
                'basis_id' => $data_update['basis_id'],
                'category_id' => $data_update['category_id'],
                'department_id' => $data_update['department_id'],
                'is_hot' => $data_update['is_hot'],
                'status' => $data_update['status'],
                'updated' => $data_update['updated'],
                'type' => $data_update['type'],
                'thumb' => $data_update['thumb'],
                'banner' => $data_update['banner'],
            ];
        } else {
            if (!empty($data_update['thumb'])) {
                $data_update_articles = [
                    'slug' => $data_update['slug'],
                    'basis_id' => $data_update['basis_id'],
                    'category_id' => $data_update['category_id'],
                    'department_id' => $data_update['department_id'],
                    'is_hot' => $data_update['is_hot'],
                    'status' => $data_update['status'],
                    'thumb' => $data_update['thumb'],
                    'updated' => $data_update['updated'],
                    'type' => $data_update['type'],
                ];
            } else if (!empty($data_update['banner'])) {
                $data_update_articles = [
                    'slug' => $data_update['slug'],
                    'basis_id' => $data_update['basis_id'],
                    'category_id' => $data_update['category_id'],
                    'department_id' => $data_update['department_id'],
                    'is_hot' => $data_update['is_hot'],
                    'status' => $data_update['status'],
                    'banner' => $data_update['banner'],
                    'updated' => $data_update['updated'],
                    'type' => $data_update['type'],
                ];
            } else {
                $data_update_articles = [
                    'slug' => $data_update['slug'],
                    'basis_id' => $data_update['basis_id'],
                    'category_id' => $data_update['category_id'],
                    'department_id' => $data_update['department_id'],
                    'is_hot' => $data_update['is_hot'],
                    'status' => $data_update['status'],
                    'updated' => $data_update['updated'],
                    'type' => $data_update['type'],
                ];
            }
        }

        if (Articles::where('id', $request->id)->update($data_update_articles)) {
            $data_update_articles_vi = [
                'lang_code' => $data_update['lang_code_vi'],
                'title' => $data_update['title_vi'],
                'content' => $data_update['content_vi'],
                'summary' => $data_update['summary_vi'],
            ];

            $data_update_articles_en = [
                'lang_code' => $data_update['lang_code_en'],
                'title' => $data_update['title_en'],
                'content' => $data_update['content_en'],
                'summary' => $data_update['summary_en'],
            ];

            ArticlesLang::where('articles_id', $request->id)->where('lang_code', $lang_code['vi'])->update($data_update_articles_vi);
            ArticlesLang::where('articles_id', $request->id)->where('lang_code', $lang_code['en'])->update($data_update_articles_en);

            if (ActionHistory::create($data_log)) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_update_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_update_error'));
            }
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_update_error'));
        }

        die(json_encode($result));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();
        $ajaxDelete = $this->ajaxHelper->ajaxDelete($data_request, ConvertDataHelper::ARTICLES);

        die(json_encode($ajaxDelete));
    }

    public function langArticles()
    {
        return [
            'en' => 'en',
            'vi' => 'vi'
        ];
    }

    public function typeArticles()
    {
        return [
            1 => 'New',
            2 => 'Faq',
            3 => 'Recruitment'
        ];
    }
}
