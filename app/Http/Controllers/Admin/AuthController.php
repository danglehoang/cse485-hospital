<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\Models\Users;

class AuthController extends Controller
{


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getLogin()
    {
        if (empty(session('user_auth'))) {
            return view('admin/auth/login');
        } else {
            return redirect('/admin/dashboard');
        }
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback(Request $request)
    {
        try {

            $user = Socialite::driver('google')->user();

            $finduser = Users::where('email', $user->email)->first();

            if ($finduser && $finduser->status == 1) {

                $session_login = [
                    'id' => $finduser->id,
                    'email' => $finduser->email,
                    'full_name' => $finduser->full_name,
                    'status' => $finduser->status,
                    'role_id' => $finduser->role_id,
                    'avatar' => $finduser->avatar
                ];

                session(['user_auth' => $session_login]);

                return redirect('admin/dashboard');

            } else {
                return redirect('admin/auth/loginGoogleError');
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function loginGoogleError()
    {
        return view('admin/auth/login_error');
    }

    public function logout(Request $request)
    {
        session(['user_auth' => '']);
        return redirect('admin/auth/login');
    }

}
