<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\AjaxHelper;
use App\Helper\GetUserLogin;
use Illuminate\Http\Request;
use App\Helper\ConvertDataHelper;
use App\Models\Role;

class RoleController extends AdminController
{
    protected $roleModel;
    protected $convertDataHelper;
    protected $ajaxHelper;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->roleModel = new Role();
        $this->convertDataHelper = new ConvertDataHelper();
        $this->ajaxHelper = new AjaxHelper();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index()
    {
        $typeRole = $this->typeRole();
        $user_login = $this->getUserLogin->UserLogin();

        $data = [
            'type' => $typeRole,
            'user_login' => $user_login
        ];

        return view('admin/role/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $typeRole = $this->typeRole();
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;

        if (empty($request->status_search)) {
            $list_data = $this->roleModel->getListRole($params);
        } else {
            $list_data = $this->roleModel->getListRoleSearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $typeRole[$item->type];
            $row[] = $item->status == Role::STATUS_ACTIVE_ROLE ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactivate</span>';

            if ($user_login['detailRole']['type'] == Role::TYPE_ADMIN) {
                $action = '<div class="text-center">';
                $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
                $action .= '</div>';
                $row[] = $action;
            }

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->roleModel->countAll(),
            "recordsFiltered" => $this->roleModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function ajax_edit(Request $request, $id)
    {
        $ajaxEdit = $this->ajaxHelper->ajaxEdit($id, ConvertDataHelper::ROLE);

        die(json_encode($ajaxEdit));
    }

    public function ajax_add(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::ROLE;
        $ajaxAdd = $this->ajaxHelper->ajaxAdd($data_request, ConvertDataHelper::ROLE);

        die(json_encode($ajaxAdd));
    }

    public function ajax_update(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::ROLE;
        $ajaxUpdate = $this->ajaxHelper->ajaxUpdate($data_request, $request, ConvertDataHelper::ROLE);

        die(json_encode($ajaxUpdate));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();
        $ajaxDelete = $this->ajaxHelper->ajaxDelete($data_request, ConvertDataHelper::ROLE);

        die(json_encode($ajaxDelete));
    }

    public function typeRole()
    {
        return [
            '1' => 'ADMIN',
            '2'=> 'OTHER'
        ];
    }
}
