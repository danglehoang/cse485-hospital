<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\GetUserLogin;
use App\Models\Cadres;
use App\Models\DepartmentAfter;
use Illuminate\Http\Request;
use App\Helper\ConvertDataHelper;
use App\Models\Office;
use App\Models\Role;

class OfficeController extends AdminController
{
    protected $officeModel;
    protected $convertDataHelper;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->officeModel = new Office();
        $this->convertDataHelper = new ConvertDataHelper();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index()
    {
        $data = [
            'department_after' => DepartmentAfter::whereIn('status', [DepartmentAfter::STATUS_ACTIVE_DEPARTMENT_AFTER, DepartmentAfter::STATUS_DEACTIVATE_DEPARTMENT_AFTER])->get(),
            'cadres' => Cadres::whereIn('status', [Cadres::STATUS_ACTIVE_CADRES, Cadres::STATUS_DEACTIVATE_CADRES])->get(),
            'user_login' => session('user_auth'),
        ];
        return view('admin/office/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();

        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;
        $params['name_search'] = $request->name_search;

        if (empty($request->name_search) && empty($request->status_search)) {
            $list_data = $this->officeModel->getListOffice($params);
        } else {
            $list_data = $this->officeModel->getListOfficeSearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $item->phone;
            $row[] = $item->email;
            $row[] = $item->status == Office::STATUS_ACTIVE_OFFICE ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactivate</span>';
            $row[] = $item->updated;

            $action = '<div class="text-center">';
            $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
            $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
            $action .= '</div>';
            $row[] = $action;

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->officeModel->countAll(),
            "recordsFiltered" => $this->officeModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function ajax_edit(Request $request, $id)
    {
        if ($id == null) {
            die(json_encode([
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_please_fill_out_the_form')
            ]));
        }
        $data = $this->officeModel->findOrFail($id);

        if ($data) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_find_item_success'),
                'data' => $data
            ];
        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_find_item_error'),
            ];
        }

        die(json_encode($result));
    }

    public function ajax_add(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::OFFICE;
        $data_insert = $this->convertDataHelper->convertData($data_request);

        if ($this->officeModel::create($data_insert)) {

            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_create_success'),
            ];
        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_create_error'),
            ];
        }

        die(json_encode($result));
    }

    public function ajax_update(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::OFFICE;
        $data_update = $this->convertDataHelper->convertData($data_request);

        unset($data_update['id']);
        $data_update['updated'] = date('Y-m-d H:i:s');
        if ($this->officeModel::where('id', $request->id)->update($data_update)) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_update_success'),
            ];

        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_update_error'),
            ];
        }

        die(json_encode($result));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();

        if (!$data_request->id) {
            die(json_encode([
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_please_fill_out_the_form')
            ]));
        }

        if ($this->officeModel::where('id', $data_request->id)->delete()) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_delete_success'),
            ];

        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_delete_error'),
            ];
        }

        die(json_encode($result));
    }
}
