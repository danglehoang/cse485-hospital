<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\GetUserLogin;
use App\Models\Articles;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Socialite;


class DashboardController extends AdminController
{
    protected $usersModel;
    protected $articlesModel;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->usersModel = new Users();
        $this->articlesModel = new Articles();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index(Request $request)
    {
        $users = $this->usersModel->getTotalItemActive();
        $listUserNew = $this->usersModel->getListUserNew(8);
        $articles = $this->articlesModel->getTotalItemActive();

        $user_login = $this->getUserLogin->UserLogin();

        $data = [
            'users' => $users,
            'user_login' => $user_login,
            'articles' => $articles,
            'listUserNew' => $listUserNew
        ];

        return view('admin/dashboard/index', $data);
    }
}
