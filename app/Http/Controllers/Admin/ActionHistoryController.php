<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use Illuminate\Http\Request;
use App\Models\ActionHistory;
use App\Models\Users;

class ActionHistoryController extends AdminController
{
    protected $actionHistoryModel;

    public function __construct()
    {
        parent::__construct();
        $this->actionHistoryModel = new ActionHistory();
    }

    public function index()
    {
        $type = self::typeActionHistory();

        $data = [
            'user' => Users::whereIn('status', [Users::STATUS_ACTIVE_USER, Users::STATUS_DEACTIVATE_USER])->get(),
            'type' => $type,
        ];
        return view('admin/actionHistory/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $type = self::typeActionHistory();
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['name_search'] = $request->name_search;
        $params['type_search'] = $request->type_search;

        if (empty($request->name_search) && empty($request->type_search)) {
            $list_data = $this->actionHistoryModel->getListActionHistory($params);
        } else {
            $list_data = $this->actionHistoryModel->getListActionHistorySearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $type[$item->type];
            $row[] = $item->postsUser->full_name;
            $row[] = $item->postsUser->email;
            $row[] = $item->description;
            $row[] = $item->updated;

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->actionHistoryModel->countAll(),
            "recordsFiltered" => $this->actionHistoryModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function typeActionHistory() {
        return  [
            1 => 'CREATE',
            2 => 'EDIT',
            3 => 'DELETE',
        ];
    }
}
