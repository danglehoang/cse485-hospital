<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\AjaxHelper;
use App\Helper\GetUserLogin;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Helper\ConvertDataHelper;
use App\Models\Menu;

class MenuController extends AdminController
{
    protected $menuModel;
    protected $convertDataHelper;
    protected $ajaxHelper;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->menuModel = new Menu();
        $this->convertDataHelper = new ConvertDataHelper();
        $this->ajaxHelper = new AjaxHelper();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index()
    {
        $user_login = $this->getUserLogin->UserLogin();

        $data = [
            'user_login' => $user_login
        ];

        return view('admin/menu/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;
        $params['name_search'] = $request->name_search;
        $params['show_menu_search'] = $request->show_menu_search;

        if (empty($request->name_search) && empty($request->status_search) && empty($request->show_menu_search)) {
            $list_data = $this->menuModel->getListMenu($params);
        } else {
            $list_data = $this->menuModel->getListMenuSearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $item->menu_number;
            $row[] = $item->slug;
            $row[] = $item->show_menu == Menu::SHOW_MENU ? '<span class="btn btn-block btn-success btn-sm">Yes</span>' : '<span class="btn btn-block btn-danger btn-sm">No</span>';
            $row[] = $item->updated;
            $row[] = $item->status == Menu::STATUS_ACTIVE_MENU ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactivate</span>';

            if ($user_login['detailRole']['type'] == Role::TYPE_ADMIN) {
                $action = '<div class="text-center">';
                $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
                $action .= '</div>';
                $row[] = $action;
            }

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->menuModel->countAll(),
            "recordsFiltered" => $this->menuModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function ajax_edit(Request $request, $id)
    {
        $ajaxEdit = $this->ajaxHelper->ajaxEdit($id, ConvertDataHelper::MENU);

        die(json_encode($ajaxEdit));
    }

    public function ajax_add(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::MENU;
        $ajaxAdd = $this->ajaxHelper->ajaxAdd($data_request, ConvertDataHelper::MENU);

        die(json_encode($ajaxAdd));
    }

    public function ajax_update(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::MENU;
        $ajaxUpdate = $this->ajaxHelper->ajaxUpdate($data_request, $request, ConvertDataHelper::MENU);

        die(json_encode($ajaxUpdate));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();
        $ajaxDelete = $this->ajaxHelper->ajaxDelete($data_request, ConvertDataHelper::MENU);

        die(json_encode($ajaxDelete));
    }
}
