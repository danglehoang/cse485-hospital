<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\AjaxHelper;
use App\Helper\GetUserLogin;
use App\Helper\ResultHelper;
use App\Models\ActionHistory;
use App\Models\Basis;
use App\Models\Department;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Doctors;
use App\Helper\ConvertDataHelper;

class DoctorController extends AdminController
{
    protected $doctorsModel;
    protected $convertDataHelper;
    protected $resultHelper;
    protected $ajaxHelper;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->doctorsModel = new Doctors();
        $this->convertDataHelper = new ConvertDataHelper();
        $this->resultHelper = new ResultHelper();
        $this->ajaxHelper = new AjaxHelper();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index()
    {
        $user_login = $this->getUserLogin->UserLogin();
        $language = config('countries.code');

        $data = [
            'user_login' => $user_login,
            'basis' => Basis::whereIn('status', [Basis::STATUS_ACTIVE_BASIS, Basis::STATUS_DEACTIVATE_BASIS])->get(),
            'department' => Department::whereIn('status', [Department::STATUS_ACTIVE_DEPARTMENT, Department::STATUS_DEACTIVATE_DEPARTMENT])->get(),
            'language' => $language,
        ];

        return view('admin/doctors/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;
        $params['name_search'] = $request->name_search;
        $params['basis_search'] = $request->basis_search;
        $params['department_search'] = $request->department_search;
        $params['language_search'] = $request->language_search;

        if (empty($request->name_search) && empty($request->status_search) && empty($request->basis_search) && empty($request->department_search) && empty($request->language_search)) {
            $list_data = $this->doctorsModel->getListDoctor($params);
        } else {
            $list_data = $this->doctorsModel->getListDoctorSearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = isset($item->thumb) ? '<img class="w-25" src="/' . $item->thumb . '">' : '';
            $row[] = $item->name;
            $row[] = $item->postsDepartment->name;
            $row[] = $item->postsBasis->name;
            $row[] = $item->language;
            $row[] = $item->status == Doctors::STATUS_ACTIVE_DOCTOR ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactivate</span>';

            if ($user_login['detailRole']['type'] == Role::TYPE_ADMIN) {
                $action = '<div class="text-center">';
                $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
                $action .= '</div>';
                $row[] = $action;
            }

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->doctorsModel->countAll(),
            "recordsFiltered" => $this->doctorsModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));

    }

    public function create(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $language = config('countries.code');

        $data = [
            'department' => Department::whereIn('status', [Department::STATUS_ACTIVE_DEPARTMENT, Department::STATUS_DEACTIVATE_DEPARTMENT])->get(),
            'basis' => Basis::whereIn('status', [Basis::STATUS_ACTIVE_BASIS, Basis::STATUS_DEACTIVATE_BASIS])->get(),
            'language' => $language,
            'user_login' => $user_login,
        ];

        return view('admin/doctors/create', $data);
    }

    public function ajax_edit(Request $request, $id)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $language = config('countries.code');

        $data = [
            'department' => Department::whereIn('status', [Department::STATUS_ACTIVE_DEPARTMENT, Department::STATUS_DEACTIVATE_DEPARTMENT])->get(),
            'basis' => Basis::whereIn('status', [Basis::STATUS_ACTIVE_BASIS, Basis::STATUS_DEACTIVATE_BASIS])->get(),
            'language' => $language,
            'doctor' => Doctors::find($id),
            'user_login' => $user_login,
        ];

        return view('admin/doctors/edit', $data);
    }

    public function ajax_add(Request $request)
    {
        $user_login = session('user_auth');

        if (empty($_FILES['imageFile'])) {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

            die(json_encode($result));
        }

        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::DOCTOR;
        $data_insert = $this->convertDataHelper->convertData($data_request);

        //upload file image
        if ($_FILES['imageFile']['name'] |= '') {
            $tmpName = $_FILES['imageFile']['tmp_name'];
            $extension = explode(".", $_FILES['imageFile']['name']);
            $file_extension = end($extension);
            $allowed_type = array("jpg", "jpeg", "png", "gif");
            $size = getimagesize($tmpName);
            list($width, $height) = $size;
//            if ($width < 0 || $height < 0) {
//                $result = [
//                    'status' => 'fail',
//                    'type' => 'warning',
//                    'message' => 'Vui lòng chọn file ảnh có độ phân giải 1280px x 720px'
//                ];
//                die(json_encode($result));
//            } else {
            if (in_array($file_extension, $allowed_type)) {
                $new_name = rand() . "." . $file_extension;
                $path = public_path() . "/images/admin/doctors/" . $new_name;

                if (move_uploaded_file($_FILES['imageFile']['tmp_name'], $path)) {
                    $data_insert['thumb'] = "images/admin/doctors/" . $new_name;
                    unset($data_insert['imageFile']);
                }
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

                die(json_encode($result));
            }
//            }
        }

        $data_log = [
            'type' => ActionHistory::TYPE_CREATE,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_CREATE . ' ' . ConvertDataHelper::DOCTOR,
            'data' => json_encode($data_insert),
            'user_id' => $user_login['id'],
        ];

        if ($this->doctorsModel::create($data_insert)) {
            if(ActionHistory::create($data_log)) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_create_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
            }
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_create_error'));
        }

        die(json_encode($result));
    }

    public function ajax_update(Request $request)
    {
        $user_login = session('user_auth');
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::DOCTOR;
        $data_update = $this->convertDataHelper->convertData($data_request);

        //upload file image
        if (!empty($_FILES['imageFile']['name'])) {
            $tmpName = $_FILES['imageFile']['tmp_name'];
            $extension = explode(".", $_FILES['imageFile']['name']);
            $file_extension = end($extension);
            $allowed_type = array("jpg", "jpeg", "png", "gif");
            $size = getimagesize($tmpName);
            list($width, $height) = $size;
//            if ($width < 0 || $height < 0) {
//                $result = [
//                    'status' => 'fail',
//                    'type' => 'warning',
//                    'message' => 'Vui lòng chọn file ảnh có độ phân giải 1280px x 720px'
//                ];
//                die(json_encode($result));
//            } else {
            if (in_array($file_extension, $allowed_type)) {
                $new_name = rand() . "." . $file_extension;
                AjaxHelper::create_folder(ConvertDataHelper::DOCTOR, $data_update['id']);
                $path = public_path() . "/images/admin/doctors/" . $new_name;

                if (move_uploaded_file($_FILES['imageFile']['tmp_name'], $path)) {
                    $data_update['thumb'] = "images/admin/doctors/" . $new_name;
                }
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_warning'), trans('label.admin_result_choose_file_image'));

                die(json_encode($result));
            }
//            }
        }

        unset($data_update['id']);
        unset($data_update['imageFile']);
        $data_update['updated'] = date('Y-m-d H:i:s');
        $data_before_update = Doctors::where('id', $request->id)->first();

        $data_log = [
            'type' => ActionHistory::TYPE_EDIT,
            'description' => $user_login['full_name'] . ' ' . ActionHistory::DESCRIPTION_EDIT . ' ' . ConvertDataHelper::DOCTOR,
            'data' => json_encode($data_before_update),
            'user_id' => $user_login['id'],
        ];

        if ($this->doctorsModel::where('id', $request->id)->update($data_update)) {
            if (ActionHistory::create($data_log)) {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_success'), trans('label.admin_result_success'), trans('label.admin_result_update_success'));
            } else {
                $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_update_error'));
            }
        } else {
            $result = $this->resultHelper->resultAjax(trans('label.admin_result_fail'), trans('label.admin_result_error'), trans('label.admin_result_update_error'));
        }

        die(json_encode($result));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();
        $ajaxDelete = $this->ajaxHelper->ajaxDelete($data_request, ConvertDataHelper::DOCTOR);

        die(json_encode($ajaxDelete));
    }
}
