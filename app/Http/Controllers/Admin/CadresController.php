<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Models\DepartmentAfter;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Cadres;
use App\Helper\ConvertDataHelper;

class CadresController extends AdminController
{
    protected $cadresModel;
    protected $convertDataHelper;

    public function __construct()
    {
        parent::__construct();
        $this->cadresModel = new Cadres();
        $this->convertDataHelper = new ConvertDataHelper();
    }

    public function index()
    {
        $data = [
            'user_login' => session('user_auth'),
            'department' => DepartmentAfter::where('status', 1)->get(),
        ];
        return view('admin/cadres/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = session('user_auth');

        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;
        $params['name_search'] = $request->name_search;
        $params['email_search'] = $request->email_search;
        $params['office_search'] = $request->office_search;

        if (empty($request->name_search) && empty($request->status_search) && empty($request->email_search) && empty($request->office_search)) {
            $list_data = $this->cadresModel->getListCadres($params);
        } else {
            $list_data = $this->cadresModel->getListCadresSearch($params);
        }

        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $item->email;
            $row[] = $item->phone;
            $row[] = $item->postsDepartment->name;
            $row[] = $item->postsDepartment->postsOffice->name;
            $row[] = $item->status == Cadres::STATUS_ACTIVE_CADRES ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactive</span>';
            $row[] = $item->updated;

            $action = '<div class="text-center">';
            $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
            $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
            $action .= '</div>';
            $row[] = $action;

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->cadresModel->countAll(),
            "recordsFiltered" => $this->cadresModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function ajax_edit(Request $request, $id)
    {
        if (!$id) {
            die(json_encode([
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_please_fill_out_the_form')
            ]));
        }
        $data = $this->cadresModel->findOrFail($id);

        if ($data) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_find_item_success'),
                'data' => $data
            ];
        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_find_item_error'),
            ];
        }

        die(json_encode($result));
    }

    public function ajax_add(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::CADRES;
        $data_insert = $this->convertDataHelper->convertData($data_request);

        if ($this->cadresModel::create($data_insert)) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_create_success'),
            ];
        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_create_error'),
            ];
        }

        die(json_encode($result));
    }

    public function ajax_update(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::CADRES;
        $data_update = $this->convertDataHelper->convertData($data_request);

        unset($data_update['id']);
        $data_update['updated'] = date('Y-m-d H:i:s');

        if ($this->cadresModel::where('id', $request->id)->update($data_update)) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_update_success'),
            ];

        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_update_error'),
            ];
        }

        die(json_encode($result));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();

        if (!$data_request->id) {
            die(json_encode([
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_please_fill_out_the_form')
            ]));
        }

        if ($this->cadresModel::where('id', $data_request->id)->delete()) {
            $result = [
                'status' => trans('label.admin_result_success'),
                'type' => trans('label.admin_result_success'),
                'message' => trans('label.admin_result_delete_success'),
            ];

        } else {
            $result = [
                'status' => trans('label.admin_result_fail'),
                'type' => trans('label.admin_result_error'),
                'message' => trans('label.admin_result_delete_error'),
            ];
        }

        die(json_encode($result));
    }
}
