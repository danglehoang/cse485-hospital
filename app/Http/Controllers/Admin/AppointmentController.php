<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use Illuminate\Http\Request;
use App\Models\Appointment;

class AppointmentController extends AdminController
{
    protected $appointmentModel;

    public function __construct()
    {
        parent::__construct();
        $this->appointmentModel = new Appointment();
    }

    public function index(Request $request)
    {
        return view('admin/appointment/index');
    }

    public function ajax_data(Request $request)
    {
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;
        $list_data = $this->appointmentModel->getListAppointment($params);

        $data = [];
        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $item->phone;
            $row[] = $item->email;
            $row[] = $item->updated;

            $action = '<div class="text-center">';
            $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="show_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-eye"></i></a>';
            $action .= '</div>';
            $row[] = $action;
            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->appointmentModel->countAll(),
            "recordsFiltered" => $this->appointmentModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function ajax_show(Request $request, $id)
    {
        if (!$id) {
            die(json_encode(['status' => 'fail', 'type' => 'error', 'message' => 'Vui lòng điền đầy đủ thông tin']));
        }

        $data = $this->appointmentModel->find($id);

        if ($data) {
            $result = [
                'status' => 'success',
                'type' => 'success',
                'message' => 'Thông tin province',
                'data' => $data
            ];
        } else {
            $result = [
                'status' => 'fail',
                'type' => 'error',
                'message' => 'Không tồn tại province, vui lòng kiểm tra lại.',
            ];
        }

        die(json_encode($result));
    }
}

