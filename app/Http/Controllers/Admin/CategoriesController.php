<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminController;
use App\Helper\AjaxHelper;
use App\Helper\GetUserLogin;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Menu;
use App\Helper\ConvertDataHelper;

class CategoriesController extends AdminController
{
    protected $categoriesModel;
    protected $convertDataHelper;
    protected $ajaxHelper;
    protected $getUserLogin;

    public function __construct()
    {
        parent::__construct();
        $this->categoriesModel = new Categories();
        $this->convertDataHelper = new ConvertDataHelper();
        $this->ajaxHelper = new AjaxHelper();
        $this->getUserLogin = new GetUserLogin();
    }

    public function index()
    {
        $type = self::typeCategories();
        $user_login = $this->getUserLogin->UserLogin();

        $data = [
            'menu' => Menu::where('status', Menu::STATUS_ACTIVE_MENU)->where('show_menu', Menu::SHOW_MENU)
                ->get(),
            'type' => $type,
            'user_login' => $user_login
        ];
        return view('admin/categories/index', $data);
    }

    public function ajax_data(Request $request)
    {
        $user_login = $this->getUserLogin->UserLogin();
        $length = $request->length ? $request->length : 10;
        $no = $request->start ? $request->start : 0;
        $page = $no / $length + 1;
        $params['page'] = $page;
        $params['limit'] = $length;
        $params['status'] = $request->status ? $request->status : 1;

        $params['status_search'] = $request->status_search;
        $params['name_search'] = $request->name_search;
        $params['type_search'] = $request->type_search;
        $params['menu_search'] = $request->menu_search;

        if (empty($request->name_search) && empty($request->status_search) && empty($request->menu_search) && empty($request->type_search)) {
            $list_data = $this->categoriesModel->getListCategories($params);
        } else {
            $list_data = $this->categoriesModel->getListCategoriesSearch($params);
        }

        $type = self::typeCategories();
        $data = [];

        if ($list_data) foreach ($list_data as $item) {
            $row = [];
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $item->slug;
            $row[] = $item->postsMenu->name;
            $row[] = $type[$item->type];
            $row[] = $item->is_show_menu == Categories::STATUS_SHOW_MENU_CATEGORY ? '<span class="btn btn-block btn-success btn-sm">Yes</span>' : '<span class="btn btn-block btn-danger btn-sm">No</span>';
            $row[] = $item->status == Categories::STATUS_ACTIVE_CATEGORY ? '<span class="btn btn-block btn-success btn-sm">Active</span>' : '<span class="btn btn-block btn-danger btn-sm">Deactive</span>';

            if ($user_login['detailRole']['type'] == Role::TYPE_ADMIN) {
                $action = '<div class="text-center">';
                $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-pencil-alt"></i></a>';
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="fas fa-trash"></i></a>';
                $action .= '</div>';
                $row[] = $action;
            }

            $data[] = $row;
        }

        $result = [
            "draw" => $request->draw,
            "recordsTotal" => $this->categoriesModel->countAll(),
            "recordsFiltered" => $this->categoriesModel->countDataByDatatable(),
            "data" => $data,
        ];

        die(json_encode($result));
    }

    public function ajax_edit(Request $request, $id)
    {
        $ajaxEdit = $this->ajaxHelper->ajaxEdit($id, ConvertDataHelper::CATEGORY);

        die(json_encode($ajaxEdit));
    }

    public function ajax_add(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::CATEGORY;
        $ajaxAdd = $this->ajaxHelper->ajaxAdd($data_request, ConvertDataHelper::CATEGORY);

        die(json_encode($ajaxAdd));
    }

    public function ajax_update(Request $request)
    {
        $data_request = $request->all();
        $data_request['key'] = ConvertDataHelper::CATEGORY;
        $ajaxUpdate = $this->ajaxHelper->ajaxUpdate($data_request, $request, ConvertDataHelper::CATEGORY);

        die(json_encode($ajaxUpdate));
    }

    public function ajax_delete(Request $request)
    {
        $data_request = (object)$request->all();
        $ajaxDelete = $this->ajaxHelper->ajaxDelete($data_request, ConvertDataHelper::CATEGORY);

        die(json_encode($ajaxDelete));
    }

    public function typeCategories()
    {
        return [
            1 => trans('label.admin_type_categories_new'),
            2 => trans('label.admin_type_categories_landing_page'),
            3 => trans('label.admin_type_categories_list_new_two_column'),
            4 => trans('label.admin_type_categories_list_new_one_column'),
            5 => trans('label.admin_type_categories_list_new_two_row'),
            6 => trans('label.admin_type_categories_faq'),
            7 => trans('label.admin_type_categories_other'),
        ];
    }
}
