<?php
Route::get('admin/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');

Route::get('admin/users', 'Admin\UsersController@index')->name('admin.users');
Route::post('admin/users/ajax_list_data', 'Admin\UsersController@ajax_data')->name('admin.users.ajax_list_data');
Route::post('admin/users/ajax_add', 'Admin\UsersController@ajax_add')->name('admin.users.ajax_add');
Route::get('admin/users/ajax_edit/{id}', 'Admin\UsersController@ajax_edit')->name('admin.users.ajax_edit');
Route::post('admin/users/ajax_update', 'Admin\UsersController@ajax_update')->name('admin.users.ajax_update');
Route::post('admin/users/ajax_delete', 'Admin\UsersController@ajax_delete')->name('admin.users.ajax_delete');

Route::get('admin/auth/login', 'Admin\AuthController@getLogin');
Route::get('admin/auth/redirectGoogle', 'Admin\AuthController@redirectToGoogle');
Route::get('admin/auth/googleCallback', 'Admin\AuthController@googleCallback');
Route::get('admin/auth/logout', 'Admin\AuthController@logout');
Route::get('admin/auth/loginGoogleError', 'Admin\AuthController@loginGoogleError');

Route::get('admin/banner', 'Admin\BannerController@index')->name('admin.banner');
Route::post('admin/banner/ajax_list_data', 'Admin\BannerController@ajax_data')->name('admin.banner.ajax_list_data');
Route::post('admin/banner/ajax_add', 'Admin\BannerController@ajax_add')->name('admin.banner.ajax_add');
Route::get('admin/banner/ajax_edit/{id}', 'Admin\BannerController@ajax_edit')->name('admin.banner.ajax_edit');
Route::post('admin/banner/ajax_update', 'Admin\BannerController@ajax_update')->name('admin.banner.ajax_update');
Route::post('admin/banner/ajax_delete', 'Admin\BannerController@ajax_delete')->name('admin.banner.ajax_delete');

Route::get('admin/categories', 'Admin\CategoriesController@index')->name('admin.categories');
Route::post('admin/categories/ajax_list_data', 'Admin\CategoriesController@ajax_data')->name('admin.categories.ajax_list_data');
Route::post('admin/categories/ajax_add', 'Admin\CategoriesController@ajax_add')->name('admin.categories.ajax_add');
Route::get('admin/categories/ajax_edit/{id}', 'Admin\CategoriesController@ajax_edit')->name('admin.categories.ajax_edit');
Route::post('admin/categories/ajax_update', 'Admin\CategoriesController@ajax_update')->name('admin.categories.ajax_update');
Route::post('admin/categories/ajax_delete', 'Admin\CategoriesController@ajax_delete')->name('admin.categories.ajax_delete');

Route::get('admin/basis', 'Admin\BasisController@index')->name('admin.basis');
Route::post('admin/basis/ajax_list_data', 'Admin\BasisController@ajax_data')->name('admin.basis.ajax_list_data');
Route::post('admin/basis/ajax_add', 'Admin\BasisController@ajax_add')->name('admin.basis.ajax_add');
Route::get('admin/basis/ajax_edit/{id}', 'Admin\BasisController@ajax_edit')->name('admin.basis.ajax_edit');
Route::post('admin/basis/ajax_update', 'Admin\BasisController@ajax_update')->name('admin.basis.ajax_update');
Route::post('admin/basis/ajax_delete', 'Admin\BasisController@ajax_delete')->name('admin.basis.ajax_delete');

Route::get('admin/menu', 'Admin\MenuController@index')->name('admin.menu');
Route::post('admin/menu/ajax_list_data', 'Admin\MenuController@ajax_data')->name('admin.menu.ajax_list_data');
Route::post('admin/menu/ajax_add', 'Admin\MenuController@ajax_add')->name('admin.menu.ajax_add');
Route::get('admin/menu/ajax_edit/{id}', 'Admin\MenuController@ajax_edit')->name('admin.menu.ajax_edit');
Route::post('admin/menu/ajax_update', 'Admin\MenuController@ajax_update')->name('admin.menu.ajax_update');
Route::post('admin/menu/ajax_delete', 'Admin\MenuController@ajax_delete')->name('admin.menu.ajax_delete');

Route::get('admin/role', 'Admin\RoleController@index')->name('admin.role');
Route::post('admin/role/ajax_list_data', 'Admin\RoleController@ajax_data')->name('admin.role.ajax_list_data');
Route::post('admin/role/ajax_add', 'Admin\RoleController@ajax_add')->name('admin.role.ajax_add');
Route::get('admin/role/ajax_edit/{id}', 'Admin\RoleController@ajax_edit')->name('admin.role.ajax_edit');
Route::post('admin/role/ajax_update', 'Admin\RoleController@ajax_update')->name('admin.role.ajax_update');
Route::post('admin/role/ajax_delete', 'Admin\RoleController@ajax_delete')->name('admin.role.ajax_delete');

Route::get('admin/department', 'Admin\DepartmentController@index')->name('admin.department');
Route::post('admin/department/ajax_list_data', 'Admin\DepartmentController@ajax_data')->name('admin.department.ajax_list_data');
Route::post('admin/department/ajax_add', 'Admin\DepartmentController@ajax_add')->name('admin.department.ajax_add');
Route::get('admin/department/ajax_edit/{id}', 'Admin\DepartmentController@ajax_edit')->name('admin.department.ajax_edit');
Route::post('admin/department/ajax_update', 'Admin\DepartmentController@ajax_update')->name('admin.department.ajax_update');
Route::post('admin/department/ajax_delete', 'Admin\DepartmentController@ajax_delete')->name('admin.department.ajax_delete');

Route::get('admin/department_after', 'Admin\DepartmentAfterController@index')->name('admin.department_after');
Route::post('admin/department_after/ajax_list_data', 'Admin\DepartmentAfterController@ajax_data')->name('admin.department_after.ajax_list_data');
Route::post('admin/department_after/ajax_add', 'Admin\DepartmentAfterController@ajax_add')->name('admin.department_after.ajax_add');
Route::get('admin/department_after/ajax_edit/{id}', 'Admin\DepartmentAfterController@ajax_edit')->name('admin.department_after.ajax_edit');
Route::post('admin/department_after/ajax_update', 'Admin\DepartmentAfterController@ajax_update')->name('admin.department_after.ajax_update');
Route::post('admin/department_after/ajax_delete', 'Admin\DepartmentAfterController@ajax_delete')->name('admin.department_after.ajax_delete');

Route::get('admin/doctor', 'Admin\DoctorController@index')->name('admin.doctor');
Route::post('admin/doctor/ajax_list_data', 'Admin\DoctorController@ajax_data')->name('admin.doctor.ajax_list_data');
Route::post('admin/doctor/ajax_add', 'Admin\DoctorController@ajax_add')->name('admin.doctor.ajax_add');
Route::get('admin/doctor/add', 'Admin\DoctorController@create')->name('admin.doctor.add');
Route::get('admin/doctor/ajax_edit/{id}', 'Admin\DoctorController@ajax_edit')->name('admin.doctor.ajax_edit');
Route::post('admin/doctor/ajax_update', 'Admin\DoctorController@ajax_update')->name('admin.doctor.ajax_update');
Route::post('admin/doctor/ajax_delete', 'Admin\DoctorController@ajax_delete')->name('admin.doctor.ajax_delete');

Route::get('admin/articles', 'Admin\ArticlesController@index')->name('admin.articles');
Route::post('admin/articles/ajax_list_data', 'Admin\ArticlesController@ajax_data')->name('admin.articles.ajax_list_data');
Route::post('admin/articles/ajax_add', 'Admin\ArticlesController@ajax_add')->name('admin.articles.ajax_add');
Route::get('admin/articles/add', 'Admin\ArticlesController@create')->name('admin.articles.add');
Route::get('admin/articles/ajax_edit/{id}', 'Admin\ArticlesController@ajax_edit')->name('admin.articles.ajax_edit');
Route::post('admin/articles/ajax_update', 'Admin\ArticlesController@ajax_update')->name('admin.articles.ajax_update');
Route::post('admin/articles/ajax_delete', 'Admin\ArticlesController@ajax_delete')->name('admin.articles.ajax_delete');

Route::get('admin/appointment', 'Admin\AppointmentController@index')->name('admin.appointment');
Route::post('admin/appointment/ajax_list_data', 'Admin\AppointmentController@ajax_data')->name('admin.appointment.ajax_list_data');
Route::get('admin/appointment/ajax_show/{id}', 'Admin\AppointmentController@ajax_show')->name('admin.appointment.ajax_show');

Route::get('admin/actionHistory', 'Admin\ActionHistoryController@index')->name('admin.actionHistory');
Route::post('admin/actionHistory/ajax_list_data', 'Admin\ActionHistoryController@ajax_data')->name('admin.actionHistory.ajax_list_data');
Route::get('admin/auth/logout', 'Admin\AuthController@logout')->name('admin.auth.logout');

Route::get('admin/office', 'Admin\OfficeController@index')->name('admin.office');
Route::post('admin/office/ajax_list_data', 'Admin\OfficeController@ajax_data')->name('admin.office.ajax_list_data');
Route::post('admin/office/ajax_add', 'Admin\OfficeController@ajax_add')->name('admin.office.ajax_add');
Route::get('admin/office/ajax_edit/{id}', 'Admin\OfficeController@ajax_edit')->name('admin.office.ajax_edit');
Route::post('admin/office/ajax_update', 'Admin\OfficeController@ajax_update')->name('admin.office.ajax_update');
Route::post('admin/office/ajax_delete', 'Admin\OfficeController@ajax_delete')->name('admin.office.ajax_delete');

Route::get('admin/cadres', 'Admin\CadresController@index')->name('admin.cadres');
Route::post('admin/cadres/ajax_list_data', 'Admin\CadresController@ajax_data')->name('admin.cadres.ajax_list_data');
Route::post('admin/cadres/ajax_add', 'Admin\CadresController@ajax_add')->name('admin.cadres.ajax_add');
Route::get('admin/cadres/ajax_edit/{id}', 'Admin\CadresController@ajax_edit')->name('admin.cadres.ajax_edit');
Route::post('admin/cadres/ajax_update', 'Admin\CadresController@ajax_update')->name('admin.cadres.ajax_update');
Route::post('admin/cadres/ajax_delete', 'Admin\CadresController@ajax_delete')->name('admin.cadres.ajax_delete');

Route::get('/list', 'ListController@index')->name('list.index');
Route::post('/search-list', 'ListController@search')->name('list.search');
