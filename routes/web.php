<?php

Route::get('/', function () {
    return redirect('admin/auth/login');
});

if (isset($_SERVER['HTTP_HOST'])) {
    $host = $_SERVER['HTTP_HOST'];
} else {
    $host = '';
}

if($host == 'admin.hongngochospital.xyz' || $host == "admin.hospital.abc") {
    require_once __DIR__ . '/../routes/admin.php';
} else if($host == 'hongngochospital.xyz' || $host == "hospital.abc"){
    require_once __DIR__ . '/../routes/web_hospital.php';
}
