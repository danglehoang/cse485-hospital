<?php

Route::group(['middleware' => 'locale'], function() {
    Route::get('/', 'Hospital\HospitalController@index')->name('hospital.index');
    Route::get('/search', 'Hospital\HospitalController@searchArticles')->name('hospital.searchArticles');
    Route::get('/doctor/{slug_doctor}', 'Hospital\HospitalController@detailDoctor')->name('hospital.detailDoctor');
    Route::get('/footer/{basis_id}', 'Hospital\HospitalController@detailArticlesByBasisId')->name('hospital.detailArticlesByBasisId');
    Route::get('/department/{department_id}', 'Hospital\HospitalController@detailArticlesByDepartmentId')->name('hospital.detailArticlesByDepartmentId');
    Route::get('/menu/{menu_id}', 'Hospital\HospitalController@redirectByMenuId')->name('hospital.redirectByMenuId');
    Route::get('/{slug_articles}', 'Hospital\HospitalController@detailArticles')->name('hospital.detailArticles');
    Route::get('/change-language/{language}', 'Hospital\HospitalController@changeLanguage')->name('hospital.change_language');
    Route::get('/{slug_menu}/{slug_category}', 'Hospital\HospitalController@detailCategory')->name('hospital.detailCategory');
    Route::post('/submitAppointment', 'Hospital\HospitalController@submitAppointment')->name('hospital.submitAppointment');
});

